/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { Room } from "matrix-js-sdk/src/models/room";
import { isNullOrUndefined } from "matrix-js-sdk/src/utils";
import DMRoomMap from "../../../utils/DMRoomMap";
import { EventEmitter } from "events";
import { arrayDiff, arrayHasDiff, ArrayUtil } from "../../../utils/arrays";
import { getEnumValues } from "../../../utils/enums";
import { DefaultTagID, RoomUpdateCause, TagID } from "../models";
import {
    IListOrderingMap,
    IOrderingAlgorithmMap,
    ITagMap,
    ITagSortingMap,
    ListAlgorithm,
    SortAlgorithm,
} from "./models";
import { FILTER_CHANGED, FilterPriority, IFilterCondition } from "../filters/IFilterCondition";
import { EffectiveMembership, getEffectiveMembership, splitRoomsByMembership } from "../../../utils/membership";
import { OrderingAlgorithm } from "./list-ordering/OrderingAlgorithm";
import { getListAlgorithmInstance } from "./list-ordering";
import SettingsStore from "../../../settings/SettingsStore";

/**
 * Fired when the Algorithm has determined a list has been updated.
 */
export const LIST_UPDATED_EVENT = "list_updated_event";

// These are the causes which require a room to be known in order for us to handle them. If
// a cause in this list is raised and we don't know about the room, we don't handle the update.
//
// Note: these typically happen when a new room is coming in, such as the user creating or
// joining the room. For these cases, we need to know about the room prior to handling it otherwise
// we'll make bad assumptions.
const CAUSES_REQUIRING_ROOM = [
    RoomUpdateCause.Timeline,
    RoomUpdateCause.ReadReceipt,
];

interface IStickyRoom {
    room: Room;
    tag: TagID;
}

interface IPendingRoom {
    oldTag: TagID;
    newTag: TagID;
}

const isDM = (room: Room) => DMRoomMap.shared().getUserIdForRoomId(room.roomId);

/**
 * Represents a list ordering algorithm. This class will take care of tag
 * management (which rooms go in which tags) and ask the implementation to
 * deal with ordering mechanics.
 */
export class Algorithm extends EventEmitter {
    private readonly _tagChangePendingMap = new Map<Room, IPendingRoom>();
    private _cachedRooms: ITagMap = {};
    private filteredRooms: ITagMap = {};
    private _stickyRoom: IStickyRoom = null;
    private sortAlgorithms: ITagSortingMap;
    private listAlgorithms: IListOrderingMap;
    private algorithms: IOrderingAlgorithmMap;
    private rooms: Room[] = [];
    private roomIdsToTags: {
        [roomId: string]: TagID[];
    } = {};
    private allowedByFilter: Map<IFilterCondition, Room[]> = new Map<IFilterCondition, Room[]>();
    private allowedRoomsByFilters: Set<Room> = new Set<Room>();
    private bridgeTags = {};

    public constructor() {
        super();
    }

    public get stickyRoom(): Room {
        return this._stickyRoom ? this._stickyRoom.room : null;
    }

    protected get hasFilters(): boolean {
        return this.allowedByFilter.size > 0;
    }

    protected set cachedRooms(val: ITagMap) {
        this._cachedRooms = val;
        this.recalculateFilteredRooms();
    }

    protected get cachedRooms(): ITagMap {
        // 🐉 Here be dragons.
        // Note: this is used by the underlying algorithm classes, so don't make it return
        // the sticky room cache. If it ends up returning the sticky room cache, we end up
        // corrupting our caches and confusing them.
        return this._cachedRooms;
    }

    /**
     * Awaitable version of the sticky room setter.
     * @param val The new room to sticky.
     */
    public async setStickyRoom(val: Room) {
        await this.updateStickyRoom(val);
    }

    public getTagSorting(tagId: TagID): SortAlgorithm {
        if (!this.sortAlgorithms) return null;
        return this.sortAlgorithms[tagId];
    }

    public async setTagSorting(tagId: TagID, sort: SortAlgorithm) {
        if (!tagId) throw new Error("Tag ID must be defined");
        if (!sort) throw new Error("Algorithm must be defined");
        this.sortAlgorithms[tagId] = sort;

        const algorithm: OrderingAlgorithm = this.algorithms[tagId];
        await algorithm.setSortAlgorithm(sort);
        this._cachedRooms[tagId] = algorithm.orderedRooms;
        this.recalculateFilteredRoomsForTag(tagId); // update filter to re-sort the list
        // Finally, trigger an update
        this.emit(LIST_UPDATED_EVENT);
    }

    public setBridgeTags(bridgeTags) {
        this.bridgeTags = bridgeTags;
    }

    public getBridgeInfoByTag(tagId) {
        return this.bridgeTags[tagId];
    }

    public getListOrdering(tagId: TagID): ListAlgorithm {
        if (!this.listAlgorithms) return null;
        return this.listAlgorithms[tagId];
    }

    public async setListOrdering(tagId: TagID, order: ListAlgorithm) {
        if (!tagId) throw new Error("Tag ID must be defined");
        if (!order) throw new Error("Algorithm must be defined");
        this.listAlgorithms[tagId] = order;

        const algorithm = getListAlgorithmInstance(order, tagId, this.sortAlgorithms[tagId]);
        this.algorithms[tagId] = algorithm;

        await algorithm.setRooms(this._cachedRooms[tagId]);
        this._cachedRooms[tagId] = algorithm.orderedRooms;
        this.recalculateFilteredRoomsForTag(tagId); // update filter to re-sort the list
        // Finally, trigger an update
        this.emit(LIST_UPDATED_EVENT);
    }

    public addFilterCondition(filterCondition: IFilterCondition): void {
        // Populate the cache of the new filter
        this.allowedByFilter.set(filterCondition, this.rooms.filter(r => filterCondition.isVisible(r)));
        this.recalculateFilteredRooms();
        filterCondition.on(FILTER_CHANGED, this.handleFilterChange.bind(this));
    }

    public removeFilterCondition(filterCondition: IFilterCondition): void {
        filterCondition.off(FILTER_CHANGED, this.handleFilterChange.bind(this));
        if (this.allowedByFilter.has(filterCondition)) {
            this.allowedByFilter.delete(filterCondition);
            this.recalculateFilteredRooms();

            // If we removed the last filter, tell consumers that we've "updated" our filtered
            // view. This will trick them into getting the complete room list.
            if (!this.hasFilters) {
                this.emit(LIST_UPDATED_EVENT);
            }
        }
    }

    private async handleFilterChange() {
        await this.recalculateFilteredRooms();

        // re-emit the update so the list store can fire an off-cycle update if needed
        this.emit(FILTER_CHANGED);
    }

    private async clearStickyRoom() {
        if (this._stickyRoom) {
            const { tag } = this._stickyRoom;
            const algorithm = this.algorithms[tag];
            algorithm.stickyRoom = null;
            await algorithm.setRooms(this._cachedRooms[tag]);
            this._cachedRooms[tag] = algorithm.orderedRooms;
            this.emit(LIST_UPDATED_EVENT);
        }
    }

    private async updateStickyRoom(room: Room) {
        // It's possible to have no selected room. In that case, clear the sticky room
        if (!room) {
            await this.clearStickyRoom();
            return;
        }
        const tagForNewSticky = this.roomIdsToTags[room.roomId][0];
        if (!tagForNewSticky) throw new Error(`${room.roomId} does not belong to a tag and cannot be sticky`);
        // clear previous sticky room in another tag
        if (this._stickyRoom && this._stickyRoom.tag !== tagForNewSticky) {
            await this.clearStickyRoom();
        }
        this._stickyRoom = {
            room: room,
            tag: tagForNewSticky,
        };
        const algorithm = this.algorithms[tagForNewSticky];
        algorithm.stickyRoom = room;
        await algorithm.setRooms(this._cachedRooms[tagForNewSticky]);
        this._cachedRooms[tagForNewSticky] = algorithm.orderedRooms;
        this.emit(LIST_UPDATED_EVENT);
    }


    protected recalculateFilteredRooms() {
        if (!this.hasFilters) {
            return;
        }

        console.warn("Recalculating filtered room list");
        const filters = Array.from(this.allowedByFilter.keys());

        const orderedFilters = new ArrayUtil(filters)
            .groupBy(f => f.relativePriority)
            .orderBy(getEnumValues(FilterPriority))
            .value;
        const newMap: ITagMap = {};
        for (const tagId of Object.keys(this.cachedRooms)) {
            // Cheaply clone the rooms so we can more easily do operations on the list.
            // We optimize our lookups by trying to reduce sample size as much as possible
            // to the rooms we know will be deduped by the Set.
            const rooms = this.cachedRooms[tagId].map(r => r); // cheap clone
            let remainingRooms = rooms.map(r => r);
            let allowedRoomsInThisTag = [];
            let lastFilterPriority = orderedFilters[0].relativePriority;
            for (const filter of orderedFilters) {
                if (filter.relativePriority !== lastFilterPriority) {
                    // Every time the filter changes priority, we want more specific filtering.
                    // To accomplish that, reset the variables to make it look like the process
                    // has started over, but using the filtered rooms as the seed.
                    remainingRooms = allowedRoomsInThisTag;
                    allowedRoomsInThisTag = [];
                    lastFilterPriority = filter.relativePriority;
                }
                const filteredRooms = remainingRooms.filter(r => filter.isVisible(r));
                for (const room of filteredRooms) {
                    const idx = remainingRooms.indexOf(room);
                    if (idx >= 0) remainingRooms.splice(idx, 1);
                    allowedRoomsInThisTag.push(room);
                }
            }
            newMap[tagId] = allowedRoomsInThisTag;

            if (SettingsStore.getValue("advancedRoomListLogging")) {
                // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                console.log(`[DEBUG] ${newMap[tagId].length}/${rooms.length} rooms filtered into ${tagId}`);
            }
        }

        const allowedRooms = Object.values(newMap).reduce((rv, v) => { rv.push(...v); return rv; }, <Room[]>[]);
        this.allowedRoomsByFilters = new Set(allowedRooms);
        this.filteredRooms = newMap;
        this.emit(LIST_UPDATED_EVENT);
    }

    protected recalculateFilteredRoomsForTag(tagId: TagID): void {
        if (!this.hasFilters) return; // don't bother doing work if there's nothing to do

        if (SettingsStore.getValue("advancedRoomListLogging")) {
            // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
            console.log(`Recalculating filtered rooms for ${tagId}`);
        }
        delete this.filteredRooms[tagId];
        const rooms = this.cachedRooms[tagId].map(r => r); // cheap clone
        const filteredRooms = rooms.filter(r => this.allowedRoomsByFilters.has(r));
        if (filteredRooms.length > 0) {
            this.filteredRooms[tagId] = filteredRooms;
        }

        if (SettingsStore.getValue("advancedRoomListLogging")) {
            // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
            console.log(`[DEBUG] ${filteredRooms.length}/${rooms.length} rooms filtered into ${tagId}`);
        }
    }

    /**
     * Asks the Algorithm to regenerate all lists, using the tags given
     * as reference for which lists to generate and which way to generate
     * them.
     * @param {ITagSortingMap} tagSortingMap The tags to generate.
     * @param {IListOrderingMap} listOrderingMap The ordering of those tags.
     * @returns {Promise<*>} A promise which resolves when complete.
     */
    public async populateTags(tagSortingMap: ITagSortingMap, listOrderingMap: IListOrderingMap): Promise<any> {
        if (!tagSortingMap) throw new Error(`Sorting map cannot be null or empty`);
        if (!listOrderingMap) throw new Error(`Ordering ma cannot be null or empty`);
        if (arrayHasDiff(Object.keys(tagSortingMap), Object.keys(listOrderingMap))) {
            throw new Error(`Both maps must contain the exact same tags`);
        }
        this.sortAlgorithms = tagSortingMap;
        this.listAlgorithms = listOrderingMap;
        this.algorithms = {};
        for (const tag of Object.keys(tagSortingMap)) {
            this.algorithms[tag] = getListAlgorithmInstance(this.listAlgorithms[tag], tag, this.sortAlgorithms[tag]);
        }
        return this.setKnownRooms(this.rooms);
    }

    /**
     * Gets an ordered set of rooms for the all known tags, filtered.
     * @returns {ITagMap} The cached list of rooms, ordered,
     * for each tag. May be empty, but never null/undefined.
     */
    public getOrderedRooms(): ITagMap {
        if (!this.hasFilters) {
            return this.cachedRooms;
        }
        return this.filteredRooms;
    }

    public getUnfilteredRooms(): ITagMap {
        return this.cachedRooms;
    }

    private async addRoomToTag(room: Room, tagId: TagID): Promise<any> {
        const algorithm: OrderingAlgorithm = this.algorithms[tagId];
        if (!algorithm) throw new Error(`No algorithm for ${tagId}`);
        await algorithm.handleRoomUpdate(room, RoomUpdateCause.NewRoom);
        this._cachedRooms[tagId] = algorithm.orderedRooms;
        this.recalculateFilteredRoomsForTag(tagId);
    }

    private async removeRoomFromTag(room: Room, tagId: TagID): Promise<any> {
        const algorithm: OrderingAlgorithm = this.algorithms[tagId];
        if (!algorithm) throw new Error(`No algorithm for ${tagId}`);
        await algorithm.handleRoomUpdate(room, RoomUpdateCause.RoomRemoved);
        this._cachedRooms[tagId] = algorithm.orderedRooms;
        this.recalculateFilteredRoomsForTag(tagId);
    }

    public async handleTagChangePending(room: Room, newTag: TagID | null, oldTag: TagID | null): Promise<any> {
        if (!newTag) {
            newTag = isDM(room) ? DefaultTagID.DM : DefaultTagID.Untagged;
        }
        if (!oldTag) {
            oldTag = isDM(room) ? DefaultTagID.DM : DefaultTagID.Untagged;
        }

        this._tagChangePendingMap.set(room, {newTag, oldTag});
        await Promise.all([
            this.removeRoomFromTag(room, oldTag),
            this.addRoomToTag(room, newTag),
        ]);
    }

    public async handleBulkTagChangePending(rooms: Room, newTag: TagID | null, oldTag: TagID | null): Promise<any> {
        if (!oldTag) {
            // we can move rooms from one tag at a time, so just we use tag of first room here
            oldTag = isDM(rooms[0]) ? DefaultTagID.DM : DefaultTagID.Untagged;
        }

        const roomsByNewTags = {};
        for (const room of rooms) {
            let correctTag = newTag;
            if (!newTag) {
                correctTag = isDM(room) ? DefaultTagID.DM : DefaultTagID.Untagged;
            }
            if (!roomsByNewTags[correctTag]) {
                roomsByNewTags[correctTag] = [];
            }
            roomsByNewTags[correctTag].push(room);
            this._tagChangePendingMap.set(room, {newTag: correctTag, oldTag});
        }

        const algorithm: OrderingAlgorithm = this.algorithms[oldTag];
        if (!algorithm) throw new Error(`No algorithm for ${oldTag}`);
        await algorithm.setRooms(algorithm.orderedRooms.filter(r => !rooms.includes(r)));
        this._cachedRooms[oldTag] = algorithm.orderedRooms;
        this.recalculateFilteredRoomsForTag(oldTag);

        for (const tagId of Object.keys(roomsByNewTags)) {
            const algorithm1: OrderingAlgorithm = this.algorithms[tagId];
            if (!algorithm1) throw new Error(`No algorithm for ${tagId}`);
            await algorithm1.setRooms(algorithm1.orderedRooms.concat(roomsByNewTags[tagId]));
            this._cachedRooms[tagId] = algorithm1.orderedRooms;
            this.recalculateFilteredRoomsForTag(tagId);
        }
    }

    /**
     * Seeds the Algorithm with a set of rooms. The algorithm will discard all
     * previously known information and instead use these rooms instead.
     * @param {Room[]} rooms The rooms to force the algorithm to use.
     * @returns {Promise<*>} A promise which resolves when complete.
     */
    public async setKnownRooms(rooms: Room[]): Promise<any> {
        if (isNullOrUndefined(rooms)) throw new Error(`Array of rooms cannot be null`);
        if (!this.sortAlgorithms) throw new Error(`Cannot set known rooms without a tag sorting map`);

        console.warn("Resetting known rooms, initiating regeneration");

        this.rooms = rooms;

        const newTags: ITagMap = {};
        for (const tagId in this.sortAlgorithms) {
            // noinspection JSUnfilteredForInLoop
            newTags[tagId] = [];
        }

        // If we can avoid doing work, do so.
        if (!rooms.length) {
            await this.generateFreshTags(newTags); // just in case it wants to do something
            this.cachedRooms = newTags;
            return;
        }

        // Split out the easy rooms first (leave and invite)
        const memberships = splitRoomsByMembership(rooms);
        for (const room of memberships[EffectiveMembership.Invite]) {
            newTags[DefaultTagID.Invite].push(room);
        }
        for (const room of memberships[EffectiveMembership.Leave]) {
            newTags[DefaultTagID.Archived].push(room);
        }

        // Now process all the joined rooms. This is a bit more complicated
        for (const room of memberships[EffectiveMembership.Join]) {
            const tags = this.getTagsOfJoinedRoom(room);

            let inTag = false;
            if (tags.length > 0) {
                for (const tag of tags) {
                    if (!isNullOrUndefined(newTags[tag])) {
                        newTags[tag].push(room);
                        inTag = true;
                    }
                }
            }

            if (!inTag) {
                if (isDM(room)) {
                    newTags[DefaultTagID.DM].push(room);
                } else {
                    newTags[DefaultTagID.Untagged].push(room);
                }
            }
        }

        await this.generateFreshTags(newTags);

        this.cachedRooms = newTags;
        this.updateTagsFromCache();
        this.recalculateFilteredRooms();
    }

    public getTagsForRoom(room: Room): TagID[] {
        const tags: TagID[] = [];

        const membership = getEffectiveMembership(room.getMyMembership());
        if (membership === EffectiveMembership.Invite) {
            tags.push(DefaultTagID.Invite);
        } else if (membership === EffectiveMembership.Leave) {
            tags.push(DefaultTagID.Archived);
        } else {
            tags.push(...this.getTagsOfJoinedRoom(room));
        }

        if (!tags.length) tags.push(DefaultTagID.Untagged);

        return tags;
    }

    private getTagsOfJoinedRoom(room: Room): TagID[] {
        if (this._tagChangePendingMap.has(room)) {
            return [this._tagChangePendingMap.get(room).newTag];
        }

        let tags = Object.keys(room.tags || {});
        if (tags.length === 0) {
            // Check to see if it's a DM if it isn't anything else
            if (isDM(room)) {
                tags = [DefaultTagID.DM];
            }
        }
        return tags;
    }

    /**
     * Updates the roomsToTags map
     */
    private updateTagsFromCache() {
        const newMap = {};

        const tags = Object.keys(this.cachedRooms);
        for (const tagId of tags) {
            const rooms = this.cachedRooms[tagId];
            for (const room of rooms) {
                if (!newMap[room.roomId]) newMap[room.roomId] = [];
                newMap[room.roomId].push(tagId);
            }
        }

        this.roomIdsToTags = newMap;
    }

    /**
     * Called when the Algorithm believes a complete regeneration of the existing
     * lists is needed.
     * @param {ITagMap} updatedTagMap The tag map which needs populating. Each tag
     * will already have the rooms which belong to it - they just need ordering. Must
     * be mutated in place.
     * @returns {Promise<*>} A promise which resolves when complete.
     */
    private async generateFreshTags(updatedTagMap: ITagMap): Promise<any> {
        if (!this.algorithms) throw new Error("Not ready: no algorithms to determine tags from");

        for (const tag of Object.keys(updatedTagMap)) {
            const algorithm: OrderingAlgorithm = this.algorithms[tag];
            if (!algorithm) throw new Error(`No algorithm for ${tag}`);

            await algorithm.setRooms(updatedTagMap[tag]);
            updatedTagMap[tag] = algorithm.orderedRooms;
        }
    }

    /**
     * Asks the Algorithm to update its knowledge of a room. For example, when
     * a user tags a room, joins/creates a room, or leaves a room the Algorithm
     * should be told that the room's info might have changed. The Algorithm
     * may no-op this request if no changes are required.
     * @param {Room} room The room which might have affected sorting.
     * @param {RoomUpdateCause} cause The reason for the update being triggered.
     * @returns {Promise<boolean>} A promise which resolve to true or false
     * depending on whether or not getOrderedRooms() should be called after
     * processing.
     */
    public async handleRoomUpdate(room: Room, cause: RoomUpdateCause): Promise<boolean> {
        if (SettingsStore.getValue("advancedRoomListLogging")) {
            // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
            console.log(`Handle room update for ${room.roomId} called with cause ${cause}`);
        }
        if (!this.algorithms) throw new Error("Not ready: no algorithms to determine tags from");

        if (cause === RoomUpdateCause.NewRoom) {
            const roomTags = this.roomIdsToTags[room.roomId];
            const hasTags = roomTags && roomTags.length > 0;

            // Don't change the cause if the last sticky room is being re-added. If we fail to
            // pass the cause through as NewRoom, we'll fail to lie to the algorithm and thus
            // lose the room.
            if (hasTags) {
                console.warn(`${room.roomId} is reportedly new but is already known - assuming TagChange instead`);
                cause = RoomUpdateCause.PossibleTagChange;
            }

            // Check to see if the room is known first
            let knownRoomRef = this.rooms.includes(room);
            if (hasTags && !knownRoomRef) {
                console.warn(`${room.roomId} might be a reference change - attempting to update reference`);
                this.rooms = this.rooms.map(r => r.roomId === room.roomId ? room : r);
                knownRoomRef = this.rooms.includes(room);
                if (!knownRoomRef) {
                    console.warn(`${room.roomId} is still not referenced. It may be sticky.`);
                }
            }

            // If after all that we're still a NewRoom update, add the room if applicable.
            if (cause === RoomUpdateCause.NewRoom && !knownRoomRef) {
                this.rooms.push(room);
            }
        }

        let didTagChange = false;
        if (cause === RoomUpdateCause.PossibleTagChange) {
            if (this._tagChangePendingMap.has(room)) {
                // we already moved this room to another tag
                // so we just need update room tags
                this._tagChangePendingMap.delete(room);
                this.roomIdsToTags[room.roomId] = this.getTagsForRoom(room);
                return false;
            }

            const oldTags = this.roomIdsToTags[room.roomId] || [];
            const newTags = this.getTagsForRoom(room);
            const diff = arrayDiff(oldTags, newTags);
            if (diff.removed.length > 0 || diff.added.length > 0) {
                for (const rmTag of diff.removed) {
                    if (SettingsStore.getValue("advancedRoomListLogging")) {
                        // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                        console.log(`Removing ${room.roomId} from ${rmTag}`);
                    }
                    await this.removeRoomFromTag(room, rmTag);
                }
                for (const addTag of diff.added) {
                    if (SettingsStore.getValue("advancedRoomListLogging")) {
                        // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                        console.log(`Adding ${room.roomId} to ${addTag}`);
                    }
                    await this.addRoomToTag(room, addTag);
                }

                // Update the tag map so we don't regen it in a moment
                this.roomIdsToTags[room.roomId] = newTags;

                if (SettingsStore.getValue("advancedRoomListLogging")) {
                    // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                    console.log(`Changing update cause for ${room.roomId} to Timeline to sort rooms`);
                }
                cause = RoomUpdateCause.Timeline;
                didTagChange = true;
            } else {
                if (SettingsStore.getValue("advancedRoomListLogging")) {
                    // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                    console.log(`Skipping tag update for ${room.roomId} because tags didn't change`);
                }
                return false;
            }
        }

        if (!this.roomIdsToTags[room.roomId]) {
            if (CAUSES_REQUIRING_ROOM.includes(cause)) {
                if (SettingsStore.getValue("advancedRoomListLogging")) {
                    // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                    console.warn(`Skipping tag update for ${room.roomId} because we don't know about the room`);
                }
                return false;
            }

            if (SettingsStore.getValue("advancedRoomListLogging")) {
                // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                console.log(`[RoomListDebug] Updating tags for room ${room.roomId} (${room.name})`);
            }

            // Get the tags for the room and populate the cache
            const roomTags = this.getTagsForRoom(room).filter(t => !isNullOrUndefined(this.cachedRooms[t]));

            // "This should never happen" condition - we specify DefaultTagID.Untagged in getTagsForRoom(),
            // which means we should *always* have a tag to go off of.
            if (!roomTags.length) throw new Error(`Tags cannot be determined for ${room.roomId}`);

            this.roomIdsToTags[room.roomId] = roomTags;

            if (SettingsStore.getValue("advancedRoomListLogging")) {
                // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
                console.log(`[RoomListDebug] Updated tags for ${room.roomId}:`, roomTags);
            }
        }

        if (SettingsStore.getValue("advancedRoomListLogging")) {
            // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
            console.log(`[RoomListDebug] Reached algorithmic handling for ${room.roomId} and cause ${cause}`);
        }

        const tags = this.roomIdsToTags[room.roomId];
        if (!tags) {
            console.warn(`No tags known for "${room.name}" (${room.roomId})`);
            return false;
        }

        let changed = didTagChange;
        for (const tag of tags) {
            const algorithm: OrderingAlgorithm = this.algorithms[tag];
            if (!algorithm) throw new Error(`No algorithm for ${tag}`);

            await algorithm.handleRoomUpdate(room, cause);
            this._cachedRooms[tag] = algorithm.orderedRooms;

            // Flag that we've done something
            this.recalculateFilteredRoomsForTag(tag); // update filter to re-sort the list
            changed = true;
        }

        if (SettingsStore.getValue("advancedRoomListLogging")) {
            // TODO: Remove debug: https://github.com/vector-im/element-web/issues/14602
            console.log(`[RoomListDebug] Finished handling ${room.roomId} with cause ${cause} (changed=${changed})`);
        }
        return changed;
    }
}

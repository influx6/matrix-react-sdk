/*
Copyright 2020 Nova Technology Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { AsyncStoreWithClient } from "../AsyncStoreWithClient";
import defaultDispatcher from "../../dispatcher/dispatcher";
import { ActionPayload } from "../../dispatcher/payloads";
import { Room } from "matrix-js-sdk/src/models/room";
import { TagID } from "./models";
import RoomListStore from "./RoomListStore";

interface IState {}

export const SELECTION_CHANGED = "selection_changed";

export default class RoomListSelectionStore extends AsyncStoreWithClient<IState> {
    private static internalInstance: RoomListSelectionStore;

    private readonly selectedMap = new Map<TagID, Room[]>();
    private selectedTagId: TagID = null;

    constructor() {
        super(defaultDispatcher);
    }

    public static get instance(): RoomListSelectionStore {
        if (!RoomListSelectionStore.internalInstance) {
            RoomListSelectionStore.internalInstance = new RoomListSelectionStore();
        }
        return RoomListSelectionStore.internalInstance;
    }

    public getSelectedFor(tagId: TagID): Room[] {
        if (!this.selectedMap.has(tagId)) {
            this.selectedMap.set(tagId, []);
        }
        return this.selectedMap.get(tagId);
    }

    private clearSelection() {
        this.clearSelectionFor(this.selectedTagId);
        this.selectedTagId = null;
    }

    private toggleSelection(room: Room, tagId: TagID) {
        if (this.selectedTagId && this.selectedTagId !== tagId) {
            this.clearSelectionFor(this.selectedTagId);
        }
        this.selectedTagId = tagId;

        const selectedRooms = this.selectedMap.get(tagId);
        if (selectedRooms.includes(room)) {
            this.selectedMap.set(tagId, selectedRooms.filter(r => r !== room));
            if (!this.selectedMap.get(tagId).length) {
                this.selectedTagId = null;
            }
        } else {
            this.selectedMap.set(tagId, [...selectedRooms, room]);
        }
        this.emit(SELECTION_CHANGED);
    }

    private multiSelectTo = (room: Room, tagId: TagID) => {
        if (this.selectedTagId !== tagId || !this.selectedMap.get(tagId).length) {
            this.toggleSelection(room, tagId);
            return;
        }
        const selectedRooms = this.selectedMap.get(tagId);
        const lastSelected = selectedRooms[selectedRooms.length - 1];
        if (room === lastSelected) {
            return; // nothing to do
        }
        const orderedRooms = RoomListStore.instance.orderedLists[tagId];
        const indexOfNew = orderedRooms.indexOf(room);
        const indexOfLast = orderedRooms.indexOf(lastSelected);

        const isSelectingForwards = indexOfNew > indexOfLast;
        const start = isSelectingForwards ? indexOfLast : indexOfNew;
        const end = isSelectingForwards ? indexOfNew : indexOfLast;

        const roomsBetween = orderedRooms.slice(start, end + 1);
        const shouldUnselect = selectedRooms.includes(room);
        if (shouldUnselect) {
            this.selectedMap.set(tagId, selectedRooms.filter((r) => !roomsBetween.includes(r)));
        } else {
            const roomsToAdd = roomsBetween.filter((r) => !selectedRooms.includes(r));
            const sorted = isSelectingForwards ? roomsToAdd : roomsToAdd.reverse();
            this.selectedMap.set(tagId, [...selectedRooms, ...sorted]);
        }
        this.emit(SELECTION_CHANGED);
    }

    private clearSelectionFor(tagId: TagID) {
        this.selectedMap.set(tagId, []);
        this.emit(SELECTION_CHANGED);
    }

    protected async onNotReady(): Promise<any> {
        // On logout, clear.
        this.selectedMap.clear();
        this.selectedTagId = null;
    }

    protected async onAction(payload: ActionPayload): Promise<any> {
        if (!this.matrixClient) return;
        switch (payload.action) {
            case 'RoomListActions.Selection.toggle':
                this.toggleSelection(payload.room, payload.tagId);
                break;
            case 'RoomListActions.Selection.multiplyToggle':
                this.multiSelectTo(payload.room, payload.tagId);
                break;
            case 'RoomListActions.Selection.clear':
                this.clearSelection();
                break;
            case 'RoomListActions.bulkTagRoom.pending':
                setImmediate(() => this.clearSelection());
                break;
        }
    }
}

window.nvRoomListSelectionStore = RoomListSelectionStore.instance;

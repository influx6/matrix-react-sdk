/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { Room } from "matrix-js-sdk/src/models/room";
import { ActionPayload } from "../../dispatcher/payloads";
import shouldHideEvent from "../../shouldHideEvent";
import { AsyncStoreWithClient } from "../AsyncStoreWithClient";
import defaultDispatcher from "../../dispatcher/dispatcher";
import { MessageEventPreview } from "./previews/MessageEventPreview";
import { TagID } from "./models";
import { isNullOrUndefined } from "matrix-js-sdk/src/utils";
import { CallInviteEventPreview } from "./previews/CallInviteEventPreview";
import { CallAnswerEventPreview } from "./previews/CallAnswerEventPreview";
import { CallHangupEvent } from "./previews/CallHangupEvent";
import { StickerEventPreview } from "./previews/StickerEventPreview";
import { ReactionEventPreview } from "./previews/ReactionEventPreview";
import { UPDATE_EVENT } from "../AsyncStore";
import {isSelf} from "./previews/utils";
// Emitted event for when a room's preview has changed. First argument will the room for which
// the change happened.
export const ROOM_PREVIEW_CHANGED = "room_preview_changed";

const PREVIEWS = {
    'm.room.message': {
        isState: false,
        previewer: new MessageEventPreview(),
    },
    'm.call.invite': {
        isState: false,
        previewer: new CallInviteEventPreview(),
    },
    'm.call.answer': {
        isState: false,
        previewer: new CallAnswerEventPreview(),
    },
    'm.call.hangup': {
        isState: false,
        previewer: new CallHangupEvent(),
    },
    'm.sticker': {
        isState: false,
        previewer: new StickerEventPreview(),
    },
    'm.reaction': {
        isState: false,
        previewer: new ReactionEventPreview(),
    },
};

// The maximum number of events we're willing to look back on to get a preview.
const MAX_EVENTS_BACKWARDS = 50;

// type merging ftw
type TAG_ANY = "im.vector.any";
const TAG_ANY: TAG_ANY = "im.vector.any";

export interface IPreview {
    text: string;
    ts: number;
    readByAll: boolean;
    eventId: string;
}

interface IState {
    // Empty because we don't actually use the state
}

export class MessagePreviewStore extends AsyncStoreWithClient<IState> {
    private static internalInstance = new MessagePreviewStore();

    // null indicates the preview is empty / irrelevant
    private previews = new Map<string, Map<TagID|TAG_ANY, IPreview|null>>();

    private constructor() {
        super(defaultDispatcher, {});
    }

    public static get instance(): MessagePreviewStore {
        return MessagePreviewStore.internalInstance;
    }

    /**
     * Gets the pre-translated preview for a given room
     * @param room The room to get the preview for.
     * @param inTagId The tag ID in which the room resides
     * @returns The preview, or null if none present.
     */
    public getPreviewForRoom(room: Room, inTagId: TagID): IPreview | null {
        if (!room) return null; // invalid room, just return nothing

        if (!this.previews.has(room.roomId)) this.generatePreview(room, inTagId);

        const previews = this.previews.get(room.roomId);
        if (!previews) return null;

        if (!previews.has(inTagId)) {
            return previews.get(TAG_ANY);
        }
        return previews.get(inTagId);
    }

    private generatePreview(room: Room, tagId?: TagID) {
        const events = room.timeline;
        if (!events) return; // should only happen in tests

        let map = this.previews.get(room.roomId);
        if (!map) {
            map = new Map<TagID | TAG_ANY, IPreview | null>();
            // We set the state later with the map, so no need to send an update now
            this.previews.set(room.roomId, map);
        }

        // Set the tags so we know what to generate
        if (!map.has(TAG_ANY)) map.set(TAG_ANY, null);
        if (tagId && !map.has(tagId)) map.set(tagId, null);

        let changed = false;
        for (let i = events.length - 1; i >= 0; i--) {
            if (i === events.length - MAX_EVENTS_BACKWARDS) return; // limit reached

            const event = events[i];
            // no previewable if we hide event in timeline
            if (shouldHideEvent(event)) continue;
            const previewDef = PREVIEWS[event.getType()];
            if (!previewDef) continue;
            if (previewDef.isState && isNullOrUndefined(event.getStateKey())) continue;

            const anyPreviewText = previewDef.previewer.getTextFor(event, null);
            if (!anyPreviewText) continue; // not previewable for some reason

            let readByAll = false;
            if (isSelf(event)) {
                readByAll = !this.getUnreadByMembersForEvent(room, event).length;
            }

            const prevPreview = map.get(TAG_ANY);
            changed = changed || !prevPreview || anyPreviewText !== prevPreview.text;
            map.set(TAG_ANY, {
                eventId: event.getId(),
                text: anyPreviewText,
                ts: event.getTs(),
                readByAll,
            });

            const tagsToGenerate = Array.from(map.keys()).filter(t => t !== TAG_ANY); // we did the any tag above
            for (const genTagId of tagsToGenerate) {
                const realTagId: TagID = genTagId === TAG_ANY ? null : genTagId;
                const previewText = previewDef.previewer.getTextFor(event, realTagId);
                if (previewText === anyPreviewText) {
                    changed = changed || !map.get(genTagId) || anyPreviewText !== map.get(genTagId).text;
                    map.delete(genTagId);
                } else {
                    changed = changed || !map.get(genTagId) || previewText !== map.get(genTagId).text;
                    map.set(genTagId, {
                        eventId: event.getId(),
                        text: previewText,
                        ts: event.getTs(),
                        readByAll,
                    });
                }
            }

            if (changed) {
                // We've muted the underlying Map, so just emit that we've changed.
                this.previews.set(room.roomId, map);
                this.emit(UPDATE_EVENT, this);
                this.emit(ROOM_PREVIEW_CHANGED, room);
            }
            return; // we're done
        }

        // At this point, we didn't generate a preview so clear it
        this.previews.set(room.roomId, new Map<TagID|TAG_ANY, IPreview|null>());
        this.emit(UPDATE_EVENT, this);
        this.emit(ROOM_PREVIEW_CHANGED, room);
    }

    private getUnreadByMembersForEvent(room: Room, event: Event) {
        const myUserId = this.matrixClient.getUserId();
        const readByMembers = room.getReceiptsForEvent(event).reduce((acc, receipt) => {
            if (!receipt.userId || receipt.type !== "m.read" || receipt.userId === myUserId) {
                return acc; // ignore non-read receipts and receipts from self.
            }
            return [...acc, receipt.userId];
        }, [myUserId]);
        return room.getMembersWithMembership("join").filter((member) => !readByMembers.includes(member.userId));
    }

    protected async onAction(payload: ActionPayload) {
        if (!this.matrixClient) return;

        if (payload.action === 'MatrixActions.Room.timeline' || payload.action === 'MatrixActions.Event.decrypted') {
            const event = payload.event; // TODO: Type out the dispatcher
            if (!this.previews.has(event.getRoomId())) return; // not important
            this.generatePreview(this.matrixClient.getRoom(event.getRoomId()), TAG_ANY);
        } else if (payload.action === "MatrixActions.Room.receipt") {
            const {room} = payload;
            if (!this.previews.has(room.roomId)) return; // not important

            const currentMap = this.previews.get(room.roomId);

            const eventId = currentMap.get(TAG_ANY).eventId;
            const receipts = payload.event.getContent()[eventId];
            if (!receipts || !receipts['m.read']) return;

            const previewedEvent = room.findEventById(eventId);
            if (!isSelf(previewedEvent)) return;

            const unreadByMembers = this.getUnreadByMembersForEvent(room, previewedEvent);
            if (unreadByMembers.length) return;

            for (const tagId of currentMap.keys()) {
                currentMap.set(tagId, {
                    ...currentMap.get(tagId),
                    readByAll: true,
                });
            }
            this.emit(UPDATE_EVENT, this);
            this.emit(ROOM_PREVIEW_CHANGED, room);
        }
    }
}

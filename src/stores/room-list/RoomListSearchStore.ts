/*
Copyright 2020 Nova Technology Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import {AsyncStoreWithClient} from "../AsyncStoreWithClient";
import defaultDispatcher from "../../dispatcher/dispatcher";
import {ActionPayload} from "../../dispatcher/payloads";
import {Room} from "matrix-js-sdk/src/models/room";
import {NovaTagID, RoomUpdateCause, TagID} from "./models";
import {EffectiveMembership, getEffectiveMembership, splitRoomsByMembership} from "../../utils/membership";
import {IOrderingAlgorithmMap, ITagMap, SortAlgorithm} from "./algorithms/models";
import {throttle} from "lodash";
import DMRoomMap from "../../utils/DMRoomMap";
import {NaturalAlgorithm} from "./algorithms/list-ordering/NaturalAlgorithm";

interface IState {}

export const SEARCH_LISTS_UPDATE_EVENT = "search_list_update";

export default class RoomListSearchStore extends AsyncStoreWithClient<IState> {
    private static internalInstance: RoomListSearchStore;
    private search: string = "";
    private algorithms: IOrderingAlgorithmMap = {
        [NovaTagID.Chats]: new NaturalAlgorithm(NovaTagID.Chats, SortAlgorithm.Recent),
        [NovaTagID.Groups]: new NaturalAlgorithm(NovaTagID.Groups, SortAlgorithm.Recent),
    };
    private result: ITagMap = {};
    private roomsForSearch: Room[] = [];

    constructor() {
        super(defaultDispatcher);
    }

    public static get instance(): RoomListSearchStore {
        if (!RoomListSearchStore.internalInstance) {
            RoomListSearchStore.internalInstance = new RoomListSearchStore();
        }
        return RoomListSearchStore.internalInstance;
    }

    private callUpdate = throttle(() => {
        this.emit(SEARCH_LISTS_UPDATE_EVENT);
    }, 200, {trailing: true, leading: true});

    public get orderedLists(): ITagMap {
        return this.result;
    }

    private clear() {
        this.result = {};
        this.search = "";
        this.roomsForSearch = [];
        this.emit(SEARCH_LISTS_UPDATE_EVENT);
    }

    private async onSearchChanged(val: string) {
        const newSearch = val.trim();

        if (!this.search || !newSearch.includes(this.search)) {
            const rooms = this.matrixClient.getVisibleRooms();
            const memberships = splitRoomsByMembership(rooms);
            this.roomsForSearch = memberships[EffectiveMembership.Invite].concat(memberships[EffectiveMembership.Join]);
        }

        this.search = newSearch;
        await this.generateResult();
        this.roomsForSearch = this.result[NovaTagID.Chats].concat(this.result[NovaTagID.Groups]);
        this.callUpdate();
    }

    private async generateResult() {
        const roomsByTag = {
            [NovaTagID.Chats]: [],
            [NovaTagID.Groups]: [],
        };
        for (const room of this.roomsForSearch) {
            const tagId = this.getTagForRoom(room);
            if (tagId) roomsByTag[tagId].push(room);
        }
        for (const tagId of Object.keys(roomsByTag)) {
            await this.algorithms[tagId].setRooms(roomsByTag[tagId]);
            this.result[tagId] = this.algorithms[tagId].orderedRooms;
        }
    }


    private getTagForRoom(room: Room): TagID | null {
        const regexp = this.buildRegexp(this.search);
        if (DMRoomMap.shared().getUserIdForRoomId(room.roomId) && room.name.search(regexp) !== -1) {
            return NovaTagID.Chats;
        }
        const membersNames = this.getMembersNamesForRoom(room);
        const membersMatches = membersNames.filter((str) => str.search(regexp) !== -1);
        if (membersMatches.length || room.name.search(regexp) !== -1) {
            return NovaTagID.Groups;
        }
        return null;
    }

    private getMembersNamesForRoom(room: Room): string[] {
        const members = room.getMembersWithMembership("join");
        const res = [];
        for (const member of members) {
            if (member.userId === room.myUserId) {
                continue;
            }
            res.push(member.displayName || member.name || member.userId);
        }
        return res;
    }

    private buildRegexp(str): RegExp {
        const escapedStr = str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        return new RegExp(`(^|\\s|_|-|#|"|')${escapedStr}`, 'i');
    }

    protected async onNotReady(): Promise<any> {
        // On logout, clear.
        this.result = {};
        this.search = "";
        this.roomsForSearch = [];
    }

    private async handleRoomUpdate(room: Room, cause: RoomUpdateCause) {
        const tagId = this.getTagForRoom(room);
        if (!tagId) return; // not important

        if (cause === RoomUpdateCause.NewRoom) {
            this.roomsForSearch = [...this.roomsForSearch, room];
        } else if (cause === RoomUpdateCause.RoomRemoved) {
            this.roomsForSearch.splice(this.roomsForSearch.indexOf(room), 1);
        }
        await this.algorithms[tagId].handleRoomUpdate(room, cause);
        this.result[tagId] = this.algorithms[tagId].orderedRooms;
        this.emit(SEARCH_LISTS_UPDATE_EVENT);
    }

    protected async onAction(payload: ActionPayload): Promise<any> {
        if (!this.matrixClient) return;
        if (payload.action === 'RoomListActions.Search.clear') {
            this.clear();
            return;
        } else if (payload.action === 'RoomListActions.Search.find') {
            await this.onSearchChanged(payload.search);
            return;
        }
        this.onDispatchAsync(payload);
        // We do this to intentionally break out of the current event loop task, allowing
        // us to instead wait for a more convenient time to run our updates.
        // setImmediate(() => this.onDispatchAsync(payload));
    }

    protected async onDispatchAsync(payload: ActionPayload) {
        // Everything here requires a MatrixClient or some sort of logical readiness.
        if (!this.matrixClient || !this.search) return;

        if (payload.action === 'MatrixActions.Room.myMembership') {
            const membershipPayload = (<any>payload); // TODO: Type out the dispatcher types
            const oldMembership = getEffectiveMembership(membershipPayload.oldMembership);
            const newMembership = getEffectiveMembership(membershipPayload.membership);
            if (oldMembership !== newMembership) {
                let shouldAddRoom = false;
                if (newMembership === EffectiveMembership.Join) {
                    const createEvent = membershipPayload.room.currentState.getStateEvents("m.room.create", "");
                    if (createEvent && createEvent.getContent()['predecessor']) {
                        const prevRoom = this.matrixClient.getRoom(createEvent.getContent()['predecessor']['room_id']);
                        if (prevRoom) {
                            await this.handleRoomUpdate(prevRoom, RoomUpdateCause.RoomRemoved);
                        }
                        shouldAddRoom = true;
                    }
                }
                if (newMembership === EffectiveMembership.Invite || shouldAddRoom) {
                    await this.handleRoomUpdate(membershipPayload.room, RoomUpdateCause.NewRoom);
                }
            }
        } else if (payload.action === 'MatrixActions.Room.timeline') {
            const eventPayload = (<any>payload); // TODO: Type out the dispatcher types

            // Ignore non-live events (backfill)
            if (!eventPayload.isLiveEvent || !payload.isLiveUnfilteredRoomTimelineEvent) return;

            const roomId = eventPayload.event.getRoomId();
            const room = this.matrixClient.getRoom(roomId);
            const tryUpdate = async (updatedRoom: Room) => {
                if (eventPayload.event.getType() === 'm.room.tombstone' && eventPayload.event.getStateKey() === '') {
                    const newRoom = this.matrixClient.getRoom(eventPayload.event.getContent()['replacement_room']);
                    if (newRoom) {
                        // If we have the new room, then the new room check will have seen the predecessor
                        // and did the required updates, so do nothing here.
                        return;
                    }
                }
                await this.handleRoomUpdate(room, RoomUpdateCause.Timeline);
            };
            if (!room) {
                console.warn(`Live timeline event ${eventPayload.event.getId()} received without associated room`);
                console.warn(`Queuing failed room update for retry as a result.`);
                setTimeout(async () => {
                    const updatedRoom = this.matrixClient.getRoom(roomId);
                    await tryUpdate(updatedRoom);
                }, 100); // 100ms should be enough for the room to show up
                return;
            } else {
                await tryUpdate(room);
            }
        } else if (payload.action === 'MatrixActions.Event.decrypted') {
            const eventPayload = (<any>payload); // TODO: Type out the dispatcher types
            const roomId = eventPayload.event.getRoomId();
            const room = this.matrixClient.getRoom(roomId);
            if (!room) {
                console.warn(`Event ${eventPayload.event.getId()} was decrypted in an unknown room ${roomId}`);
                return;
            }
            await this.handleRoomUpdate(room, RoomUpdateCause.Timeline);
        }
    }
}

window.nvRoomListSearchStore = RoomListSearchStore.instance;

/*
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { TagID, DefaultTagID } from "./models";
import { isNullOrUndefined } from "matrix-js-sdk/src/utils";

const LARGE_TILE_HEIGHT_PX = 55;
const SMALL_TILE_HEIGHT_PX = 31;
export const HEADER_HEIGHT = 37;

interface ISerializedListLayout {
    numTiles: number;
    showPreviews: boolean;
    collapsed: boolean;
    smallRows: boolean;
}

export class ListLayout {
    private _numTiles: number;
    private _previews: boolean;
    private _collapsed: boolean;
    private _smallRows: boolean;

    constructor(public readonly tagId: TagID) {
        const serialized = localStorage.getItem(this.key) || '{}';
        const parsed = <ISerializedListLayout>JSON.parse(serialized);

        this._numTiles = parsed.numTiles || 5;
        this._previews = isNullOrUndefined(parsed.showPreviews) ? true : parsed.showPreviews;
        this._collapsed = isNullOrUndefined(parsed.collapsed) ? false : parsed.collapsed;
        this._smallRows = isNullOrUndefined(parsed.smallRows) ? (tagId !== DefaultTagID.DM) : parsed.smallRows;
    }

    public get isCollapsed(): boolean {
        return this._collapsed;
    }

    public set isCollapsed(v: boolean) {
        this._collapsed = v;
        this.save();
    }

    public get showPreviews(): boolean {
        return this._previews;
    }

    public set showPreviews(v: boolean) {
        this._previews = v;
        this.save();
    }

    public get headerHeight(): number {
        return HEADER_HEIGHT;
    }

    public get tileHeight(): number {
        return this._smallRows ? SMALL_TILE_HEIGHT_PX : LARGE_TILE_HEIGHT_PX;
    }

    private get key(): string {
        return `mx_sublist_layout_${this.tagId}_boxed`;
    }

    public get visibleTiles(): number {
        return this._numTiles;
    }

    public set visibleTiles(v: number) {
        this._numTiles = v;
        this.save();
    }

    public get smallRows(): boolean {
        return this._smallRows;
    }

    public set smallRows(v: boolean) {
        this._smallRows = v;
        this.save();
    }

    public get minVisibleTiles(): number {
        return 1;
    }

    public get defaultVisibleTiles(): number {
        // This number is what "feels right", and mostly subject to design's opinion.
        return 5;
    }

    public tilesWithPadding(n: number, paddingPx: number): number {
        return this.pixelsToTiles(this.tilesToPixelsWithPadding(n, paddingPx));
    }

    public tilesToPixelsWithPadding(n: number, paddingPx: number): number {
        return this.tilesToPixels(n) + paddingPx;
    }

    public tilesToPixels(n: number): number {
        return n * this.tileHeight;
    }

    public pixelsToTiles(px: number): number {
        return px / this.tileHeight;
    }

    public reset() {
        localStorage.removeItem(this.key);
    }

    private save() {
        localStorage.setItem(this.key, JSON.stringify(this.serialize()));
    }

    private serialize(): ISerializedListLayout {
        return {
            numTiles: this.visibleTiles,
            showPreviews: this.showPreviews,
            collapsed: this.isCollapsed,
            smallRows: this.smallRows,
        };
    }
}

/*
Copyright 2020 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { MatrixEvent } from "matrix-js-sdk/src/models/event";
import { MatrixClientPeg } from "../../../MatrixClientPeg";
import DMRoomMap from '../../../utils/DMRoomMap';
import SettingsStore from "../../../settings/SettingsStore";

export function isSelf(event: MatrixEvent): boolean {
    const selfUserId = MatrixClientPeg.get().getUserId();
    if (event.getType() === 'm.room.member') {
        return event.getStateKey() === selfUserId;
    }
    return event.getSender() === selfUserId;
}

export function isSelfTarget(event: MatrixEvent): boolean {
    const selfUserId = MatrixClientPeg.get().getUserId();
    return event.getStateKey() === selfUserId;
}

export function shouldPrefixMessagesIn(event: MatrixEvent): boolean {
    return !DMRoomMap.shared().getUserIdForRoomId(event.getRoomId());
}

export function getSenderName(event: MatrixEvent): string {
    const senderName = event.sender ? event.sender.name : event.getSender();
    const useShortName = SettingsStore.getValue("useShortNameInPreviews");

    return useShortName ? senderName.split(' ')[0] : senderName;
}

export function getTargetName(event: MatrixEvent): string {
    const targetName = event.target ? event.target.name : event.getStateKey();
    const useShortName = SettingsStore.getValue("useShortNameInPreviews");

    return useShortName ? targetName.split(' ')[0] : targetName;
}

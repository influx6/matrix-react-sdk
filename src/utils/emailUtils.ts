/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { MatrixEvent } from "matrix-js-sdk/src/models/event";
import { MatrixClientPeg } from '../MatrixClientPeg';
import SdkConfig from "../SdkConfig";

const BASE_URL = 'https://nova.gitlab.io/nova-web';

export const sendEmail = async (email: string, subject: string, html: string) => {
    const emailEndpoint = SdkConfig.get().email_sending_endpoint_url;
    try {
        await fetch(emailEndpoint, {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify({
                to: email,
                subject,
                html,
            }),
        });
    } catch (err) {
        console.error(`Cannot send email to "${email}"`, err);
    }
}

export const sendEventByEmail = async (email: string, event: MatrixEvent) => {
    const {
        sender: userId,
        content,
        origin_server_ts: ts,
        event_id: eventId,
        room_id: roomId
    } = event;

    const subject = content.body.substring(0, 101)
        .replace(/<del>(.*?)<\/del>|_/gim, '$1')
        .replace(/([^\p{Alpha}\p{M}\p{Nd}\p{Pc}\p{Join_C}])/gu, ' ')
        .replace(/[ ]{2,}/g, ' ')
        .trim();

    const cli = MatrixClientPeg.get();
    const user = cli.getUser(userId) || { displayName: userId };
    const sender = user.displayName;

    const link = `${BASE_URL}/#/room/${roomId}/${eventId}`;

    const text = content.formatted_body ? content.formatted_body : content.body;

    const htmlBody = [
        `<p>Sender: ${sender}</p>`,
        `<p>Date: ${new Date(ts)}</p>`,
        `<p><a href="${link}">Open in browser</a></p>`,
        `<p>Message: ${text}</p>`,
    ] .join('');

    await sendEmail(email, subject, htmlBody);
}

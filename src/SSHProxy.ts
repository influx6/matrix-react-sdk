/*
Copyright 2020 Nova Technology Ltd
*/

import dis from './dispatcher/dispatcher';
import { MatrixClientPeg } from "./MatrixClientPeg";
import PlatformPeg from "./PlatformPeg";

let shutdown = false;

async function startSSHProxy() {
    const platform = PlatformPeg.get();
    const client = MatrixClientPeg.get();
    const username = client.getUserId().split(':')[0].slice(1);
    console.log("Fetching SSH proxy details from asmux.beeperhq.com");
    const res = await fetch(`https://asmux.beeperhq.com/_matrix/asmux/mxauth/user/${username}/proxy`, {
        headers: {Authorization: `Bearer ${client.getAccessToken()}`},
    });
    if (res.status === 404) {
        console.log("SSH proxy not enabled on server");
        return;
    }
    const data = await res.json();
    console.log("Fetching SSH server port from api.beeperhq.com");
    const portRes = await fetch("https://api.beeperhq.com/whoami", {
        headers: {Authorization: `Bearer ${client.getAccessToken()}`},
    });
    const portData = await portRes.json();
    if (!portData.user.bridges.ssh || !portData.user.bridges.ssh.nodePort) {
        console.log("SSH proxy not enabled in api.beeperhq.com/whoami");
        return;
    }
    data.ssh.host = "176.9.21.44";
    data.ssh.port = portData.user.bridges.ssh.nodePort;

    console.log(`SSHing to ${data.ssh.host}:${data.ssh.port} and opening SOCKS proxy...`);
    await platform.startSshSocksProxy(data);
    console.log("SSH/SOCKS proxy opened");
}

let sshProxyTimeout = null;

function scheduleTryStartSSHProxy() {
    if (sshProxyTimeout !== null) {
        console.log("Not scheduling SSH proxy restart: one is already scheduled");
        return;
    }
    console.log("Restarting SSH proxy in 1 minute");
    sshProxyTimeout = setTimeout(() => {
        sshProxyTimeout = null;
        startSSHProxy().catch(err => {
            console.error("Error reopening SSH proxy:", err);
            scheduleTryStartSSHProxy();
        })
    }, 60 * 1000);
}

dis.register(payload => {
    if (payload.action !== "client_started" && payload.action !== "client_stopped") {
        return;
    }
    const platform = PlatformPeg.get();
    if (!platform.canSshProxy()) {
        console.log("Platform does not support SSH proxy");
        return;
    }
    platform.onSshSocksProxyClosed(() => {
        if (shutdown) {
            console.log("Electron platform told us the SSH socks proxy closed. Also, we're shutting down, so not restarting proxy.");
            return;
        }
        console.log("Electron platform told us the SSH socks proxy closed");
        scheduleTryStartSSHProxy();
    });
    if (payload.action === "client_started") {
        startSSHProxy().catch(err => {
            console.error("Error opening SSH proxy", err);
        });
    } else if (payload.action === "client_stopped") {
        console.log("Stopping SSH proxy");
        shutdown = true;
        platform.stopSshSocksProxy().catch(err => {
            console.error("Error closing SSH proxy", err);
        });
    }
})

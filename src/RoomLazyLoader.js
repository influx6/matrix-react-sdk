/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const PRELOAD_MESSAGES_COUNT = 50;
const PRELOAD_ROOMS_COUNT = 5;

class RoomsLazyLoader {
    constructor() {
        this._fullLoaded = false;
        this._matrixClient = null;
        this._loadedRoomsIds = [];

        this.sync = this.sync.bind(this);
        this.onLoadMessages = this.onLoadMessages.bind(this);
    }

    sync({ state, matrixClient }) {
        if (state !== 'SYNCING') return;
        this._matrixClient = matrixClient;
        this._loadRooms();
    }

    onLoadMessages(err, room) {
        if (err) {
            console.log(`Filed to load room`);
            return;
        }
        this._loadedRoomsIds.push(room.roomId);
    }

    markRoomAsLoaded(roomId) {
        if (this._fullLoaded) return;
        this._loadedRoomsIds.push(roomId);
    }

    _loadRooms() {
        const unloadedRooms = this._matrixClient.getRooms().filter((room) => {
            return room.getMyMembership() === 'join' && !this._loadedRoomsIds.includes(room.roomId);
        });

        if (!unloadedRooms.length) {
            if (!this._fullLoaded) {
                this._fullLoaded = true;
                console.log(`Initial messages for all rooms loaded by lazy-load`);
            }
            return;
        }

        unloadedRooms
            .sort((a, b) => this.__getLastMessageEventTimeline(b) - this.__getLastMessageEventTimeline(a))
            .slice(0, PRELOAD_ROOMS_COUNT)
            .forEach((room) => {
                this._matrixClient.scrollback(room, PRELOAD_MESSAGES_COUNT, this.onLoadMessages);
            });
    }

    __getLastMessageEventTimeline = ({ timeline }) => {
        let lastMessageTs = 0;
        for (let i = timeline.length - 1; i >= 0; i--) {
            if (timeline[i].event.type === "m.room.message") {
                lastMessageTs = timeline[i].event.origin_server_ts;
                break;
            }
        }
        return lastMessageTs;
    }
}

let singletonRoomsLazyLoader = null;
if (!singletonRoomsLazyLoader) {
    singletonRoomsLazyLoader = new RoomsLazyLoader();
}

export default singletonRoomsLazyLoader;

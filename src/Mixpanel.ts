/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import dis from './dispatcher/dispatcher';
import { MatrixClientPeg } from "./MatrixClientPeg";
import SdkConfig from './SdkConfig';
import Timer from './utils/Timer';

const UNAVAILABLE_TIME_MS = 3 * 60 * 1000; // 3 mins

class Detector {
    static getBrowser(userAgent: string, vendor: string, opera: boolean): string {
        vendor = vendor || ''; // vendor is undefined for at least IE9
        if (opera || userAgent.includes(' OPR/')) {
            if (userAgent.includes('Mini')) {
                return 'Opera Mini';
            }
            return 'Opera';
        } else if (/(BlackBerry|PlayBook|BB10)/i.test(userAgent)) {
            return 'BlackBerry';
        } else if (userAgent.includes('IEMobile') || userAgent.includes('WPDesktop')) {
            return 'Internet Explorer Mobile';
        } else if (userAgent.includes('SamsungBrowser/')) {
            // https://developer.samsung.com/internet/user-agent-string-format
            return 'Samsung Internet';
        } else if (userAgent.includes('Edge') || userAgent.includes('Edg/')) {
            return 'Microsoft Edge';
        } else if (userAgent.includes('FBIOS')) {
            return 'Facebook Mobile';
        } else if (userAgent.includes('Chrome')) {
            return 'Chrome';
        } else if (userAgent.includes('CriOS')) {
            return 'Chrome iOS';
        } else if (userAgent.includes('UCWEB') || userAgent.includes('UCBrowser')) {
            return 'UC Browser';
        } else if (userAgent.includes('FxiOS')) {
            return 'Firefox iOS';
        } else if (vendor.includes('Apple')) {
            if (userAgent.includes('Mobile')) {
                return 'Mobile Safari';
            }
            return 'Safari';
        } else if (userAgent.includes('Android')) {
            return 'Android Mobile';
        } else if (userAgent.includes('Konqueror')) {
            return 'Konqueror';
        } else if (userAgent.includes('Firefox')) {
            return 'Firefox';
        } else if (userAgent.includes('MSIE') || userAgent.includes('Trident/')) {
            return 'Internet Explorer';
        } else if (userAgent.includes('Gecko')) {
            return 'Mozilla';
        } else {
            return '';
        }
    }

    static  getBrowserVersion(userAgent, vendor, opera) {
        const browser = this.getBrowser(userAgent, vendor, opera);
        const versionRegexs = {
            'Internet Explorer Mobile': /rv:(\d+(\.\d+)?)/,
            'Microsoft Edge': /Edge?\/(\d+(\.\d+)?)/,
            'Chrome': /Chrome\/(\d+(\.\d+)?)/,
            'Chrome iOS': /CriOS\/(\d+(\.\d+)?)/,
            'UC Browser' : /(UCBrowser|UCWEB)\/(\d+(\.\d+)?)/,
            'Safari': /Version\/(\d+(\.\d+)?)/,
            'Mobile Safari': /Version\/(\d+(\.\d+)?)/,
            'Opera': /(Opera|OPR)\/(\d+(\.\d+)?)/,
            'Firefox': /Firefox\/(\d+(\.\d+)?)/,
            'Firefox iOS': /FxiOS\/(\d+(\.\d+)?)/,
            'Konqueror': /Konqueror:(\d+(\.\d+)?)/,
            'BlackBerry': /BlackBerry (\d+(\.\d+)?)/,
            'Android Mobile': /android\s(\d+(\.\d+)?)/,
            'Samsung Internet': /SamsungBrowser\/(\d+(\.\d+)?)/,
            'Internet Explorer': /(rv:|MSIE )(\d+(\.\d+)?)/,
            'Mozilla': /rv:(\d+(\.\d+)?)/
        };
        const regex = versionRegexs[browser];
        if (regex === undefined) {
            return null;
        }
        const matches = userAgent.match(regex);
        if (!matches) {
            return null;
        }
        return parseFloat(matches[matches.length - 2]);
    }

    static  getOS(userAgent: string): string {
        if (/Windows/i.test(userAgent)) {
            if (/Phone/.test(userAgent) || /WPDesktop/.test(userAgent)) {
                return 'Windows Phone';
            }
            return 'Windows';
        } else if (/(iPhone|iPad|iPod)/.test(userAgent)) {
            return 'iOS';
        } else if (/Android/.test(userAgent)) {
            return 'Android';
        } else if (/(BlackBerry|PlayBook|BB10)/i.test(userAgent)) {
            return 'BlackBerry';
        } else if (/Mac/i.test(userAgent)) {
            return 'Mac OS X';
        } else if (/Linux/.test(userAgent)) {
            return 'Linux';
        } else if (/CrOS/.test(userAgent)) {
            return 'Chrome OS';
        } else {
            return '';
        }
    }

    static  getDevice(userAgent: string): string {
        if (/Windows Phone/i.test(userAgent) || /WPDesktop/.test(userAgent)) {
            return 'Windows Phone';
        } else if (/iPad/.test(userAgent)) {
            return 'iPad';
        } else if (/iPod/.test(userAgent)) {
            return 'iPod Touch';
        } else if (/iPhone/.test(userAgent)) {
            return 'iPhone';
        } else if (/(BlackBerry|PlayBook|BB10)/i.test(userAgent)) {
            return 'BlackBerry';
        } else if (/Android/.test(userAgent)) {
            return 'Android';
        } else {
            return '';
        }
    }

    static  referringDomain(referrer: string): string {
        const split = referrer.split('/');
        if (split.length >= 3) {
            return split[2];
        }
        return '';
    }
}

class Mixpanel {
    private permanentProps: object | null = null;
    private baseUrl: string | null = null;
    private trackToken: string | null = '8f7e11e92b22ce096b86828e7349b93f';

    private trackingEnabled: boolean = true;
    private clientStarted: boolean = false;
    private online: boolean = false;

    private dispatcherRef: any;
    private unavailableTimer: Timer | null = null;

    private userID: string = null;

    constructor() {
        this.onAction = this.onAction.bind(this);
        this.dispatcherRef = dis.register(this.onAction);
    }

    public async updateProfile() {
        if (!this.trackingEnabled) {
            return;
        }
        console.info(`Send request to update user profile`);
        try {
            // @ts-ignore
            const isOpera = Boolean(window.opera);
            const data = btoa(JSON.stringify({
                '$set': {
                    '$os': Detector.getOS(navigator.userAgent),
                    '$browser': Detector.getBrowser(navigator.userAgent, navigator.vendor, isOpera),
                    'todesktop_version': window.todesktop ? window.todesktop.version : null,
                    'app_hash': SdkConfig.get().app_hash || 'UNKNOWN',
                    '$name': localStorage.getItem('mx_user_id')
                },
                '$token': this.trackToken,
                '$distinct_id': this.userID,
            }));
            await fetch(`https://api.mixpanel.com/engage/?data=${data}`, {
                method: 'POST',
                mode: 'cors',
            });
        } catch (err) {
            console.warn("Updating profile threw exception:", err)
        }
    }

    public async track(event: string, properties: Object = {}) {
        if (!this.trackingEnabled) {
            return;
        }
        console.info(`Send request to track event: ${event}`);
        if (!this.permanentProps) {
            const userAgent = navigator.userAgent;
            // @ts-ignore
            const isOpera = Boolean(window.opera);
            const appHash = SdkConfig.get().app_hash;
            this.permanentProps = {
                '$os': Detector.getOS(userAgent),
                '$browser': Detector.getBrowser(userAgent, navigator.vendor, isOpera),
                '$browser_version': Detector.getBrowserVersion(userAgent, navigator.vendor, isOpera),
                '$referrer': document.referrer,
                '$referring_domain': Detector.referringDomain(document.referrer),
                '$device': Detector.getDevice(userAgent),
                '$device_id': localStorage.getItem('mx_device_id') || '',
                'todesktop_version': window.todesktop ? window.todesktop.version : null,
                'app_hash': appHash || 'UNKNOWN',
                'token': this.trackToken,
            }
        }
        properties = {
            '$current_url': window.location.href,
            '$screen_height': screen.height,
            '$screen_width': screen.width,
            'time': Date.now() / 1000, // epoch time in seconds,
            'distinct_id': this.userID,
            ...this.permanentProps,
            ...properties,
        };
        // clean up props
        for (const key of Object.keys(properties)) {
            if (!properties[key]) {
                delete properties[key];
            }
        }
        try {
            const data = btoa(JSON.stringify({event, properties}))
            await fetch(`https://api.mixpanel.com/track/?data=${data}`, {
                method: 'POST',
                mode: 'cors',
            });
        } catch (err) {
            console.warn("Tracking threw exception:", err)
        }
    }

    private async init() {
        if (this.clientStarted) {
            return;
        }
        this.clientStarted = true;
        const client = MatrixClientPeg.get();
        this.userID = client.getUserId();

        // Legacy login tracked variable
        if (localStorage.nv_analytics_token) {
            localStorage.novaLoginTracked = "true";
        }
        const shouldTrackLogin = localStorage.novaLoginTracked !== "true";

        if (!this.trackingEnabled) {
            console.warn('Mixpanel tracking disabled');
            return;
        }
        console.info('Mixpanel tracking enabled');
        if (shouldTrackLogin) {
            await this.track("Nova-web login", { method: "direct" });
            localStorage.novaLoginTracked = "true";
        }
        this.updateProfile();
        this.startPresenceSync();
    }

    private stop() {
        this.togglePresence(false);
        if (this.dispatcherRef) {
            dis.unregister(this.dispatcherRef);
            this.dispatcherRef = null;
        }
        if (this.unavailableTimer) {
            this.unavailableTimer.abort();
            this.unavailableTimer = null;
        }
        this.baseUrl = null;
        this.trackToken = null;
        this.clientStarted = false;
    }

    public onAction(payload) {
        switch (payload.action) {
            case 'client_started':
                this.init();
                break;
            case 'client_stopped':
                this.stop();
                break;
            case 'user_activity':
                if (this.trackingEnabled && this.unavailableTimer) {
                    this.togglePresence(true);
                    this.unavailableTimer.restart();
                }
        }
    }

    private async startPresenceSync() {
        this.unavailableTimer = new Timer(UNAVAILABLE_TIME_MS);
        while (this.unavailableTimer) {
            try {
                await this.unavailableTimer.finished();
                this.togglePresence(false);
            } catch (e) {}
        }
    }

    private togglePresence(isOnline: boolean) {
        if (this.online === isOnline) {
            return;
        }
        if (isOnline) {
            this.track('$session_start');
        } else {
            const lastActiveTime = Date.now() - UNAVAILABLE_TIME_MS;
            this.track('$session_end', { 'time': lastActiveTime / 1000 });
        }
        this.online = isOnline;
    }
}

export default new Mixpanel();

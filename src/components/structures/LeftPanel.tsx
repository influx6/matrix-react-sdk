/*
Copyright 2020 Nova Technology Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as React from "react";
import { createRef } from "react";
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import { _t } from "../../languageHandler";
import RoomList, {TAG_AESTHETICS, ITagAesthetics, SUBLIST_HEADER_HEIGHT, customTagAesthetics} from "../views/rooms/RoomList";
import RoomListForTag from "../views/rooms/RoomListForTag";
import UserMenu from "./UserMenu";
import AppControls from "../views/elements/AppControls";
import RoomSearch from "./RoomSearch";
import RoomBreadcrumbs from "../views/rooms/RoomBreadcrumbs";
import { BreadcrumbsStore } from "../../stores/BreadcrumbsStore";
import { UPDATE_EVENT } from "../../stores/AsyncStore";
import ResizeNotifier from "../../utils/ResizeNotifier";
import SettingsStore from "../../settings/SettingsStore";
import { isCustomTag } from "../../stores/room-list/models";
import RoomListStore, { LISTS_UPDATE_EVENT } from "../../stores/room-list/RoomListStore";
import {Key} from "../../Keyboard";
import IndicatorScrollbar from "../structures/IndicatorScrollbar";
import {TagID} from "../../stores/room-list/models";
import dis from "../../dispatcher/dispatcher";
import AccessibleTooltipButton from "../views/elements/AccessibleTooltipButton";
import { OwnProfileStore } from "../../stores/OwnProfileStore";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import RoomListNumResults from "../views/rooms/RoomListNumResults";
import LeftPanelWidget from "./LeftPanelWidget";

// List of CSS classes which should be included in keyboard navigation within the room list
const cssClasses = [
    "nv_RoomSearch_input",
    "nv_RoomSublist2_sticky",
    "nv_RoomTile2",
    "nv_RoomSublist2_moreButton",
];
interface IProps {
    isMinimized: boolean;
    resizeNotifier: ResizeNotifier;
}

interface IState {
    searchFilter: string;
    showBreadcrumbs: boolean;
    activeTagId: TagID;
    scrollerHeight: number;
    scrollTop: number;
}

export default class LeftPanel extends React.Component<IProps, IState> {
    private listContainerRef: React.RefObject<HTMLDivElement> = createRef();

    private focusedElement = null;
    private isDoingStickyHeaders = false;

    constructor(props: IProps) {
        super(props);

        this.state = {
            searchFilter: "",
            showBreadcrumbs: BreadcrumbsStore.instance.visible,
            activeTagId: null,
            scrollerHeight: 0,
            scrollTop: 0
        };

        BreadcrumbsStore.instance.on(UPDATE_EVENT, this.onBreadcrumbsUpdate);
        RoomListStore.instance.on(LISTS_UPDATE_EVENT, this.onBreadcrumbsUpdate);
        // We watch the middle panel because we don't actually get resized, the middle panel does.
        // We listen to the noisy channel to avoid choppy reaction times.
        this.props.resizeNotifier.on("middlePanelResizedNoisy", this.onResize);
    }

    public componentWillUnmount() {
        BreadcrumbsStore.instance.off(UPDATE_EVENT, this.onBreadcrumbsUpdate);
        RoomListStore.instance.off(LISTS_UPDATE_EVENT, this.onBreadcrumbsUpdate);
        this.props.resizeNotifier.off("middlePanelResizedNoisy", this.onResize);
    }

    private onBreadcrumbsUpdate = () => {
        const newVal = BreadcrumbsStore.instance.visible;
        if (newVal !== this.state.showBreadcrumbs) {
            this.setState({showBreadcrumbs: newVal});

            // Update the sticky headers too as the breadcrumbs will be popping in or out.
            if (!this.listContainerRef.current) return; // ignore: no headers to sticky
            this.handleStickyHeaders(this.listContainerRef.current);
        }
    };

    public onResetScrollPosition = () => {
        this.listContainerRef.current.scrollTop = 0;
    }

    private onSearch = (term: string): void => {
        if (term) {
            dis.dispatch({
                action: 'RoomListActions.Search.find',
                search: term
            });
        } else {
            dis.dispatch({
                action: 'RoomListActions.Search.clear',
            })
        }
        const hadSearch = Boolean(this.state.searchFilter);
        const hasFilter = Boolean(term);
        if (hadSearch !== hasFilter) {
            // we use setImmediate here to prevent blink on components render if we should change sublists
            setImmediate(() => {
                this.setState({searchFilter: term}, () => {
                    if (term) {
                        setImmediate(this.focusFirstTile);
                    }
                });
            })
        } else {
            this.setState({searchFilter: term}, () => {
                if (term) {
                    setImmediate(this.focusFirstTile);
                }
            });
        }
    };

    private focusFirstTile = () => {
        const firstRoom = this.listContainerRef.current.querySelector<HTMLDivElement>(".nv_RoomTile2");
        if (firstRoom) {
            this.focusedElement = firstRoom;
        }
    }

    private handleStickyHeaders(list: HTMLDivElement) {
        if (this.isDoingStickyHeaders) return;
        this.isDoingStickyHeaders = true;
        window.requestAnimationFrame(() => {
            this.doStickyHeaders(list);
            this.isDoingStickyHeaders = false;
        });
    }

    private onMouseEnter = () => {
        const list = this.listContainerRef.current;
        if (list.classList.contains("nv_HideScrollbarFix")) {
            list.classList.remove("nv_HideScrollbarFix");
        }
        const headers = list.querySelectorAll<HTMLDivElement>(
            ".nv_RoomSublist2_sticky"
        );
        for (const header of headers) {
            if (!header.classList.contains("nv_RoomSublist2_sticky_showScroll")) {
                header.classList.add("nv_RoomSublist2_sticky_showScroll");
            }
        }
    }

    private onMouseLeave = () => {
        const list = this.listContainerRef.current;
        // hide scrollbar so that it does not overlap sublists headers border
        if (!list.classList.contains("nv_HideScrollbarFix")) {
            list.classList.add("nv_HideScrollbarFix");
        }
        const headers = list.querySelectorAll<HTMLDivElement>(
            ".nv_RoomSublist2_sticky_showScroll"
        );
        for (const header of headers) {
            header.classList.remove("nv_RoomSublist2_sticky_showScroll");
        }
    }

    public scrollToSublist = (sublistIndex: number) => {
        const list = this.listContainerRef.current;
        const sublists = list.querySelectorAll<HTMLDivElement>(".nv_RoomSublist2");

        let scrollTo = 0;
        for (const [i, sublist] of sublists.entries()) {
            if (i === sublistIndex) {
                list.scroll({
                    top: scrollTo,
                    behavior: 'smooth'
                })
                break;
            }
            scrollTo += sublist.offsetHeight - SUBLIST_HEADER_HEIGHT;
        }
    }

    private doStickyHeaders(list: HTMLDivElement) {
        const sublists = list.querySelectorAll<HTMLDivElement>(".nv_RoomSublist2");
        const listBottomEdge = list.scrollTop + list.offsetHeight;
        let sublistTopEdge = 0;

        for (const [index, sublist] of sublists.entries()) {
            const header = sublist.querySelector<HTMLDivElement>(".nv_RoomSublist2_sticky");
            const headerBottomEdge = (sublists.length - index - 1) * SUBLIST_HEADER_HEIGHT;

            if (sublistTopEdge - (index * SUBLIST_HEADER_HEIGHT) < list.scrollTop) {
                if (!header.classList.contains("nv_RoomSublist2_sticky_active")) {
                    header.classList.add("nv_RoomSublist2_sticky_active");
                }
                header.style.top = `${index * SUBLIST_HEADER_HEIGHT}px`;
                header.style.bottom = '';
            } else if (listBottomEdge - headerBottomEdge >= sublistTopEdge + SUBLIST_HEADER_HEIGHT) {
                if (header.classList.contains("nv_RoomSublist2_sticky_active")) {
                    header.classList.remove("nv_RoomSublist2_sticky_active");
                }
                header.style.top = '';
                header.style.bottom = '';
            } else {
                if (!header.classList.contains("nv_RoomSublist2_sticky_active")) {
                    header.classList.add("nv_RoomSublist2_sticky_active");
                }
                header.style.top = '';
                header.style.bottom = `${headerBottomEdge}px`;
            }

            sublistTopEdge = sublistTopEdge + sublist.offsetHeight;
        }
    }

    private onScroll = (ev: React.MouseEvent<HTMLDivElement>) => {
        this.setState({
            scrollTop: this.listContainerRef.current.scrollTop,
        });
        if (!this.listContainerRef.current) return;
        this.handleStickyHeaders(this.listContainerRef.current);
    };

    private onResize = () => {
        if (!this.listContainerRef.current) return; // ignore: no headers to sticky
        this.handleStickyHeaders(this.listContainerRef.current);
        this.setState({ scrollerHeight: this.listContainerRef.current.offsetHeight });
    };

    private onFocus = (ev: React.FocusEvent) => {
        this.focusedElement = ev.target;
    };

    private onBlur = () => {
        this.focusedElement = null;
    };

    private onKeyDown = (ev: React.KeyboardEvent) => {
        if (!this.focusedElement) return;

        switch (ev.key) {
            case Key.ARROW_UP:
            case Key.ARROW_DOWN:
                ev.stopPropagation();
                ev.preventDefault();
                this.onMoveFocus(ev.key === Key.ARROW_UP);
                break;
        }
    };

    private onEnter = () => {
        const firstRoom = this.listContainerRef.current.querySelector<HTMLDivElement>(".nv_RoomTile2");
        if (firstRoom) {
            firstRoom.click();
            return true; // to get the field to clear
        }
    };

    private onMoveFocus = (up: boolean) => {
        let element = this.focusedElement;

        let descending = false; // are we currently descending or ascending through the DOM tree?
        let classes: DOMTokenList;

        do {
            const child = up ? element.lastElementChild : element.firstElementChild;
            const sibling = up ? element.previousElementSibling : element.nextElementSibling;

            if (descending) {
                if (child) {
                    element = child;
                } else if (sibling) {
                    element = sibling;
                } else {
                    descending = false;
                    element = element.parentElement;
                }
            } else {
                if (sibling) {
                    element = sibling;
                    descending = true;
                } else {
                    element = element.parentElement;
                }
            }

            if (element) {
                classes = element.classList;
            }
        } while (element && !cssClasses.some(c => classes.contains(c)));

        if (element) {
            element.focus();
            this.focusedElement = element;
        }
    };

    private renderBreadcrumbs(): React.ReactNode {
        if (this.state.showBreadcrumbs && !this.state.searchFilter) {
            return (
                <IndicatorScrollbar
                    className="mx_LeftPanel_breadcrumbsContainer mx_AutoHideScrollbar"
                    verticalScrollsHorizontally={true}
                    // Firefox sometimes makes this element focusable due to
                    // overflow:scroll;, so force it out of tab order.
                    tabIndex={-1}
                >
                    <RoomBreadcrumbs />
                </IndicatorScrollbar>
            );
        }
    }

    private renderMainHeader(): React.ReactNode {
        const roomSearch = (
            <RoomSearch
                onQueryUpdate={this.onSearch}
                onVerticalArrow={this.onKeyDown}
                onEnter={this.onEnter}
            />
        );
        return (
            <div
                className="nv_LeftPanel2_headerContainer"
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onKeyDown={this.onKeyDown}
            >
                <AppControls side="left" />
                {roomSearch}
                <UserMenu />
            </div>
        );
    }

    public onOpenListForTagClick = (tagId: TagID) => () => {
        this.setState({ activeTagId: tagId });
    }

    public onCloseListForTagClick = () => {
        this.setState({ activeTagId: null });
    }
    private renderListForTag() {
        const { activeTagId } = this.state;
        const aesthetics: ITagAesthetics = isCustomTag(activeTagId)
            ? customTagAesthetics(activeTagId)
            : TAG_AESTHETICS[activeTagId];
        return (
            <RoomListForTag
                key={`sublist-${activeTagId}`}
                onKeyDown={this.onKeyDown}
                tagId={activeTagId}
                label={_t(aesthetics.sectionLabel)}
                onHeaderClick={this.onCloseListForTagClick}
                addRoomLabel={aesthetics.addRoomLabel}
                onAddRoom={aesthetics.onAddRoom}
                scrollerHeight={this.state.scrollerHeight}
            />
        );
    }

    private renderMainList() {
        const roomList = <RoomList
            scrollTop={this.state.scrollTop}
            scrollToSublist={this.scrollToSublist}
            onKeyDown={this.onKeyDown}
            resizeNotifier={null}
            collapsed={false}
            searchFilter={this.state.searchFilter}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            onResize={this.onResize}
            onResetScrollPosition={this.onResetScrollPosition}
            scrollerHeight={this.state.scrollerHeight}
            onSublistFooterClick={this.onOpenListForTagClick}
            isMinimized={this.props.isMinimized}
        />;

        return (
            <React.Fragment key="main-list">
                {this.renderMainHeader()}
                {this.renderBreadcrumbs()}
                <div
                    className="nv_LeftPanel2_roomListWrapper"
                    // Firefox sometimes makes this element focusable due to
                    // overflow:scroll;, so force it out of tab order.
                    tabIndex={-1}
                    onScroll={this.onScroll}
                >
                    <div
                        ref={this.listContainerRef}
                        // onScroll={this.onScroll}
                        onMouseEnter={this.onMouseEnter}
                        onMouseLeave={this.onMouseLeave}
                        className="mx_AutoHideScrollbar nv_LeftPanel_actualRoomListContainer"
                        tabIndex={-1}
                    >
                        {roomList}
                    </div>
                </div>
            </React.Fragment>
        );
    }

    public render(): React.ReactNode {
        const activeList = this.state.activeTagId ? (
            <CSSTransition
                key="list-for-tag"
                timeout={200}
                classNames="tagList"
            >
                {this.renderListForTag()}
            </CSSTransition>
        ) : (
            <CSSTransition
                key="main-list"
                timeout={200}
                classNames="mainList"
            >
                <aside className="nv_LeftPanel2_roomListContainer">
                    {this.renderMainList()}
                </aside>
            </CSSTransition>
        );

        return (
            <TransitionGroup className="nv_LeftPanel2">
                {activeList}
            </TransitionGroup>
        );
    }
}

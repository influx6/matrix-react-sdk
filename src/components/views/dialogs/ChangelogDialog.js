/*
Copyright 2016 Aviral Dasgupta
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2020 Nova Technology Ltd

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import React from 'react';
import PropTypes from 'prop-types';
import * as sdk from '../../../index';
import request from 'browser-request';
import { _t } from '../../../languageHandler';

const PROJECT_ID = '14987503';

export default class ChangelogDialog extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            details: null,
        };
    }

    componentDidMount() {
        const version = this.props.newVersion.split('-');
        const version2 = this.props.version.split('-');
        if (version == null || version2 == null) return;
        // parse versions of form: [novaweb]-react-[react-sdk-version]-js-[js-sdk-version]
        const oldVersion = version2[0];
        const newVersion = version[0];

        // eslint-disable-next-line max-len
        const url = `https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/compare?from=${oldVersion}&to=${newVersion}`;
        request(url, (err, res, body) => {
            if (res.statusCode < 200 || res.statusCode >= 300) {
                this.setState({ details: res.statusText });
                return;
            }
            this.setState({ details: JSON.parse(body).commits });
        });
    }

    _elementsForCommit(commit) {
        return (
            <li key={commit.short_id} className="mx_ChangelogDialog_li">
                <a href={commit.web_url} target="_blank" rel="noreferrer noopener">
                    {commit.message.split('\n')[0]}
                </a>
            </li>
        );
    }

    render() {
        const Spinner = sdk.getComponent('views.elements.Spinner');
        const QuestionDialog = sdk.getComponent('dialogs.QuestionDialog');

        const { details } = this.state;

        let content;
        if (details == null) {
            content = <Spinner />;
        } else if (typeof details === "string") {
            content = _t("Unable to load commit detail: %(msg)s", {
                msg: details,
            });
        } else {
            content = details.map(this._elementsForCommit);
        }

        const description = (
            <div className="mx_ChangelogDialog_content">
                {this.props.version == null || this.props.newVersion == null ? <h2>{_t("Unavailable")}</h2> : (
                    <div>
                        <h2>Beeper changes</h2>
                        <ul>{content}</ul>
                    </div>
                )}
            </div>
        );

        return (
            <QuestionDialog
                title={_t("Changelog")}
                description={description}
                button={_t("Update")}
                onFinished={this.props.onFinished}
            />
        );
    }
}

ChangelogDialog.propTypes = {
    version: PropTypes.string.isRequired,
    newVersion: PropTypes.string.isRequired,
    onFinished: PropTypes.func.isRequired,
};


/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { _t } from '../../../languageHandler';
import * as sdk from '../../../index';
import { MatrixClientPeg } from '../../../MatrixClientPeg';
import {
    Amp,
    Discord,
    Facebook,
    Hangouts,
    Instagram,
    Skype,
    Signal,
    Slack,
    Twitter,
    Telegram,
    WhatsApp,
    StatusCheck,
    StatusWarn,
    StatusWifiSlash,
} from '../icons';
import spinner from '../../../../res/img/spinner.gif';
import ToggleSwitch from '../elements/ToggleSwitch';
import TelegramPanel from './chat_networks/TelegramPanel';
import FacebookPanel from './chat_networks/FacebookPanel';
import DiscordPanel from './chat_networks/DiscordPanel';
import HangoutsPanel from './chat_networks/HangoutsPanel';
import InstagramPanel from './chat_networks/InstagramPanel';
import SignalPanel from './chat_networks/SignalPanel';
import SlackPanel from './chat_networks/SlackPanel';
import TwitterPanel from './chat_networks/TwitterPanel';
import WhatsAppPanel from './chat_networks/WhatsAppPanel';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const bridges = [
    // { Icon: Amp, bridge: 'amp', name: 'Android SMS', whoami: `/amp/public/api/whoami` },
    { Icon: Discord, panel: DiscordPanel, bridge: 'discord', name: 'Discord', whoami: `/discord/_matrix/provision/v1/status` },
    { Icon: Facebook, panel: FacebookPanel, bridge: 'facebook', name: 'Messenger', whoami: `/facebook/public/api/whoami` },
    { Icon: Hangouts, panel: HangoutsPanel, bridge: 'hangouts', name: 'Hangouts', whoami: `/hangouts/login/api/whoami` },
    {
        Icon: Instagram,
        panel: InstagramPanel,
        bridge: 'instagram',
        name: 'Instagram',
        whoami: `/instagram/_matrix/provision/v1/api/whoami`,
    },
    // { Icon: Skype, bridge: 'skype', name: 'Skype', whoami: `/skype/public/api/whoami` },
    { Icon: Signal, panel: SignalPanel, bridge: 'signal', name: 'Signal', whoami: `/signal/_matrix/provision/v1/api/whoami` },
    { Icon: Slack, panel: SlackPanel, bridge: 'slack', name: 'Slack', whoami: `/slack/_matrix/provision/v1/status` },
    { Icon: Telegram, panel: TelegramPanel, bridge: 'telegram', name: 'Telegram', whoami: `/telegram/_matrix/provision/v1/user` },
    { Icon: Twitter, panel: TwitterPanel, bridge: 'twitter', name: 'Twitter', whoami: `/twitter/_matrix/provision/v1/api/whoami` },
    { Icon: WhatsApp, panel: WhatsAppPanel, bridge: 'whatsapp', name: 'WhatsApp', whoami: `/whatsapp/_matrix/provision/v1/ping` },
];

export const fetchWhoami = async ({ userId, username, bridge, loginToken }) => {
    let connected = false;
    let bridgeJson;
    let status;
    const domain = `${username}.user.beeperhq.com`;
    const path = bridges.find(b => b.bridge === bridge).whoami;
    const suffix = bridge === 'telegram' ? `/${userId}` : `?user_id=${userId}`;
    try {
        const bridgeRes = await window.fetch(`https://${domain}${path}${suffix}`, {
            headers: { authorization: `Bearer ${loginToken}` },
        });
        status = bridgeRes.status;
        if (status === 200) {
            bridgeJson = await bridgeRes.json();
            if (
                (['discord', 'slack'].includes(bridge) && bridgeJson.puppets.length > 0) ||
                (bridge === 'facebook' && get(bridgeJson, 'facebook.name')) ||
                (bridge === 'instagram' && get(bridgeJson, 'instagram.full_name')) ||
                (bridge === 'telegram' && get(bridgeJson, 'telegram.id')) ||
                (bridge === 'twitter' && get(bridgeJson, 'twitter.name')) ||
                (bridge === 'whatsapp' && get(bridgeJson, 'whatsapp.conn.is_connected') && get(bridgeJson, 'whatsapp.conn.is_logged_in')) ||
                (bridge === 'signal' && get(bridgeJson, 'signal.number')) ||
                get(bridgeJson, `${bridge}.connected`)
            ) {
                connected = true;
            }
        } else {
            connected = status;
        }
    } catch (err) {
        console.error('whoami fetch error', err, status);
        status = 'error';
    }
    return { connected, bridgeJson, status };
};

export default class ChatNetworksDialog extends React.Component {
    static propTypes = {
        onFinished: PropTypes.func.isRequired,
    };

    state = {
        tab: 'discord',
        fetchingUserWhoami: true,
        fetchingamp: true,
        fetchingdiscord: true,
        fetchingfacebook: true,
        fetchinghangouts: true,
        fetchinginstagram: true,
        fetchingsignal: true,
        fetchingskype: true,
        fetchingslack: true,
        fetchingtelegram: true,
        fetchingtwitter: true,
        fetchingwhatsapp: true,
        enabling: [],
        disabling: [],
        connected: {},
        bridgeData: {},
        userData: null,
    };

    whoami = async (bridge, force) => {
        const { userData, username, userId, loginToken } = this.state;
        if (!force && !userData.bridges[bridge]) {
            this.setState({ [`fetching${bridge}`]: false });
            return false;
        }
        if (!force) {
            this.setState({ [`fetching${bridge}`]: true });
        }

        const { connected, bridgeJson, status } = await fetchWhoami({ username, bridge, userId, loginToken });

        if (status === 200) {
            this.setState({ bridgeData: { ...this.state.bridgeData, [bridge]: bridgeJson } });
        }

        this.setState({
            connected: { ...this.state.connected, [bridge]: connected },
            [`fetching${bridge}`]: false,
        });

        return { status, bridgeJson };
    };

    fetchUserWhoami = async (checkBridges = true) => {
        this.matrixClient = MatrixClientPeg.get();
        const userId = this.matrixClient.getUserId();
        const username = userId.split(':')[0].slice(1);
        const accessToken = await this.matrixClient.getAccessToken();
        const authedOpts = {
            headers: { Authorization: `Bearer ${accessToken}` },
        };
        const userData = (await window.fetch(`https://api.beeperhq.com/whoami`, authedOpts).then(res => res.json())).user;
        const loginToken = userData.asmuxData.login_token;
        this.setState({ userId, username, accessToken, userData, loginToken, fetchingUserWhoami: false }, async () => {
            if (checkBridges) {
                await Promise.all(bridges.map(bridge => this.whoami(bridge.bridge)));
            }
        });
    };

    componentDidMount = () => {
        this.fetchUserWhoami();
    };

    bridgeInfo = () => {
        const { tab, userData, bridgeData } = this.state;
        let info = 'not deployed';
        if (userData && userData.bridges[tab]) {
            const bridge = userData.bridges[tab];
            info = `${JSON.stringify(bridgeData[tab], null, 2)}\n\n${bridge.version}\nin ${bridge.namespace} on ${bridge.clusterAddress}`;
        }
        return (
            <div className="bridge-info">
                <span className="pi">π</span>
                <pre className="info">{info}</pre>
            </div>
        );
    };

    toggleBridge = async bridge => {
        const { enabling, disabling, userData } = this.state;
        if (enabling.includes(bridge) || disabling.includes(bridge)) return;
        if (userData.bridges[bridge]) {
            //disable
            this.setState({ disabling: [...disabling, bridge] });
            await this.stopBridge(bridge);
            await this.fetchUserWhoami(false);
            this.setState({ disabling: disabling.filter(b => b !== bridge) });
        } else {
            //enable
            this.setState({ enabling: [...enabling, bridge] });
            await this.startBridge(bridge);
            await this.fetchUserWhoami(false);
            this.setState({ enabling: enabling.filter(b => b !== bridge) });
        }
    };

    startBridge = async bridge => {
        this.startingBridge = true;
        await window.fetch(`https://api.beeperhq.com/bridge/${bridge}/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.state.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.whoami(bridge, true)).status;
        }
    };

    stopBridge = async bridge => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/${bridge}/stop`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.state.accessToken}` },
        });
        this.setState({
            connected: { ...this.state.connected, [bridge]: undefined },
        });
    };

    render() {
        const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
        const {
            fetchingUserWhoami,
            tab,
            connected,
            bridgeData,
            accessToken,
            userData,
            username,
            userId,
            loginToken,
            enabling,
            disabling,
        } = this.state;
        const panelConnected = connected[tab];
        const panelBridgeData = bridgeData[tab];
        const Panel = bridges.find(bridge => tab === bridge.bridge)?.panel;

        return (
            <BaseDialog className="mx_ChatNetworksDialog" hasCancel={true} onFinished={this.props.onFinished}>
                <div className="ms_SettingsDialog_content ChatNetworksDialog">
                    <div className="ChatNetworksDialog-leftbar">
                        <h2>Chat Networks</h2>
                        <div className="tabbar">
                            {bridges.map(({ Icon, bridge, name }) => {
                                let icon = null;
                                let toggle = null;
                                if (connected[bridge] === true) {
                                    icon = <StatusCheck className="status-icon ok" />;
                                } else if (connected[bridge] === false) {
                                    icon = <StatusWarn className="status-icon warn" />;
                                } else if (connected[bridge] === 502 || connected[bridge] === 503) {
                                    icon = <StatusWifiSlash className="status-icon error" />;
                                } else if (this.state[`fetching${bridge}`]) {
                                    icon = <img src={spinner} alt="" className="status-icon" />;
                                }
                                if (!this.state.fetchingUserWhoami) {
                                    toggle = (
                                        <ToggleSwitch
                                            checked={
                                                enabling.includes(bridge)
                                                    ? true
                                                    : disabling.includes(bridge)
                                                    ? false
                                                    : userData.bridges[bridge]
                                            }
                                            onChange={() => this.toggleBridge(bridge)}
                                        />
                                    );
                                }
                                return (
                                    <div
                                        key={bridge}
                                        onClick={() => this.setState({ tab: bridge })}
                                        className={`tab ${bridge === tab ? 'selected' : ''}`}
                                    >
                                        <Icon className="network-icon" />
                                        <div className="name">{name}</div>
                                        {icon}
                                        <span style={{ marginLeft: 10 }}>{toggle}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>

                    <div className="ChatNetworksDialog-content">
                        {fetchingUserWhoami ? (
                            <h2>Loading...</h2>
                        ) : enabling.includes(tab) ? (
                            <h2>Enabling {tab} bridge...</h2>
                        ) : disabling.includes(tab) ? (
                            <h2>Disabling {tab} bridge...</h2>
                        ) : Panel ? (
                            <Panel
                                {...{ username, userId, connected: panelConnected, bridgeData: panelBridgeData, loginToken, accessToken }}
                                setConnected={isConnected => this.setState({ connected: { ...connected, [tab]: isConnected } })}
                                whoami={() => this.whoami(tab, true)}
                            />
                        ) : null}
                        {this.bridgeInfo()}
                    </div>
                </div>
            </BaseDialog>
        );
    }
}

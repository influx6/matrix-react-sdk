/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { _t } from '../../../languageHandler';
import * as sdk from '../../../index';
import Modal from '../../../Modal';
import SdkConfig from '../../../SdkConfig';
import { MatrixClientPeg } from '../../../MatrixClientPeg';

export default class TagManagerDialog extends React.Component {
    static propTypes = {
        onFinished: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        const iframeUrl = SdkConfig.get().tag_manager_url;

        if (!iframeUrl) {
            console.log('URL for Tag Manager not defined in app config');
        }

        this.state = {
            iframeUrl: iframeUrl,
            error: !iframeUrl,
            messagePayload: null,
        };

        this._iframeRef = React.createRef();
    }

    componentDidMount() {
        const cli = MatrixClientPeg.get();
        const accessToken = cli.getAccessToken();
        const userId = cli.getUserId();
        const hsUrl = cli.getHomeserverUrl();

        this.setState({
            messagePayload: {
                accessToken,
                userId,
                hsUrl,
            },
        });
    }

    handleIframeLoad = async () => {
        const { iframeUrl, messagePayload } = this.state;

        this._iframeRef.current.contentWindow.postMessage({
            type: 'login',
            payload: messagePayload,
        }, iframeUrl);
    };

    render() {
        const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');

        const { error, iframeUrl } = this.state;

        if (error) {
            const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
            Modal.createTrackedDialog('Cannot open Tag Manager', '', ErrorDialog, {
                title: _t("Cannot open Tag Manager"),
                description: _t("URL for Tag Manager not defined in app config"),
            });
        }

        return (
            <BaseDialog
                className='mx_UserSettingsDialog'
                hasCancel={true}
                onFinished={this.props.onFinished}
                title={_t("Tag Manager")}
            >
                <div className='ms_SettingsDialog_content' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <iframe
                        ref={this._iframeRef}
                        src={iframeUrl}
                        onLoad={this.handleIframeLoad}
                        style={{ width: '100%', height: '1400px', border: 'none', margin: '0 auto' }}
                    />
                </div>
            </BaseDialog>
        );
    }
}

/*
Copyright 2020 Nova Technology Ltd

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import React from 'react';
import * as sdk from "matrix-react-sdk/src/index";
import { _t } from 'matrix-react-sdk/src/languageHandler';
import AccessibleButton from 'matrix-react-sdk/src/components/views/elements/AccessibleButton';
import Field from "matrix-react-sdk/src/components/views/elements/Field";
import StyledCheckbox from "matrix-react-sdk/src/components/views/elements/StyledCheckbox";
import isElectron from "is-electron";

const defaultState = {
    url: "socks5://138.197.204.83:9050",
    username: "",
    password: "",
    useAuth: false,
    rememberMe: false,
};

const STORAGED_PROXY_KEY = "nv_proxy_settings";

const isIpAddress = (str) => !str.split(".").filter((part) => isNaN(part) || part < 0 || part > 255).length;

const validateProxyUrl = (url) => {
    const [protocol, strWithIP, port] = url.split(":");
    if (!["socks5", "socks4", "http", "https"].includes(protocol) || isNaN(port)) {
        return "Invalid proxy url";
    }
    if (!isIpAddress(strWithIP.replace("//", ""))) {
        return "Invalid IP address";
    }
    return "";
};

export default class ConnectProxyDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            loading: false,
            ...JSON.parse(localStorage.getItem(STORAGED_PROXY_KEY) || JSON.stringify(defaultState)),
        };
    }

    componentDidMount() {
        if (isElectron()) {
            window.ipcRenderer.on("setProxyReply", this.onSetProxyReply);
        }
    }

    componentWillUnmount() {
        if (isElectron()) {
            window.ipcRenderer.removeListener("setProxyReply", this.onSetProxyReply);
        }
    }

    onSetProxyReply = async (ev, payload) => {
        if (payload.type !== "resolve") {
            this.setState({
                errorMessage: "Connection failed. Try again or use different proxy",
                loading: false,
            });
            return;
        }
        this.setState({
            loading: false,
        });
        this.props.onSuccess();
        this.props.onFinished();
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        // eslint-disable-next-line no-unused-vars
        const { errorMessage, loading, ...restState } = this.state;
        const { url, username, password } = restState;
        let validationError = "";
        const data = {};
        if (url) {
            data.url = url;
            validationError = validateProxyUrl(url);
        } else {
            window.ipcRenderer.send("setProxy", null);
            localStorage.removeItem(STORAGED_PROXY_KEY);
            return;
        }
        if (restState.useAuth) {
            if (!username.length || !password.length) {
                validationError = "Enter username and password";
            }
            data.username = username;
            data.password = password;
        }

        this.setState({
            errorMessage: validationError,
            loading: !validationError,
        });

        if (!validationError) {
            window.ipcRenderer.send("setProxy", data);
        }

        if (restState.rememberMe) {
            localStorage.setItem(STORAGED_PROXY_KEY, JSON.stringify(restState));
        } else {
            localStorage.removeItem(STORAGED_PROXY_KEY);
        }
    }

    handleUrlFieldChange = (evt) => {
        let url = evt.target.value.trim();
        this.setState((prevState) => {
            if (!prevState.url && (!isNaN(url) || isIpAddress(url.split(':')[0]))) {
                url = `socks5://${url}`;
            } else {
                const [protocol, withoutProtocol] = url.split('//');
                if (withoutProtocol) {
                    const [ips, port] = withoutProtocol.split(':');
                    const ipsArr = ips.split('.');
                    const lastNumber = ipsArr[ipsArr.length -1];
                    if (!isNaN(lastNumber) && (lastNumber[0] === '0' || lastNumber.length >= 3)) {
                        const separator = ipsArr.length === 4 ? ':' : '.';
                        const lastChar = lastNumber[lastNumber.length - 1];
                        const withoutLast = lastNumber.slice(0, lastNumber.length - 1);
                        if (withoutLast[0] === '0' || (withoutLast > 25 && withoutLast < 100) || withoutLast.length === 3) {
                            ipsArr[ipsArr.length - 1] = withoutLast + separator + lastChar;
                            url = protocol + '//' + ipsArr.join('.');
                            if (port) {
                                url += port;
                            }
                        }
                    }
                }
            }
            return {
                url,
                errorMessage: "",
            };
        });
    }

    handleFieldChange = (key) => (evt) => {
        this.setState({
            [key]: evt.target.value.trim(),
            errorMessage: "",
        });
    };

    handleCheckboxToggle = (key) => (evt) => {
        this.setState({
            [key]: evt.target.checked,
        });
    }

    renderAuthFields() {
        const { useAuth, username, password } = this.state;
        if (!useAuth) return null;

        return (<>
            <div>
                <Field
                    type="text"
                    label={_t("Username")}
                    value={username}
                    onChange={this.handleFieldChange("username")}
                />
            </div>
            <div>
                <Field
                    type="password"
                    label={_t("Password")}
                    value={password}
                    onChange={this.handleFieldChange("password")}
                />
            </div>
        </>);
    }

    renderError() {
        if (!this.state.errorMessage) {
            return null;
        }
        return <div className="error" role="alert">{_t(this.state.errorMessage)}</div>;
    }

    render() {
        const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
        return (
            <BaseDialog
                className="nv_ConnectProxyDialog"
                title={_t('Set up proxy settings')}
                hasCancel={true}
                onFinished={this.props.onFinished}
                contentId='mx_Dialog_content'
            >
                <div>
                    <div>
                        <Field
                            type="text"
                            label={_t("Proxy URL")}
                            value={this.state.url}
                            onChange={this.handleUrlFieldChange}
                        />
                    </div>
                    <div>
                        <StyledCheckbox
                            checked={this.state.useAuth}
                            onChange={this.handleCheckboxToggle("useAuth")}
                        >
                            { _t('Private proxy') }
                        </StyledCheckbox>
                    </div>
                    {this.renderAuthFields()}
                    {this.renderError()}
                    <div className="mx_Dialog_buttons">
                        <StyledCheckbox
                            checked={this.state.rememberMe}
                            onChange={this.handleCheckboxToggle("rememberMe")}
                        >
                            { _t('Remember on this device') }
                        </StyledCheckbox>

                        <AccessibleButton
                            disabled={this.state.loading}
                            kind="primary"
                            onClick={this.handleSubmit}
                        >
                            { _t('Connect to proxy') }
                        </AccessibleButton>
                    </div>
                </div>
            </BaseDialog>
        );
    }
}

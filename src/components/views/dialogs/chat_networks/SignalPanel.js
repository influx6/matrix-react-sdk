/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PhoneNumber from 'awesome-phonenumber';
import QRCode from 'qrcode.react';
import spinner from '../../../../../res/img/spinner.gif';
import get from 'lodash/get';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class Signal extends React.Component {
    state = {
        loading: false,
        bridgeStarted: false,
        signalCode: null,
        error: null,
    };

    startBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/signal/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.setState({ loading: false, bridgeStarted: true });
        await this.link();
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/signal/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.props.setConnected(undefined);
        this.setState({ loading: false, bridgeStarted: false });
    };

    componentDidMount = () => {
        // this.link();
        // if (!get(this.props.bridgeData, 'signal.conn.is_logged_in') && !get(this.props.bridgeData, 'signal.has_session')) {
        // }
    };

    link = async () => {
        const res = await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/signal/_matrix/provision/v1/api/link?user_id=${this.props.userId}`,
            {
                method: 'POST',
                body: JSON.stringify({ device_name: 'Beeper' }),
                headers: { Authorization: `Bearer ${this.props.loginToken}` },
            }
        );
        if (res.status !== 200) {
            return this.setState({ error: `HTTP Error status ${res.status}` });
        }
        const { uri } = await res.json();
        this.setState({ signalCode: uri });
        const waitRes = await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/signal/_matrix/provision/v1/api/link/wait?user_id=${this.props.userId}`,
            {
                method: 'POST',
                headers: { Authorization: `Bearer ${this.props.loginToken}` },
            }
        );
        if (waitRes.status !== 200) {
            return this.setState({ error: `QR Wait HTTP error status ${waitRes.status}` });
        }
        const json = await waitRes.json();
        console.log({ json });
        this.setState({ loading: true });
        await this.props.whoami();
        this.setState({ loading: false });
    };

    logout = async () => {
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/signal/_matrix/provision/v1/api/logout?user_id=${this.props.userId}`,
            {
                method: 'POST',
                headers: { Authorization: `Bearer ${this.props.loginToken}` },
            }
        );
        this.setState({ loading: true });
        await this.props.whoami();
        this.setState({ loading: false });
    }

    renderLogin = () => {
        const { error, bridgeStarted, signalCode } = this.state;
        if (!bridgeStarted) {
            return (
                <>
                    <h2>Log in to Signal</h2>
                    <input type="submit" className="login signal" value="Sign In" onClick={this.startBridge} />
                </>
            );
        }
        return (
            <div>
                <h2>Link Signal to Beeper</h2>
                {/*typeof connected === 'undefined' && (
                    <>
                        <p>
                            Your Signal bridge is running but is not connected. You may need to login to Signal again to continue receiving
                            messages.
                        </p>

                        <span
                            style={{
                                margin: '20px 10px 39px 20px',
                                display: 'inline-block',
                                fontSize: 12,
                            }}
                        >
                            Not using Signal?
                            <a style={{ textDecoration: 'underline' }} className="shutdown-bridge" onClick={this.stopBridge}>
                                Dismiss
                            </a>
                            .
                        </span>
                        <hr />
                    </>
                )*/}
                <ol style={{ paddingLeft: 30 }}>
                    <li>Open Signal app on your smartphone.</li>
                    <li>Then open Settings.</li>
                    <li>Click 'Linked devices'.</li>
                    <li>Click 'Link New Device'.</li>
                    <li>Scan QR code.</li>
                </ol>
                <div style={{ paddingLeft: 60, paddingTop: 30 }}>
                    {error ? (
                        <p className="error-pill">{error}</p>
                    ) : signalCode ? (
                        <QRCode size={256} value={signalCode} />
                    ) : (
                        <img src={spinner} height="24" width="24" alt="" className="spinner" />
                    )}
                </div>
            </div>
        );
    };

    render = () => {
        const { bridgeData } = this.props;
        const { loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }

        // different from `this.props.connected` -- this signifies if signal web was logged in
        // elsewhere and needs the user to manually reconnect here
        const signalData = get(bridgeData, 'signal');
        return signalData ? (
            <>
                <h2>Signal Status</h2>
                <p>
                    You are logged into Signal as{' '}
                    <strong>{new PhoneNumber(bridgeData.signal.number).getNumber('national')}</strong>
                    {/*!connected && (
                        <span>
                            {' '}
                            but you have logged in elsewhere. Click <strong>reconnect</strong> below to continue receiving messages on
                            Beeper
                        </span>
                    )*/}
                    .
                </p>
                {/*!connected && <button onClick={this.reconnect}>Reconnect</button>*/}
                <button onClick={this.logout}>Sign Out</button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

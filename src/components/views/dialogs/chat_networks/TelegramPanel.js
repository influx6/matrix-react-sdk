/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PhoneNumber from 'awesome-phonenumber';
import spinner from '../../../../../res/img/spinner.gif';

const LOGIN_STEP_START = "request";
const LOGIN_STEP_VERIFY_CODE = "code";
const LOGIN_STEP_TWO_FACTOR = "password";
const LOGIN_STEP_LOGGED_IN = "logged-in";
const KNOWN_STATES = new Set([LOGIN_STEP_START, LOGIN_STEP_VERIFY_CODE, LOGIN_STEP_TWO_FACTOR, LOGIN_STEP_LOGGED_IN]);

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class Telegram extends React.Component {
    state = {
        loading: false,
        loginStep: LOGIN_STEP_START,
        error: false,
        phoneNumber: '+1',
        verificationCode: '',
        twoFactorPassword: '',
    };

    startBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/telegram/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.setState({ loading: false });
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/telegram/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.props.setConnected(undefined);
        this.setState({ loading: false });
    };

    componentDidUpdate = () => {
        if (this.input) this.input.focus();
    };

    login = async e => {
        e.preventDefault();
        const { phoneNumber } = this.state;

        if (typeof this.props.connected === 'undefined') await this.startBridge();

        if (!phoneNumber) {
            return this.setState({ error: 'Please enter a phone number' });
        }

        if (!new PhoneNumber(phoneNumber.startsWith('+') ? phoneNumber : '+' + phoneNumber).isValid()) {
            return this.setState({ error: 'Your phone number is invalid. Did you include the country code? e.g. prefix with "+1"' });
        }

        const { userId, username, loginToken } = this.props;

        await window
            .fetch(`https://${username}.user.beeperhq.com/telegram/_matrix/provision/v1/user/${userId}/login/request_code`, {
                method: 'POST',
                body: JSON.stringify({ phone: this.state.phoneNumber }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
    };

    verifyCode = e => {
        e.preventDefault();
        const { userId, username, loginToken } = this.props;
        window
            .fetch(`https://${username}.user.beeperhq.com/telegram/_matrix/provision/v1/user/${userId}/login/send_code`, {
                method: 'POST',
                body: JSON.stringify({ code: this.state.verificationCode }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
    };

    sendPassword = e => {
        e.preventDefault();
        const { userId, username, loginToken } = this.props;
        window
            .fetch(`https://${username}.user.beeperhq.com/telegram/_matrix/provision/v1/user/${userId}/login/send_password`, {
                method: 'POST',
                body: JSON.stringify({ password: this.state.twoFactorPassword }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
    };

    handleLoginResponse = async res => {
        const { state, error, ...rest } = await res.json();
        if (KNOWN_STATES.has(state)) {
            this.setState({ loginStep: state, error });
            if (state === LOGIN_STEP_LOGGED_IN) {
                await this.props.whoami();
            }
        } else if (error) {
            this.setState({ error });
        } else {
            console.error("Unknown response data:", res.status, { state, error, ...rest });
            this.setState({ error: `Unknown response data (HTTP ${res.status})` });
        }
    }

    handleLoginError = err => {
        console.error("Fatal error in handler:", err);
        this.setState({ error: `Fatal error in handler: ${err}`});
    }

    logout = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://${this.props.username}.user.beeperhq.com/telegram/_matrix/provision/v1/user/${this.props.userId}/logout`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.loginToken}`,
            },
        });
        this.setState({ loginStep: LOGIN_STEP_START, loading: false });
        this.props.setConnected(false);
    };

    renderLogin = () => {
        const { loginStep, phoneNumber, error } = this.state;
        const { connected } = this.props;
        const phone = new PhoneNumber(phoneNumber.startsWith('+') ? phoneNumber : '+' + phoneNumber);
        const formattedPhoneNumber = phone.isValid() ? phone.getNumber('international') : phoneNumber;

        return (
            <>
                <h2>Log in to Telegram</h2>
                {connected === false && (
                    <>
                        <p>
                            Your Telegram bridge is running but is not connected. You may need to login to Telegram again to continue
                            receiving messages.
                        </p>
                        <hr />
                    </>
                )}
                {typeof connected === 'undefined' ? (
                    <>
                        <input type="submit" className="login telegram" value="Sign In" onClick={this.startBridge} />
                    </>
                ) : loginStep === LOGIN_STEP_START ? (
                    <>
                        <p>To connect Beeper to Telegram, please enter your phone number.</p>
                        <p>You'll receive a code through SMS or an existing Telegram client and you can input it here to sign in.</p>
                        {error && <p className="error-pill">{error}</p>}
                        <form onSubmit={this.login}>
                            <input
                                ref={n => (this.input = n)}
                                onChange={e => this.setState({ phoneNumber: e.target.value.replace(/[^0-9+]/g, '') })}
                                value={formattedPhoneNumber}
                                className="input telegram"
                                placeholder="Phone Number (with Country Code)"
                            />
                            <input type="submit" className="login telegram" value="Sign In" />
                            <a className="shutdown-bridge" onClick={this.stopBridge}>
                                I'm not using Telegram
                            </a>
                        </form>
                    </>
                ) : loginStep === LOGIN_STEP_VERIFY_CODE ? (
                    <>
                        <p>Please enter the verification code you received on your phone</p>
                        {error && <p className="error-pill">{error}</p>}
                        <form onSubmit={this.verifyCode}>
                            <input
                                ref={n => (this.input = n)}
                                value={this.state.verificationCode}
                                onChange={e => this.setState({ verificationCode: e.target.value.replace(/[^0-9]/g, '') })}
                                className="input telegram"
                                placeholder="Verification code"
                            />
                            <input type="submit" className="login telegram" value="Verify" />
                        </form>
                    </>
                ) : loginStep === LOGIN_STEP_TWO_FACTOR ? (
                    <>
                        <p>Please enter your two-factor authentication password</p>
                        {error && <p className="error-pill">{error}</p>}
                        <form onSubmit={this.sendPassword}>
                            <input
                                ref={n => (this.input = n)}
                                value={this.state.twoFactorPassword}
                                onChange={e => this.setState({ twoFactorPassword: e.target.value })}
                                className="input telegram"
                                placeholder="Password"
                                type="password"
                            />
                            <input type="submit" className="login telegram" value="Verify" />
                        </form>
                    </>
                ) : (
                    <>
                        <p>Logging you in...</p>
                    </>
                )}
            </>
        );
    };

    render = () => {
        const { connected, bridgeData } = this.props;
        const { loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }
        return connected === true ? (
            <>
                <h2>Telegram Status</h2>
                <p>
                    You are connected to Telegram as{' '}
                    <strong>
                        <span>
                            {bridgeData.telegram.first_name} {bridgeData.telegram.last_name}
                        </span>
                        <span className="telegram-phone">{new PhoneNumber('+' + bridgeData.telegram.phone).getNumber('national')}</span>
                    </strong>
                    .
                </p>
                <button className="logout telegram" onClick={this.logout}>
                    Sign out
                </button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

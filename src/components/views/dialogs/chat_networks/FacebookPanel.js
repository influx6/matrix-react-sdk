/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import spinner from '../../../../../res/img/spinner.gif';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const LOGIN_STEP_START = "start"
const LOGIN_STEP_2FA = "two-factor"
const LOGIN_STEP_DONE = "logged-in"

export default class Facebook extends React.Component {
    state = {
        loading: false,
        loginStep: LOGIN_STEP_START,
        email: '',
        password: '',
        code: '',
        error: null,
        waitingFor2FA: false,
    };

    constructor(props) {
        super(props);
        this.checkDeviceApprovedTimeout = null;
    }

    startBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/facebook/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.setState({ loading: false });
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/facebook/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.props.setConnected(undefined);
        this.setState({ loading: false });
    };

    handleLoginResponse = async res => {
        let status, error
        try {
            ({ status, error } = await res.json());
        } catch (err) {
            console.error("Failed to parse JSON from login response:", err);
            this.setState({ error: `Invalid response data (HTTP ${res.status})` });
            return;
        }
        if (status === LOGIN_STEP_2FA) {
            this.setState({
                loginStep: LOGIN_STEP_2FA,
            });
            this.scheduleCheckDeviceApproved();
        } else if (status === LOGIN_STEP_DONE) {
            this.setState({ loading: true });
            await this.props.whoami();
            this.setState({ loading: false });
        } else if (error) {
            this.setState({ error });
        } else {
            console.error("Unknown response data:", res.status, { status, error, ...respData });
            this.setState({ error: `Unknown response data (HTTP ${res.status})` });
        }
    }

    handleLoginError = err => {
        console.error("Fatal error in handler:", err);
        this.setState({ error: `Fatal error in handler: ${err}`});
    }

    login = async e => {
        e.preventDefault();
        if (typeof this.props.connected === 'undefined') await this.startBridge();
        this.setState({ loading: true });
        const { userId, username, loginToken } = this.props;
        await window
            .fetch(`https://${username}.user.beeperhq.com/facebook/public/api/login?user_id=${userId}`, {
                method: 'POST',
                body: JSON.stringify({ email: this.state.email, password: this.state.password }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
        this.setState({ loading: false });
    };

    verify2FA = async e => {
        e.preventDefault();
        clearTimeout(this.checkDeviceApprovedTimeout);
        const { userId, username, loginToken } = this.props;
        await window
            .fetch(`https://${username}.user.beeperhq.com/facebook/public/api/login/2fa?user_id=${userId}`, {
                method: 'POST',
                body: JSON.stringify({ email: this.state.email, code: this.state.code }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
    };

    verifyApproved = async () => {
        const { userId, username, loginToken } = this.props;
        await window
            .fetch(`https://${username}.user.beeperhq.com/facebook/public/api/login/approved?user_id=${userId}`, {
                method: 'POST',
                body: '{}',
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
    };

    scheduleCheckDeviceApproved = () => {
        this.checkDeviceApprovedTimeout = setTimeout(() => {
            this.checkDeviceApproved().catch(err => {
                console.error("Fatal error checking if login was approved:", err);
                this.setState({ error: `Error checking if login was approved: ${err}`});
            });
        }, 5000);
    }

    checkDeviceApproved = async () => {
        if (!this.state.loginStep === LOGIN_STEP_2FA || this.state.loading) {
            return;
        }
        const { userId, username, loginToken } = this.props;
        const resp = await window
            .fetch(`https://${username}.user.beeperhq.com/facebook/public/api/login/check_approved?user_id=${userId}`, {
                method: 'GET',
                headers: { authorization: `Bearer ${loginToken}` },
            });
        const { approved } = await resp.json();
        if (approved) {
            this.setState({ loading: true });
            await this.verifyApproved();
            this.setState({ loading: false });
        } else if (this.state.loginStep === LOGIN_STEP_2FA || this.state.loading) {
            this.scheduleCheckDeviceApproved();
        }
    }

    logout = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://${this.props.username}.user.beeperhq.com/facebook/public/api/logout?user_id=${this.props.userId}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.loginToken}`,
            },
        });
        this.setState({ loading: false });
        this.props.setConnected(false);
    };

    renderLogin = () => {
        const { connected } = this.props;
        const { loginStep, error } = this.state;

        if (typeof connected === 'undefined') {
            return <>
                <h2>Log in to Facebook Messenger</h2>
                <p>To connect Beeper to Facebook Messenger, please log in.</p>
                <button onClick={this.startBridge} className="login facebook">
                    Sign In
                </button>
            </>;
        } else if (loginStep === LOGIN_STEP_START) {
            return <>
                <h2>Log in to Facebook Messenger</h2>
                <p>To connect Beeper to Facebook Messenger, please log in.</p>
                {error && <p className="error-pill">{error}</p>}
                <form onSubmit={this.login}>
                    <input
                        autoFocus
                        onChange={e => this.setState({ email: e.target.value })}
                        value={this.state.email}
                        className="input facebook"
                        placeholder="email"
                        style={{ marginBottom: 10 }}
                    />
                    <input
                        type="password"
                        onChange={e => this.setState({ password: e.target.value })}
                        value={this.state.password}
                        className="input facebook"
                        placeholder="password"
                    />
                    <input type="submit" className="login facebook" value="Sign In" />
                    <a className="shutdown-bridge" onClick={this.stopBridge}>
                        I'm not using Messenger
                    </a>
                </form>
            </>
        } else if (loginStep === LOGIN_STEP_2FA) {
            return <>
                <h2>Log in to Facebook Messenger</h2>
                <p>
                    You have two-factor authentication enabled. You can either enter the code you
                    got in your authenticator app or SMS, or approve the login from another device
                    logged into Facebook or Messenger.
                </p>
                {error && <p className="error-pill">{error}</p>}
                <form onSubmit={this.verify2FA}>
                    <input
                        autoFocus
                        onChange={e => this.setState({ code: e.target.value.replace(/[^0-9]/g, '') })}
                        className="input facebook"
                        placeholder="2FA Code"
                    />
                    <input type="submit" className="login facebook" value="Verify" />
                </form>
            </>;
        } else {
            return <>
                <h2>Messenger Status</h2>
                <p>
                    Your Facebook bridge connection is not working. <b>Please try toggling the slider on the left to
                    fix this problem.</b> If that does not work, you may need to click the Reconnect button.
                </p>
                {error && <p className="error-pill">{error}</p>}
                {/* TODO: this is wrong */}
                <button onClick={this.openAuthWindow} className="login facebook">
                    Reconnect
                </button>
                <a className="shutdown-bridge" onClick={this.stopBridge}>
                    I'm not using Messenger
                </a>
            </>;
        }
    };

    render = () => {
        const { connected, bridgeData } = this.props;
        const { loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }
        console.log({ connected });
        if (connected === 502) {
            return (
                <>
                    <h2>Messenger Communication Error</h2>
                    <p>We couldn't reach your home bridgebox. Please make sure its powered on and connected to WiFi.</p>
                </>
            );
        }
        return connected === true ? (
            <>
                <h2>Messenger Status</h2>
                <p>
                    You are connected to Messenger as{' '}
                    <strong>
                        <span>{bridgeData.facebook.name}</span>
                    </strong>
                    .
                </p>
                <p>The Beeper client will show up in Facebook security settings as a {bridgeData.facebook.device_displayname}.</p>
                <button className="logout" onClick={this.logout}>
                    Logout
                </button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

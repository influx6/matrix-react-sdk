/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import isElectron from 'is-electron';
import spinner from '../../../../../res/img/spinner.gif';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class Hangouts extends React.Component {
    state = {
        loading: false,
    };

    startBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/hangouts/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.setState({ loading: false });
        this.login();
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/hangouts/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.setState({ loading: false });
        this.props.setConnected(undefined);
    };

    componentDidMount = () => {
        if (isElectron()) {
            window.ipcRenderer.on('bridgeAuthReply', this.onBridgeAuthReply);
        }
    };

    componentWillUnmount() {
        if (isElectron()) {
            window.ipcRenderer.removeListener('bridgeAuthReply', this.onBridgeAuthReply);
        }
    }

    login = async () => {
        const startRes = await window
            .fetch(`https://${this.props.username}.user.beeperhq.com/hangouts/login/api/start?manual=true&user_id=${this.props.userId}`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${this.props.loginToken}`,
                },
            })
            .then(res => res.json());

        window.ipcRenderer.send('bridgeAuth', {
            userAgent: 'Beeper/1.0',
            url: startRes.manual_auth_url,
            domain: 'accounts.google.com',
            cookie_keys: ['oauth_code'],
        });
    };

    onBridgeAuthReply = async (_ev, payload) => {
        window.ipcRenderer.send('closeAuthWindow');
        if (this.loggingIn) return;
        this.setState({ loading: true });
        this.loggingIn = true;
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/hangouts/login/api/authorization?manual=true&user_id=${this.props.userId}`,
            {
                method: 'POST',
                body: JSON.stringify({ authorization: payload.cookies.oauth_code }),
                headers: {
                    Authorization: `Bearer ${this.props.loginToken}`,
                    'Content-Type': 'application/json',
                },
            }
        );
        await this.props.whoami();
        this.setState({ loading: false });
        await setPTimeout(3000); //prevent additional 'bridgeAuthReply' events from triggering a login for 3 seconds
        this.loggingIn = false;
    };

    logout = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://${this.props.username}.user.beeperhq.com/hangouts/login/api/logout?user_id=${this.props.userId}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.loginToken}`,
            },
        });
        this.props.setConnected(false);
        this.setState({ loading: false });
    };

    renderLogin = () => {
        const { connected } = this.props;
        if (!isElectron()) return <p>Signing in requires the Beeper desktop app</p>;

        if (typeof connected === 'undefined') {
            return (
                <>
                    <h2>Log in to Hangouts</h2>
                    <p>To connect Beeper to Hangouts, please log in.</p>
                    <button onClick={this.startBridge} className="login hangouts">
                        Log In
                    </button>
                </>
            );
        }

        return (
            <>
                <h2>Hangouts Status</h2>
                <p>
                    Your Hangouts bridge is running but is not connected. You may need to login to Hangouts again to continue receiving
                    messages.
                </p>
                <button onClick={this.login} className="login hangouts">
                    Reconnect
                </button>
                <a className="shutdown-bridge" onClick={this.stopBridge}>
                    I'm not using Hangouts
                </a>
            </>
        );
    };

    render = () => {
        if (!isElectron()) return <p>Please use the Beeper desktop app to connect Hangouts</p>;
        const { connected, bridgeData } = this.props;
        const { loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }
        return connected === true ? (
            <>
                <h2>Hangouts Status</h2>
                <p>
                    You are connected to Hangouts as{' '}
                    <strong>
                        <span>{bridgeData.hangouts.name}</span>
                    </strong>
                    .
                </p>
                <button className="logout" onClick={this.logout}>
                    Logout
                </button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

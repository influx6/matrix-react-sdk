/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import isElectron from 'is-electron';
import spinner from '../../../../../res/img/spinner.gif';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class Twitter extends React.Component {
    state = {
        loading: false,
    };

    startBridge = async () => {
        this.startingBridge = true;
        await window.fetch(`https://api.beeperhq.com/bridge/twitter/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.startingBridge = false;
        if (this.bridgeAuthPayload) {
            this.onBridgeAuthReply(null, this.bridgeAuthPayload);
        }
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/twitter/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.setState({ loading: false });
        this.props.setConnected(undefined);
    };

    componentDidMount = () => {
        if (isElectron()) {
            window.ipcRenderer.on('bridgeAuthReply', this.onBridgeAuthReply);
        }
    };

    componentWillUnmount() {
        if (isElectron()) {
            window.ipcRenderer.removeListener('bridgeAuthReply', this.onBridgeAuthReply);
        }
    }

    onBridgeAuthReply = async (_ev, payload) => {
        window.ipcRenderer.send('closeAuthWindow');
        if (this.loggingIn) return;
        if (this.startingBridge) {
            this.bridgeAuthPayload = payload;
            return;
        }
        this.setState({ loading: true });
        this.loggingIn = true;
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/twitter/_matrix/provision/v1/api/login?user_id=${this.props.userId}`,
            {
                method: 'POST',
                body: JSON.stringify({
                    auth_token: payload.cookies.auth_token,
                    csrf_token: payload.cookies.ct0,
                }),
                headers: {
                    Authorization: `Bearer ${this.props.loginToken}`,
                    'Content-Type': 'application/json',
                },
            }
        );
        await this.props.whoami();
        this.setState({ loading: false });
        await setPTimeout(3000); //prevent additional 'bridgeAuthReply' events from triggering a login for 3 seconds
        this.loggingIn = false;
    };

    openAuthWindow = () => {
        if (typeof this.props.connected === 'undefined') this.startBridge();
        window.ipcRenderer.send('bridgeAuth', {
            url: 'https://twitter.com/login',
            domain: 'twitter.com',
            cookie_keys: ['auth_token', 'ct0'],
        });
    };

    logout = async () => {
        this.setState({ loading: true });
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/twitter/_matrix/provision/v1/api/logout?user_id=${this.props.userId}`,
            {
                method: 'POST',
                headers: { Authorization: `Bearer ${this.props.loginToken}` },
            }
        );
        this.props.setConnected(false);
        await this.props.whoami();
        this.setState({ loading: false });
    };

    renderLogin = () => {
        const { connected } = this.props;
        if (!isElectron()) return <p>Signing in requires the Beeper desktop app</p>;

        if (typeof connected === 'undefined') {
            return (
                <>
                    <h2>Log in to Twitter</h2>
                    <p>To connect Beeper to Twitter, please log in.</p>
                    <button onClick={this.openAuthWindow} className="login facebook">
                        Log In
                    </button>
                </>
            );
        }

        return (
            <>
                <h2>Twitter Status</h2>
                <p>
                    Your Twitter bridge is running but is not connected. You may need to login to Twitter again to continue receiving
                    messages.
                </p>
                <button onClick={this.openAuthWindow} className="login facebook">
                    Reconnect
                </button>
                <a className="shutdown-bridge" onClick={this.stopBridge}>
                    I'm not using Twitter
                </a>
            </>
        );
    };

    render = () => {
        if (!isElectron()) return <p>Please use the Beeper desktop app to connect Twitter</p>;
        const { connected, bridgeData } = this.props;
        const { loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }
        return connected === true ? (
            <>
                <h2>Twitter Status</h2>
                <div className="twitter-banner" style={{ backgroundImage: `url(${bridgeData.twitter.profile_banner_url})` }}>
                    <div
                        className="twitter-profile"
                        style={{
                            backgroundImage: `url(${bridgeData.twitter.profile_image_url_https.replace('normal.jpg', '400x400.jpg')})`,
                        }}
                    ></div>
                </div>
                <p>
                    You are connected to Twitter as <strong>{bridgeData.twitter.name}</strong>{' '}
                    <span className="twitter-screen-name">@{bridgeData.twitter.screen_name}</span>.
                </p>
                <button className="logout" onClick={this.logout}>
                    Sign Out
                </button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PhoneNumber from 'awesome-phonenumber';
import QRCode from 'qrcode.react';
import spinner from '../../../../../res/img/spinner.gif';
import get from 'lodash/get';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class WhatsApp extends React.Component {
    state = {
        loading: false,
        whatsAppCode: null,
        error: null,
    };

    startBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/whatsapp/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.login();
        this.setState({ loading: false });
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/whatsapp/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.props.setConnected(undefined);
        this.setState({ loading: false });
    };

    componentDidUpdate = () => {
        if (this.input) this.input.focus();
    };

    componentDidMount = () => {
        if (!get(this.props.bridgeData, 'whatsapp.conn.is_logged_in') && !get(this.props.bridgeData, 'whatsapp.has_session')) {
            this.login();
        }
    };

    login = async () => {
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/whatsapp/_matrix/provision/v1/delete_session?user_id=${this.props.userId}`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${this.props.loginToken}`,
                },
            }
        );
        const webSocket = new WebSocket(
            `wss://${this.props.username}.user.beeperhq.com/whatsapp/_matrix/provision/v1/login?user_id=${this.props.userId}`,
            ['net.maunium.whatsapp.login', `net.maunium.whatsapp.auth-${this.props.loginToken}`]
        );
        webSocket.onmessage = event => {
            const data = JSON.parse(event.data);
            console.log({ data }, this.login);
            if (data.success === false) {
                if (data.errcode === 'login timed out') {
                    this.login();
                    return;
                } else {
                    return this.setState({ error: data.error });
                }
            } else if (data.success === true) {
                this.props.whoami();
            }
            this.setState({ whatsAppCode: data.code });
        };
    };

    logout = async () => {
        this.setState({ loading: true });
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/whatsapp/_matrix/provision/v1/logout?user_id=${this.props.userId}`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${this.props.loginToken}`,
                },
            }
        );
        this.setState({ loading: false });
        this.props.setConnected(false);
        await this.props.whoami();
        this.login();
    };

    reconnect = async () => {
        this.setState({ loading: true });
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/whatsapp/_matrix/provision/v1/reconnect?user_id=${this.props.userId}`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${this.props.loginToken}`,
                },
            }
        );
        this.setState({ loading: false });
        this.props.whoami(false);
    };

    renderLogin = () => {
        const { error, whatsAppCode } = this.state;
        const { connected } = this.props;
        if (typeof connected === 'undefined') {
            return (
                <>
                    <h2>Log in to WhatsApp</h2>
                    <input type="submit" className="login whatsapp" value="Sign In" onClick={this.startBridge} />
                </>
            );
        }
        return (
            <div>
                <h2>Log in to WhatsApp</h2>
                {typeof connected === 'undefined' && (
                    <>
                        <p>
                            Your Whatsapp bridge is running but is not connected. You may need to login to WhatsApp again to continue
                            receiving messages.
                        </p>

                        <span
                            style={{
                                margin: '20px 10px 39px 20px',
                                display: 'inline-block',
                                fontSize: 12,
                            }}
                        >
                            Not using WhatsApp?
                            <a style={{ textDecoration: 'underline' }} className="shutdown-bridge" onClick={this.stopBridge}>
                                Dismiss
                            </a>
                            .
                        </span>
                        <hr />
                    </>
                )}
                <p>To connect Beeper to WhatsApp, please log in.</p>

                <ol style={{ paddingLeft: 30 }}>
                    <li>Open WhatsApp on your phone</li>
                    <li>
                        Tap Menu{' '}
                        <svg height="24px" viewBox="0 0 24 24" width="24px">
                            <rect fill="#f2f2f2" height="24" rx="3" width="24"></rect>
                            <path
                                d="m12 15.5c.825 0 1.5.675 1.5 1.5s-.675 1.5-1.5 1.5-1.5-.675-1.5-1.5.675-1.5 1.5-1.5zm0-2c-.825 0-1.5-.675-1.5-1.5s.675-1.5 1.5-1.5 1.5.675 1.5 1.5-.675 1.5-1.5 1.5zm0-5c-.825 0-1.5-.675-1.5-1.5s.675-1.5 1.5-1.5 1.5.675 1.5 1.5-.675 1.5-1.5 1.5z"
                                fill="#818b90"
                            ></path>
                        </svg>{' '}
                        or Settings{' '}
                        <svg width="24" height="24" viewBox="0 0 24 24">
                            <rect fill="#F2F2F2" width="24" height="24" rx="3"></rect>
                            <path
                                d="M9.34 8.694a4.164 4.164 0 0 0-1.606 3.289c0 1.338.63 2.528 1.61 3.292l-1.46 2.527a7.065 7.065 0 0 1-3.052-5.82c0-2.41 1.206-4.54 3.048-5.816l1.46 2.528zm6.713 2.859c-.217-2.079-1.992-3.739-4.148-3.739-.578 0-1.128.116-1.628.329L8.819 5.617a7.042 7.042 0 0 1 3.086-.704c3.76 0 6.834 2.958 7.059 6.64h-2.91zm-1.065.43a3.083 3.083 0 1 1-6.166 0 3.083 3.083 0 0 1 6.166 0zm-6.164 6.364l1.458-2.523a4.153 4.153 0 0 0 1.623.322 4.154 4.154 0 0 0 4.12-3.524h2.922a7.062 7.062 0 0 1-7.042 6.426c-1.105 0-2.15-.25-3.081-.7zm11.197-7.21a7.88 7.88 0 0 0-.404-1.824l.286-.12a.527.527 0 0 0-.403-.973l-.29.12a8.176 8.176 0 0 0-1.197-1.77l.231-.23a.526.526 0 1 0-.744-.744l-.234.234a8.17 8.17 0 0 0-1.775-1.18l.13-.31a.526.526 0 1 0-.972-.403l-.12.313a8.463 8.463 0 0 0-1.995-.405v-.35A.533.533 0 0 0 12 2.97a.533.533 0 0 0-.535.526v.338a8.02 8.02 0 0 0-2.173.416l-.13-.313a.526.526 0 0 0-.972.402l.129.311a8.171 8.171 0 0 0-1.775 1.18l-.235-.235a.526.526 0 0 0-.743.744l.23.231A8.175 8.175 0 0 0 4.6 8.34l-.29-.12a.526.526 0 0 0-.403.971l.285.122a7.882 7.882 0 0 0-.404 1.824h-.322a.533.533 0 0 0-.526.534c0 .29.235.535.526.535h.28c.02.831.166 1.624.418 2.378l-.257.1a.523.523 0 1 0 .402.968l.252-.105a8.17 8.17 0 0 0 1.191 1.797l-.187.187a.526.526 0 1 0 .744.743l.184-.183a8.173 8.173 0 0 0 1.792 1.208l-.096.231a.526.526 0 1 0 .972.403l.096-.23c.698.24 1.436.387 2.208.428v.243c0 .29.244.526.535.526.29 0 .534-.235.534-.526v-.253a8.012 8.012 0 0 0 2.03-.417l.09.229a.523.523 0 1 0 .967-.403l-.096-.231a8.172 8.172 0 0 0 1.792-1.208l.184.183a.526.526 0 1 0 .743-.744l-.187-.186a8.174 8.174 0 0 0 1.191-1.798l.252.104a.526.526 0 1 0 .403-.971l-.257-.095a8.074 8.074 0 0 0 .417-2.378h.281c.29 0 .526-.244.526-.535a.533.533 0 0 0-.526-.534h-.323z"
                                fill="#818B90"
                            ></path>
                        </svg>{' '}
                        and select WhatsApp Web
                    </li>
                    <li>Log out of all other WhatsApp Web sessions. They may interfere with Beeper!</li>
                    <li>Point your phone to this screen to capture the code</li>
                </ol>

                <div style={{ paddingLeft: 60, paddingTop: 30 }}>
                    {error ? (
                        <p className="error-pill">{error}</p>
                    ) : whatsAppCode ? (
                        <QRCode size={256} value={whatsAppCode} />
                    ) : (
                        <img src={spinner} height="24" width="24" alt="" className="spinner" />
                    )}
                </div>
            </div>
        );
    };

    render = () => {
        const { bridgeData } = this.props;
        const { loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }

        // different from `this.props.connected` -- this signifies if whatsapp web was logged in
        // elsewhere and needs the user to manually reconnect here
        const hasSession = get(bridgeData, 'whatsapp.has_session');
        const connected = get(bridgeData, 'whatsapp.conn.is_connected');
        const loggedIn = get(bridgeData, 'whatsapp.conn.is_logged_in');
        return hasSession === true ? (
            <>
                <h2>WhatsApp Status</h2>
                <p>
                    You are {connected ? 'connected' : 'logged in'} to WhatsApp as{' '}
                    <strong>{new PhoneNumber('+' + bridgeData.whatsapp.jid.split('@')[0]).getNumber('national')}</strong>
                    {!connected && (
                        <span>
                            {' '}
                            but you have logged in elsewhere. Click <strong>reconnect</strong> below to continue receiving messages on
                            Beeper
                        </span>
                    )}
                    .
                </p>
                <button onClick={this.reconnect}>Reconnect</button>
                <button onClick={this.logout}>Sign Out</button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

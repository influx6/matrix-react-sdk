/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import spinner from '../../../../../res/img/spinner.gif';
import get from 'lodash/get';

const LOGIN_STEP_START = "start";
const LOGIN_STEP_2FA = "two-factor";
const LOGIN_STEP_CHALLENGE = "checkpoint";
const LOGIN_DONE = "logged-in";

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class Instagram extends React.Component {
    state = {
        loading: false,
        loginStep: LOGIN_STEP_START,
        error: false,
        phoneNumber: '',
        last4Phone: '',
        verificationCode: '',
    };

    startBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/instagram/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.setState({ loading: false });
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/instagram/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.props.setConnected(undefined);
        this.setState({ loading: false });
    };

    handleLoginResponse = async res => {
        let status, error, respData
        try {
            ({ status, error, ...respData } = await res.json());
        } catch (err) {
            console.error("Failed to parse JSON from login response:", err);
            this.setState({ error: `Invalid response data (HTTP ${res.status})` });
            return;
        }
        if (error) {
            this.setState({ error });
        } else if (status === LOGIN_STEP_2FA) {
            this.setState({
                loginStep: LOGIN_STEP_2FA,
                last4Phone: get(respData, 'response.two_factor_info.obfuscated_phone_number'),
                isTotp: get(respData, 'response.two_factor_info.totp_two_factor_on'),
                login2faIdentifier: get(respData, 'response.two_factor_info.two_factor_identifier'),
            });
        } else if (status === LOGIN_STEP_CHALLENGE) {
            this.setState({ loginStep: LOGIN_STEP_CHALLENGE });
        } else if (status === LOGIN_DONE) {
            await this.props.whoami();
        } else {
            console.error("Unknown response data:", res.status, { status, error, ...respData });
            this.setState({ error: `Unknown response data (HTTP ${res.status})` });
        }
    }

    handleLoginError = err => {
        console.error("Fatal error in handler:", err);
        this.setState({ error: `Fatal error in handler: ${err}`});
    }

    login = async e => {
        e.preventDefault();
        if (typeof this.props.connected === 'undefined') await this.startBridge();
        this.setState({ loading: true });
        const { userId, username, loginToken } = this.props;
        await window
            .fetch(`https://${username}.user.beeperhq.com/instagram/_matrix/provision/v1/api/login?user_id=${userId}`, {
                method: 'POST',
                body: JSON.stringify({ username: this.state.username, password: this.state.password }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
        this.setState({ loading: false });
    };

    verify2FA = e => {
        e.preventDefault();
        const { userId, username, loginToken } = this.props;
        window
            .fetch(`https://${username}.user.beeperhq.com/instagram/_matrix/provision/v1/api/login/2fa?user_id=${userId}`, {
                method: 'POST',
                body: JSON.stringify({
                    username: this.state.username,
                    code: this.state.verificationCode,
                    '2fa_identifier': this.state.login2faIdentifier,
                    is_totp: this.state.isTotp,
                }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
    };

    verifyChallenge = e => {
        e.preventDefault();
        const { userId, username, loginToken } = this.props;
        window
            .fetch(`https://${username}.user.beeperhq.com/instagram/_matrix/provision/v1/api/login/checkpoint?user_id=${userId}`, {
                method: 'POST',
                body: JSON.stringify({
                    username: this.state.username,
                    code: this.state.verificationCode,
                }),
                headers: { authorization: `Bearer ${loginToken}` },
            })
            .then(this.handleLoginResponse)
            .catch(this.handleLoginError);
    };

    logout = async () => {
        this.setState({ loading: true });
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/instagram/_matrix/provision/v1/api/logout?user_id=${this.props.userId}`,
            {
                method: 'POST',
                headers: { Authorization: `Bearer ${this.props.loginToken}` },
            }
        );
        this.setState({ loginStep: LOGIN_STEP_START, loading: false });
        this.props.setConnected(false);
    };

    renderLogin = () => {
        const { loginStep, username, password, error, last4Phone } = this.state;
        const { connected } = this.props;

        return (
            <>
                <h2>Log in to Instagram</h2>
                {connected === false && (
                    <>
                        <p>
                            Your Instagram bridge is running but is not connected. You may need to login to Instagram again to continue
                            receiving messages.
                        </p>
                        <hr />
                    </>
                )}
                {error && <p className="error-pill">{error}</p>}
                {typeof connected === 'undefined' ? (
                    <>
                        <input type="submit" className="login instagram" value="Sign In" onClick={this.startBridge} />
                    </>
                ) : loginStep === LOGIN_STEP_START ? (
                    <>
                        <p>To connect Beeper to Instagram, please login.</p>
                        <form onSubmit={this.login}>
                            <input
                                autoFocus
                                onChange={e => this.setState({ username: e.target.value })}
                                value={username}
                                className="input instagram"
                                placeholder="username"
                                style={{ marginBottom: 10 }}
                            />
                            <input
                                type="password"
                                onChange={e => this.setState({ password: e.target.value })}
                                value={password}
                                className="input instagram"
                                placeholder="password"
                            />
                            <input type="submit" className="login instagram" value="Sign In" />
                            <a className="shutdown-bridge" onClick={this.stopBridge}>
                                I'm not using Instagram
                            </a>
                        </form>
                    </>
                ) : loginStep === LOGIN_STEP_2FA ? (
                    <>
                        <p>Please enter the verification code you received on your phone number ending in {last4Phone}</p>
                        <form onSubmit={this.verify2FA}>
                            <input
                                autoFocus
                                onChange={e => this.setState({ verificationCode: e.target.value.replace(/[^0-9]/g, '') })}
                                className="input instagram"
                                placeholder="2FA Code"
                            />
                            <input type="submit" className="login instagram" value="Verify" />
                        </form>
                    </>
                ) : loginStep === LOGIN_STEP_CHALLENGE ? (
                    <>
                        <p>Please enter the challenge code you received on email or another device</p>
                        <form onSubmit={this.verifyChallenge}>
                            <input
                                autoFocus
                                onChange={e => this.setState({ verificationCode: e.target.value.replace(/[^0-9]/g, '') })}
                                className="input instagram"
                                placeholder="Challenge Code"
                            />
                            <input type="submit" className="login instagram" value="Verify" />
                        </form>
                    </>
                ) : (
                    <>
                        <p>Logging you in...</p>
                    </>
                )}
            </>
        );
    };

    render = () => {
        const { connected, bridgeData } = this.props;
        const { loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }
        return connected === true ? (
            <>
                <h2>Instagram Status</h2>
                <p>
                    You are connected to Instagram as{' '}
                    <strong>
                        <span>{bridgeData.instagram.full_name}</span>
                        <span className="instagram-username">({bridgeData.instagram.username})</span>.
                    </strong>
                </p>
                <p><b>Heads up!</b> Your Beeper server is located in Germany. Instagram will send you an email asking if you just logged in from a {bridgeData.instagram.device_displayname}. This is actually your Beeper client logging in.</p>
                <button className="logout instagram" onClick={this.logout}>
                    Sign out
                </button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

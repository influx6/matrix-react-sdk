/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import isElectron from 'is-electron';
import spinner from '../../../../../res/img/spinner.gif';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class Slack extends React.Component {
    state = {};

    componentDidMount = () => {
        if (isElectron()) {
            window.ipcRenderer.on('bridgeAuthReply', this.onBridgeAuthReply);
        }
    };

    componentWillUnmount() {
        if (isElectron()) {
            window.ipcRenderer.removeListener('bridgeAuthReply', this.onBridgeAuthReply);
        }
    }

    startBridge = async () => {
        this.startingBridge = true;
        await window.fetch(`https://api.beeperhq.com/bridge/slack/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.startingBridge = false;
        if (this.bridgeAuthData) {
            this.link(this.bridgeAuthData);
        }
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/slack/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.props.setConnected(undefined);
        this.setState({ loading: false });
    };

    onBridgeAuthReply = async (_ev, payload) => {
        if (payload.storage) this.storage_localConfig_v2 = payload.storage.localConfig_v2;
        if (payload.cookies) this.cookie_d = payload.cookies.d;
        if (!this.storage_localConfig_v2 || !this.cookie_d) return;
        window.ipcRenderer.send('closeAuthWindow');
        const teams = JSON.parse(this.storage_localConfig_v2).teams;
        let token;
        const cookie = this.cookie_d;
        for (const team in teams) {
            token = teams[team].token;
        }
        const bridgeAuthData = { token, cookie };
        if (this.startingBridge) {
            this.bridgeAuthData = bridgeAuthData;
            return;
        }
        this.link(bridgeAuthData);
    };

    link = async bridgeAuthData => {
        if (this.loggingIn) return;
        this.loggingIn = true;
        this.setState({ loading: true });
        await window.fetch(`https://${this.props.username}.user.beeperhq.com/slack/_matrix/provision/v1/link?user_id=${this.props.userId}`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
                authorization: `Bearer ${this.props.loginToken}`,
            },
            body: JSON.stringify({ data: bridgeAuthData }),
        });

        // Poll for new user info for a few seconds
        await this.props.whoami();
        for (let i = 0; i <= 60; i++) {
            const lenKnown = this.props.bridgeData.puppets.map(puppet => puppet.data.self && puppet.data.self.name).filter(Boolean).length;
            if (lenKnown === this.props.bridgeData.puppets.length) break;
            await this.props.whoami();
            await setPTimeout(1000);
        }
        this.setState({ loading: false });
        await setPTimeout(3000); //prevent additional 'bridgeAuthReply' events from triggering a login for 3 seconds
        this.loggingIn = false;
    };

    openAuthWindow = () => {
        if (typeof this.props.connected === 'undefined') this.startBridge();
        window.ipcRenderer.send('bridgeAuth', {
            url: `https://app.slack.com/client`,
            storage_keys: { localConfig_v2: null },
            cookie_keys: ['d'],
            wait_for_update: true,
        });
    };

    unlink = async puppetId => {
        this.setState({ unlinking: puppetId });
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/slack/_matrix/provision/v1/${puppetId}/unlink?user_id=${this.props.userId}`,
            { method: 'POST', headers: { authorization: `Bearer ${this.props.loginToken}` } }
        );
        this.props.whoami();
    };

    renderLogin = () => {
        const { connected } = this.props;
        if (!isElectron()) return <p>Signing in requires the Beeper desktop app</p>;

        if (typeof connected === 'undefined') {
            return (
                <>
                    <h2>Log in to Slack</h2>
                    <p>To connect Beeper to Slack, please log in.</p>
                    <button onClick={this.openAuthWindow} className="login slack">
                        Log In
                    </button>
                </>
            );
        }

        return (
            <>
                <h2>Slack Status</h2>
                <p>
                    Your Slack bridge is running but is not connected. You may need to login to Slack again to continue receiving messages.
                </p>
                <button onClick={this.openAuthWindow} className="login slack">
                    Reconnect
                </button>
                <a className="shutdown-bridge" onClick={this.stopBridge}>
                    I'm not using Slack
                </a>
            </>
        );
    };

    render = () => {
        if (!isElectron()) return <p>Please use the Beeper desktop app to connect Slack</p>;
        const { connected, bridgeData } = this.props;
        const { unlinking, loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }
        return connected === true ? (
            <>
                <h2>Slack Status</h2>
                <p><b>Note:</b> This bridge connection is in beta. Slack does not support encryption and your messages will be stored unencrypted on Beeper. We are working to add encryption to this bridge.</p>
                <div className="puppet-list">
                    {bridgeData.puppets.map(puppet => (
                        <div className="puppet" key={puppet.puppetId}>
                            {puppet.data.team && puppet.data.self && unlinking !== puppet.puppetId ? (
                                <>
                                    <strong>{puppet.data.team.name}</strong>
                                    <span className="slack-username">{puppet.data.self.name}</span>
                                </>
                            ) : (
                                <span>Loading profile...</span>
                            )}
                            <button className="logout" onClick={() => this.unlink(puppet.puppetId)}>
                                Sign Out
                            </button>
                        </div>
                    ))}
                </div>
                <button className="login slack" onClick={this.openAuthWindow}>
                    Add Another Account
                </button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

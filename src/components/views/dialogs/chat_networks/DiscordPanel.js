/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import isElectron from 'is-electron';
import spinner from '../../../../../res/img/spinner.gif';

function setPTimeout(ms /*: number */) /*: Promise<any> */ {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default class Discord extends React.Component {
    state = {
        loading: false,
    };

    componentDidMount = () => {
        if (isElectron()) {
            window.ipcRenderer.on('bridgeAuthReply', this.onBridgeAuthReply);
        }
    };

    componentWillUnmount() {
        if (isElectron()) {
            window.ipcRenderer.removeListener('bridgeAuthReply', this.onBridgeAuthReply);
        }
    }

    startBridge = async () => {
        this.startingBridge = true;
        await window.fetch(`https://api.beeperhq.com/bridge/discord/start`, {
            method: 'POST',
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
        });
        let whoamiStatus = 0;
        while (whoamiStatus === 'error' || whoamiStatus < 200 || whoamiStatus > 299) {
            await setPTimeout(500);
            whoamiStatus = (await this.props.whoami()).status;
        }
        this.startingBridge = false;
        if (this.bridgeAuthPayload) {
            this.onBridgeAuthReply(null, this.bridgeAuthPayload);
        }
    };

    stopBridge = async () => {
        this.setState({ loading: true });
        await window.fetch(`https://api.beeperhq.com/bridge/discord/stop`, {
            headers: { Authorization: `Bearer ${this.props.accessToken}` },
            method: 'POST',
        });
        this.props.setConnected(undefined);
        this.setState({ loading: false });
    };

    onBridgeAuthReply = async (_ev, payload) => {
        window.ipcRenderer.send('closeAuthWindow');
        if (this.loggingIn) return;
        if (this.startingBridge) {
            this.bridgeAuthPayload = payload;
            return;
        }
        if (!payload || !payload.storage || !payload.storage.token) return this.setState({ error: 'Unable to grab Discord token' });
        this.loggingIn = true;
        this.setState({ loading: true });
        await window.fetch(`https://${this.props.username}.user.beeperhq.com/discord/_matrix/provision/v1/link?user_id=${this.props.userId}`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
                authorization: `Bearer ${this.props.loginToken}`,
            },
            body: JSON.stringify({ data: { token: JSON.parse(payload.storage.token), bot: false } }),
        });

        // Poll for new user info for a few seconds
        await this.props.whoami();
        for (let i = 0; i <= 60; i++) {
            const lenKnown = this.props.bridgeData.puppets.map(puppet => puppet.data.username).filter(Boolean).length;
            if (lenKnown === this.props.bridgeData.puppets.length) break;
            await this.props.whoami();
            await setPTimeout(1000);
        }
        this.setState({ loading: false });
        await setPTimeout(3000); //prevent additional 'bridgeAuthReply' events from triggering a login for 3 seconds
        this.loggingIn = false;
    };

    openAuthWindow = () => {
        if (typeof this.props.connected === 'undefined') this.startBridge();
        window.ipcRenderer.send('bridgeAuth', {
            url: `https://discord.com/login`,
            storage_keys: { token: null },
        });
    };

    unlink = async puppetId => {
        this.setState({ unlinking: puppetId });
        await window.fetch(
            `https://${this.props.username}.user.beeperhq.com/discord/_matrix/provision/v1/${puppetId}/unlink?user_id=${this.props.userId}`,
            { method: 'POST', headers: { authorization: `Bearer ${this.props.loginToken}` } }
        );
        this.props.whoami();
    };

    renderLogin = () => {
        const { connected } = this.props;
        if (!isElectron()) return <p>Please use the Beeper Desktop app to connect Discord</p>;

        if (typeof connected === 'undefined') {
            return (
                <>
                    <h2>Log in to Discord</h2>
                    <p>To connect Beeper to Discord, please log in.</p>
                    {/* TODO spin up bridge */}
                    <button onClick={this.openAuthWindow} className="login discord">
                        Log In
                    </button>
                </>
            );
        }

        return (
            <>
                <h2>Discord Status</h2>
                <p>
                    Your Discord bridge is running but is not connected. You may need to login to Discord again to continue receiving
                    messages.
                </p>
                <button onClick={this.openAuthWindow} className="login discord">
                    Reconnect
                </button>
                <a className="shutdown-bridge" onClick={this.stopBridge}>
                    I'm not using Discord
                </a>
            </>
        );
    };

    render = () => {
        const { connected, bridgeData } = this.props;
        const { unlinking, loading } = this.state;
        if (loading) {
            return <img src={spinner} height="24" width="24" alt="" className="spinner" />;
        }
        return connected === true ? (
            <>
                <h2>Discord Status</h2>

                <p><b>Note:</b> This bridge connection is in beta. Discord does not support encryption and your messages will be stored unencrypted on Beeper. We are working to add encryption to this bridge.</p>
                <p>Only direct messages on Discord are bridged by default. To add servers and channels, please contact Beeper support (ie Eric).</p>
                <div className="puppet-list">
                    {bridgeData.puppets.map(puppet => (
                        <div className="puppet" key={puppet.puppetId}>
                            {puppet.data.username && unlinking !== puppet.puppetId ? (
                                <strong>{puppet.data.username}</strong>
                            ) : (
                                <span>Loading profile...</span>
                            )}

                            <button className="logout" onClick={() => this.unlink(puppet.puppetId)}>
                                Sign Out
                            </button>
                        </div>
                    ))}
                </div>
                <button className="login discord" onClick={this.openAuthWindow}>
                    Add Another Account
                </button>
            </>
        ) : (
            this.renderLogin()
        );
    };
}

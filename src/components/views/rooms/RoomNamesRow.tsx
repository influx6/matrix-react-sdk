/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as React from "react";
import {Room} from "matrix-js-sdk/src/models/room";
import classNames from 'classnames';
import {NOTIFICATION_STATE_UPDATE} from "../../../stores/notifications/NotificationState";
import {DefaultTagID, TagID} from "../../../stores/room-list/models";
import {sanitizedHtmlNode} from "../../../HtmlUtils";
import {ListNotificationState} from "../../../stores/notifications/ListNotificationState";
import DMRoomMap from "../../../utils/DMRoomMap";
import * as Unread from "../../../Unread";

interface IRoomNamesRowProps {
    notification: ListNotificationState;
    rooms: Room[],
    offset: number,
    tagId: TagID,
    selected: Room[],
}

export default class RoomNamesRow extends React.Component<IRoomNamesRowProps> {
    private maxRooms: number = 4;
    constructor(props: IRoomNamesRowProps) {
        super(props);
        this.props.notification.on(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
    }

    public componentWillUnmount() {
        if (this.props.notification) {
            this.props.notification.off(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
        }
    }

    public componentDidUpdate(prevProps: Readonly<IRoomNamesRowProps>) {
        if (prevProps.notification) {
            prevProps.notification.off(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
        }

        this.props.notification.on(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
    }

    private onNotificationUpdate = () => {
        this.forceUpdate(); // notification state changed - update
    };

    private renderUnread(): React.ReactElement {
        const {notification, offset, tagId} = this.props;
        const rooms = this.props.rooms.slice(offset, offset + this.maxRooms);
        const result = [];
        for (const room of rooms) {
            const roomName = !!DMRoomMap.shared().getUserIdForRoomId(room.roomId) ? room.name.split(' ')[0] : room.name;
            const isUnreadRoom = tagId === DefaultTagID.Invite || Unread.doesRoomHaveUnreadMessages(room);
            const tag = isUnreadRoom ? 'b' : 'span';
            result.push(`<${tag}>${roomName}</${tag}>`);
        }
        let roomsNamesHTML = result.join(', ');

        const missedRooms = this.props.rooms.slice(0, offset);
        const missedCount = missedRooms
            .filter((room) => tagId === DefaultTagID.Invite || Unread.doesRoomHaveUnreadMessages(room))
            .length;
        const unreadCount = notification.count - missedCount;
        if (unreadCount >= this.maxRooms) {
            roomsNamesHTML = `<b>Unread ${unreadCount}: </b>` + roomsNamesHTML;
        }
        return sanitizedHtmlNode(roomsNamesHTML);
    }

    private renderSelected(): React.ReactElement {
        const {offset, tagId, selected} = this.props;
        const missedRooms = this.props.rooms.slice(0, offset);
        const missedCount = missedRooms.filter((room) => this.props.selected.includes(room)).length;
        if (missedCount === this.props.selected.length) {
            // no selected here, show unread
            return this.renderUnread();
        }
        const rooms = this.props.rooms.slice(offset, offset + this.maxRooms);
        const result = [];
        for (const room of rooms) {
            const roomName = !!DMRoomMap.shared().getUserIdForRoomId(room.roomId) ? room.name.split(' ')[0] : room.name;
            const isSelectedRoom = this.props.selected.includes(room);
            const tag = isSelectedRoom ? 'b' : 'span';
            result.push(`<${tag}>${roomName}</${tag}>`);
        }
        const selectedCount = this.props.selected.length - missedCount;
        const roomsNamesHTML = `<b>Selected ${selectedCount}: </b>` + result.join(', ');
        return sanitizedHtmlNode(roomsNamesHTML);
    }

    public render(): React.ReactElement {
        const classes = classNames({
            'nv_RoomSublist2_restRoomsSection': true,
            'nv_RoomSublist2_restRoomsSection_inconsiderable': this.props.tagId === DefaultTagID.Archived || this.props.tagId === DefaultTagID.LowPriority,
        });
        return (
            <div className={classes}>
                {this.props.selected.length ? this.renderSelected() : this.renderUnread()}
            </div>
        );
    }
}

/*
Copyright 2017 New Vector Ltd
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import dis from '../../../dispatcher/dispatcher';
import * as sdk from '../../../index';
import RoomViewStore from '../../../stores/RoomViewStore';
import PropTypes from "prop-types";
import {RoomPermalinkCreator} from "../../../utils/permalinks/Permalinks";
import {MatrixClientPeg} from "../../../MatrixClientPeg";
import {_t} from "../../../../lib/languageHandler";
import ReplyThread from "../elements/ReplyThread";
import {decryptFile} from "../../../utils/DecryptFile";

function cancelQuoting() {
    dis.dispatch({
        action: 'reply_to_event',
        event: null,
    });
}

function shouldDecryptThumb(event) {
    return event.getType() === 'm.room.sticker' || ['m.image', 'm.video'].includes(event.getContent().msgtype);
}

export default class ReplyPreview extends React.Component {
    static propTypes = {
        permalinkCreator: PropTypes.instanceOf(RoomPermalinkCreator).isRequired,
    };

    constructor(props) {
        super(props);
        this.unmounted = false;

        this.state = {
            event: RoomViewStore.getQuotingEvent(),
            thumbnailUrl: null,
        };

        this._onRoomViewStoreUpdate = this._onRoomViewStoreUpdate.bind(this);
        this._roomStoreToken = RoomViewStore.addListener(this._onRoomViewStoreUpdate);
    }

    componentDidMount() {
        this._decryptThumb();
    }


    componentWillUnmount() {
        this.unmounted = true;
        if (this.state.thumbnailUrl) {
            URL.revokeObjectURL(this.state.thumbnailUrl);
        }
        // Remove RoomStore listener
        if (this._roomStoreToken) {
            this._roomStoreToken.remove();
        }
    }

    async _decryptThumb() {
        const { event } = this.state;
        if (!event || !shouldDecryptThumb(event)) {
            return;
        }
        const content = event.getContent();
        let file = content.info && content.info.thumbnail_file ? content.info.thumbnail_file : content.file;
        if (!file) {
            return;
        }
        try {
            const blob = await decryptFile(file);
            const thumbnailUrl = URL.createObjectURL(blob);
            this.setState({ thumbnailUrl })
        } catch (err) {
            console.warn("Unable to decrypt attachment: ", err);
        }
    }

    _getThumbUrl() {
        const { thumbnailUrl, event } = this.state;
        const content = event.getContent();
        if (thumbnailUrl) {
            return thumbnailUrl;
        } else if (content.info && content.info.thumbnail_url) {
            return MatrixClientPeg.get().mxcUrlToHttp(content.info.thumbnail_url);
        } else {
            return MatrixClientPeg.get().mxcUrlToHttp(content.url);
        }
    }

    _onRoomViewStoreUpdate() {
        if (this.unmounted) return;

        const event = RoomViewStore.getQuotingEvent();
        if (this.state.event !== event) {
            if (this.state.thumbnailUrl) {
                URL.revokeObjectURL(this.state.thumbnailUrl);
            }
            this.setState({
                event,
                thumbnailUrl: null,
            }, () => {
                this._decryptThumb();
            });
        }
    }

    onShowReplyToClicked = (e) => {
        e.preventDefault();
        dis.dispatch({
            action: 'scroll_to_event',
            event_id: this.state.event.getId(),
            room_id: this.state.event.getRoomId(),
            highlighted: true,
        });
    }

    render() {
        const { event } = this.state;
        if (!event) return null;

        const SenderProfile = sdk.getComponent('messages.SenderProfile');
        const content = event.getContent();

        let thumbSrc = null;
        let previewText;
        switch (content.msgtype) {
            case 'm.image':
                previewText = _t('Image');
                thumbSrc = this._getThumbUrl();
                break;
            case 'm.video':
                previewText = _t('Video');
                thumbSrc = this._getThumbUrl();
                break;
            case 'm.file':
                thumbSrc = require("../../../../res/themes/nova-light/img/icon-media-file.svg");
                previewText = _t('File');
                break;
            case 'm.audio':
                thumbSrc = require("../../../../res/themes/nova-light/img/icon-media-audio.svg");
                previewText = _t('Audio');
                break;
            default:
                previewText = ReplyThread.stripPlainReply(content.body);
        }

        return <div className="mx_ReplyPreview">
            <div className="mx_ReplyPreview_wrapper" onClick={this.onShowReplyToClicked}>
                {thumbSrc && <div className="mx_ReplyPreview_thumbnail">
                    {content.msgtype === 'm.video' ?
                        (<video src={thumbSrc} width={36} height={36} />) :
                        (<img src={thumbSrc} width={36} height={36} />)
                    }
                </div>}
                <div className="mx_ReplyPreview_content">
                    <SenderProfile mxEvent={event} enableFlair={true} />
                    <span className="mx_ReplyPreview_contentText">{previewText}</span>
                </div>
            </div>
            <div className="mx_ReplyPreview_cancel">
                <img className="mx_filterFlipColor" src={require("../../../../res/themes/nova-light/img/icon-close-dialog.svg")} onClick={cancelQuoting} />
            </div>
        </div>;
    }
}

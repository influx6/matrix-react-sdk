/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from 'classnames';
import {_t} from '../../../languageHandler';
import AccessibleButton from "../elements/AccessibleButton";
import Tooltip from "../elements/Tooltip";

const bridgesTitles = {
    'facebook': 'Facebook Messenger',
    'hangouts': 'Google Hangouts',
    'telegram': 'Telegram',
    'whatsapp': 'WhatsApp',
    'instagram': 'Instagram',
    'twitter': 'Twitter',
    'slack': 'Slack',
    'androidmessages': 'Android Messages',
    'signal': 'Signal',
    'discord': 'Discord',
    'skype': 'Skype',
};

const BridgedIcon = ({protocol, className, size, onClick, hideTooltip}) => {
    const [hover, setHover] = useState(false);
    if (!protocol) {
        protocol = 'matrix';
    }
    const classes = classNames({
        'nv_BridgedIcon': true,
    }, className, `nv_BridgedIcon_${protocol}`);

    let style;
    if (size) {
        style = {width: `${size}px`, height: `${size}px`};
    }

    const onMouseOver = () => setHover(true);
    const onMouseOut = () => setHover(false);
    let tip;

    if (hover && !hideTooltip) {
        const label = protocol === 'matrix' ? _t('This is a chat on the Matrix network')
            : protocol === 'nova' ? _t('This is a chat on the Beeper network')
            : _t('This room is bridged with %(bridge)s', { bridge: bridgesTitles[protocol] });
        tip = <Tooltip label={label} />;
    }
    if (onClick) {
        return (
            <AccessibleButton
                onClick={onClick}
                className={classes}
                style={style}
                nMouseOver={onMouseOver}
                onMouseOut={onMouseOut}
            >
                { tip }
            </AccessibleButton>
        );
    }

    return <div className={classes} style={style} onMouseOver={onMouseOver} onMouseOut={onMouseOut}>
        { tip }
    </div>;
};

BridgedIcon.propTypes = {
    protocol: PropTypes.string,
    className: PropTypes.string,
    size: PropTypes.number,
    onClick: PropTypes.func,
};

export default BridgedIcon;

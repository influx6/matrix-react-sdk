/*
Copyright 2020 Nova Technology Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React, {createRef} from "react";
import {Room} from "matrix-js-sdk/src/models/room";
import classNames from "classnames";
import {RovingTabIndexWrapper} from "../../../accessibility/RovingTabIndex";
import AccessibleButton, {ButtonEvent} from "../../views/elements/AccessibleButton";
import dis from '../../../dispatcher/dispatcher';
import defaultDispatcher from '../../../dispatcher/dispatcher';
import ActiveRoomObserver from "../../../ActiveRoomObserver";
import {_t} from "../../../languageHandler";
import {
    ChevronFace,
    ContextMenu,
    MenuItem,
    MenuItemCheckbox,
    ContextMenuTooltipButton
} from "../../structures/ContextMenu";
import {DefaultTagID, TagID} from "../../../stores/room-list/models";
import {IPreview, MessagePreviewStore, ROOM_PREVIEW_CHANGED} from "../../../stores/room-list/MessagePreviewStore";
import {ALL_MESSAGES, MENTIONS_ONLY} from "../../../RoomNotifs";
import {MatrixClientPeg} from "../../../MatrixClientPeg";
import NotificationBadge from "./NotificationBadge";
import {Volume} from "../../../RoomNotifsTypes";
import {formatFullDate, formatDateByPassageOfTime} from '../../../DateUtils';
import RoomListStore from "../../../stores/room-list/RoomListStore";
import RoomListActions from "../../../actions/RoomListActions";
import {ActionPayload} from "../../../dispatcher/payloads";
import {RoomNotificationStateStore} from "../../../stores/notifications/RoomNotificationStateStore";
import {NOTIFICATION_STATE_UPDATE, NotificationState} from "../../../stores/notifications/NotificationState";
import {EchoChamber} from "../../../stores/local-echo/EchoChamber";
import {CachedRoomKey, RoomEchoChamber} from "../../../stores/local-echo/RoomEchoChamber";
import {PROPERTY_UPDATED} from "../../../stores/local-echo/GenericEchoChamber";
import RoomAvatar from "../avatars/RoomAvatar";
import BridgedIcon from "./BridgedIcon";
import IconizedContextMenu, {
    IconizedContextMenuCheckbox,
    IconizedContextMenuOption,
    IconizedContextMenuOptionList,
    IconizedContextMenuRadio,
} from "../context_menus/IconizedContextMenu";
import { CommunityPrototypeStore, IRoomProfile } from "../../../stores/CommunityPrototypeStore";
import { UPDATE_EVENT } from "../../../stores/AsyncStore";

interface IProps {
    room: Room;
    tagId: TagID;
    onTileClick: (ev: React.MouseEvent) => void;
    showMessagePreview: boolean;
    smallRow: boolean;
    isFavourite: boolean;
    selected?: boolean;
    inSearch?: boolean;
    onTagSelectedRoomsChanged?: (tagId: TagID) => void;
}

type PartialDOMRect = Pick<DOMRect, "left" | "bottom">;

interface IState {
    active: boolean;
    contextMenuPosition: PartialDOMRect;
    bridgedBy: string;
    messagePreview: IPreview | null;
}

const messagePreviewId = (roomId: string) => `mx_RoomTile_messagePreview_${roomId}`;

const contextMenuBelow = (elementRect: PartialDOMRect) => {
    // align the context menu's icons with the icon which opened the context menu
    const left = elementRect.left + window.pageXOffset - 9;
    const top = elementRect.bottom + window.pageYOffset + 17;
    const chevronFace = ChevronFace.None;
    return {left, top, chevronFace};
};

interface IRoomTileTimestampProps {
    ts: number;
    showTwelveHour: boolean;
}

const RoomTileTimestamp: React.FC<IRoomTileTimestampProps> = ({ts, showTwelveHour}) => {
    const date = new Date(ts);
    return (
        <span className="nv_MessageTimestamp" title={formatFullDate(date, showTwelveHour)} aria-hidden={true}>
            {formatDateByPassageOfTime(date, showTwelveHour)}
        </span>
    );
};

export default class RoomTile extends React.PureComponent<IProps, IState> {
    private dispatcherRef: string;
    private roomTileRef = createRef<HTMLDivElement>();
    private notificationState: NotificationState;
    private roomProps: RoomEchoChamber;

    constructor(props: IProps) {
        super(props);

        const bridgeProtocolId = this.props.room._nova_getBridgeInfo()?.getContent()?.protocol?.id;
        const members = Object.keys(this.props.room.currentState.members);
        const allNovaUsers = members.filter(userId => userId.endsWith(':beeperhq.com')).length === members.length;
        const bridgedBy = bridgeProtocolId || (allNovaUsers ? 'nova' : null);
        this.state = {
            active: ActiveRoomObserver.activeRoomId === this.props.room.roomId,
            contextMenuPosition: null,
            bridgedBy,
            // generatePreview() will return nothing if the user has previews disabled
            messagePreview: this.generatePreview(),
        };

        ActiveRoomObserver.addListener(this.props.room.roomId, this.onActiveRoomUpdate);
        this.dispatcherRef = defaultDispatcher.register(this.onAction);
        MessagePreviewStore.instance.on(ROOM_PREVIEW_CHANGED, this.onRoomPreviewChanged);
        this.notificationState = RoomNotificationStateStore.instance.getRoomState(this.props.room);
        this.notificationState.on(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
        this.roomProps = EchoChamber.forRoom(this.props.room);
        this.roomProps.on(PROPERTY_UPDATED, this.onRoomPropertyUpdate);
        CommunityPrototypeStore.instance.on(UPDATE_EVENT, this.onCommunityUpdate);
    }

    private onNotificationUpdate = () => {
        this.forceUpdate(); // notification state changed - update
    };

    private onRoomPropertyUpdate = (property: CachedRoomKey) => {
        if (property === CachedRoomKey.NotificationVolume) this.onNotificationUpdate();
        // else ignore - not important for this tile
    };

    private onRoomNameChange = (room) => {
        this.forceUpdate();
    }

    private get canShowContextMenu(): boolean {
        return this.props.tagId !== DefaultTagID.Invite;
    }

    private get showMessagePreview(): boolean {
        return this.props.showMessagePreview;
    }

    public componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>) {
        if (prevProps.showMessagePreview !== this.props.showMessagePreview && this.showMessagePreview) {
            this.setState({messagePreview: this.generatePreview()});
        }
    }

    public componentDidMount() {
        // when we're first rendered (or our sublist is expanded) make sure we are visible if we're active
        if (this.state.active) {
            this.scrollIntoView();
        }
        if (this.props.room) {
            this.props.room.on("Room.name", this.onRoomNameChange);
        }
    }

    public componentWillUnmount() {
        if (this.props.room) {
            this.props.room.removeListener("Room.name", this.onRoomNameChange);
            ActiveRoomObserver.removeListener(this.props.room.roomId, this.onActiveRoomUpdate);
        }
        defaultDispatcher.unregister(this.dispatcherRef);
        MessagePreviewStore.instance.off(ROOM_PREVIEW_CHANGED, this.onRoomPreviewChanged);
        this.notificationState.off(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
        CommunityPrototypeStore.instance.off(UPDATE_EVENT, this.onCommunityUpdate);
    }

    private onAction = (payload: ActionPayload) => {
        if (payload.action === "view_room" && payload.room_id === this.props.room.roomId && payload.show_room_tile) {
            setImmediate(() => {
                this.scrollIntoView();
            });
        }
    };

    private onCommunityUpdate = (roomId: string) => {
        if (roomId !== this.props.room.roomId) return;
        this.forceUpdate(); // we don't have anything to actually update
    };

    private onRoomPreviewChanged = (room: Room) => {
        if (this.props.room && room.roomId === this.props.room.roomId) {
            // generatePreview() will return nothing if the user has previews disabled
            this.setState({messagePreview: this.generatePreview()});
        }
    };

    private generatePreview(): IPreview | null {
        if (!this.showMessagePreview) {
            return null;
        }
        return MessagePreviewStore.instance.getPreviewForRoom(this.props.room, this.props.tagId);
    }

    private scrollIntoView = () => {
        if (!this.roomTileRef.current) return;
        this.roomTileRef.current.scrollIntoView({
            block: "nearest",
            behavior: "auto",
        });
    };

    private onActiveRoomUpdate = (isActive: boolean) => {
        this.setState({active: isActive});
    };

    private onContextMenu = (ev: React.MouseEvent) => {
        // If we don't have a context menu to show, ignore the action.
        if (!this.canShowContextMenu) return;

        ev.preventDefault();
        ev.stopPropagation();
        this.setState({
            contextMenuPosition: {
                left: ev.clientX,
                bottom: ev.clientY,
            },
        });
    };

    private onCloseContextMenu = () => {
        this.setState({contextMenuPosition: null});
    };

    private onTagRoom = (ev: ButtonEvent, tagId: TagID) => {
        ev.preventDefault();
        ev.stopPropagation();
        if (this.props.selected) {
            this.props.onTagSelectedRoomsChanged(tagId);
            this.onCloseContextMenu();
            return;
        }

        if (tagId !== DefaultTagID.Favourite && tagId !== DefaultTagID.LowPriority) {
            console.warn(`Unexpected tag ${tagId} applied to ${this.props.room.room_id}`);
            return;
        }

        const inverseTag = tagId === DefaultTagID.Favourite ? DefaultTagID.LowPriority : DefaultTagID.Favourite;
        const isApplied = RoomListStore.instance.getTagsForRoom(this.props.room).includes(tagId);
        const isInverseApplied = RoomListStore.instance.getTagsForRoom(this.props.room).includes(inverseTag);

        let removeTag = null;
        let addTag = tagId;
        if (isApplied) {
            removeTag = tagId;
            addTag = null;
        } else if (isInverseApplied) {
            removeTag = inverseTag;
        }

        dis.dispatch(RoomListActions.tagRoom(
            MatrixClientPeg.get(),
            this.props.room,
            removeTag,
            addTag,
            undefined,
            0
        ));
        if ([removeTag, addTag].includes(DefaultTagID.LowPriority)) {
            this.roomProps.notificationVolume = addTag === DefaultTagID.LowPriority ? MENTIONS_ONLY : ALL_MESSAGES;
        }
        this.onCloseContextMenu();
    };

    private onLeaveRoomClick = (ev: ButtonEvent) => {
        ev.preventDefault();
        ev.stopPropagation();

        dis.dispatch({
            action: 'leave_room',
            room_id: this.props.room.roomId,
        });
        this.setState({contextMenuPosition: null}); // hide the menu
    };

    private onForgetRoomClick = (ev: ButtonEvent) => {
        ev.preventDefault();
        ev.stopPropagation();

        dis.dispatch({
            action: 'forget_room',
            room_id: this.props.room.roomId,
        });
        this.onCloseContextMenu();
    };

    private async saveNotifState(ev: ButtonEvent, newState: Volume) {
        ev.preventDefault();
        ev.stopPropagation();
        if (MatrixClientPeg.get().isGuest()) return;

        this.roomProps.notificationVolume = newState;

        this.onCloseContextMenu();
        this.forceUpdate();
    }

    private onClickAllNotifs = ev => this.saveNotifState(ev, ALL_MESSAGES);
    private onClickMentions = ev => this.saveNotifState(ev, MENTIONS_ONLY);

    private onSelectChangeClicked = ev => {
        ev.preventDefault();
        ev.stopPropagation();
        if (this.props.selected) {
            dis.dispatch({
                action: 'RoomListActions.Selection.clear',
            });
            this.onCloseContextMenu();
            return;
        }
        const {room, tagId} = this.props;
        dis.dispatch({
            action: ev.shiftKey ? 'RoomListActions.Selection.multiplyToggle' : 'RoomListActions.Selection.toggle',
            room,
            tagId,
        });
        this.onCloseContextMenu();
    }

    private renderContextMenu(): React.ReactElement {
        if (!this.state.contextMenuPosition) return null;

        let selectionButton = null;
        if (!this.props.inSearch) {
            const isSelected = this.props.selected;
            const selectionText = isSelected ? _t("Clear selection") : _t("Select multiple");
            selectionButton = (
                <IconizedContextMenuOption
                    iconClassName="mx_RoomTile2_iconSignOut"
                    label={selectionText}
                    onClick={this.onSelectChangeClicked}
                />
            );
        }

        let content;
        if (this.props.tagId === DefaultTagID.Archived) {
            content = (
                <IconizedContextMenuOptionList>
                    {selectionButton}
                    <IconizedContextMenuOption
                        iconClassName="mx_RoomTile2_iconSignOut"
                        onClick={this.onForgetRoomClick}
                        label={_t("Forget Room")}
                        className="nv_RoomTile2_contextMenu_redRow"
                    />
                </IconizedContextMenuOptionList>
            );
        } else {
            const roomTags = RoomListStore.instance.getTagsForRoom(this.props.room);

            const isFavorite = roomTags.includes(DefaultTagID.Favourite);
            const favouriteLabelClassName = isFavorite ? "nv_RoomTile2_contextMenu_activeRow" : "";
            const favouriteLabel = isFavorite ? _t("Favourited") : _t("Favourite");

            const isMuted = this.roomProps.notificationVolume === MENTIONS_ONLY;
            const notifLabelClassName = isMuted ? "nv_RoomTile2_contextMenu_activeRow" : "";
            const notifLabel = isMuted ? _t("Muted") : _t("Mute");
            const onNotifChangeClick = isMuted ? this.onClickAllNotifs : this.onClickMentions;

            const isLowPriority = roomTags.includes(DefaultTagID.LowPriority);
            const lowPriorityLabelClassName = isLowPriority ? "nv_RoomTile2_contextMenu_activeRow" : "";

            content = (
                <React.Fragment>
                    <IconizedContextMenuOptionList>
                        {selectionButton}
                        <IconizedContextMenuCheckbox
                            onClick={(e) => this.onTagRoom(e, DefaultTagID.Favourite)}
                            active={isFavorite}
                            label={favouriteLabel}
                            className={favouriteLabelClassName}
                            iconClassName="nv_RoomTile2_iconStar"
                        />
                        <IconizedContextMenuCheckbox
                            onClick={(e) => this.onTagRoom(e, DefaultTagID.LowPriority)}
                            active={isLowPriority}
                            label={_t("Low Priority")}
                            className={lowPriorityLabelClassName}
                            iconClassName="nv_RoomTile2_iconArrowDown"
                        />
                        <IconizedContextMenuCheckbox
                            onClick={onNotifChangeClick}
                            active={isMuted}
                            label={notifLabel}
                            className={notifLabelClassName}
                            iconClassName="nv_RoomTile_iconBellCrossed"
                        />
                    </IconizedContextMenuOptionList>
                    <IconizedContextMenuOptionList>
                        <IconizedContextMenuCheckbox
                            onClick={this.onLeaveRoomClick}
                            label={_t("Leave Room")}
                            className="nv_RoomTile2_contextMenu_redRow"
                            iconClassName="nv_RoomTile2_iconSignOut"
                            active={true}
                        />
                    </IconizedContextMenuOptionList>
                </React.Fragment>
            );
        }
        return (
            <IconizedContextMenu
                {...contextMenuBelow(this.state.contextMenuPosition)}
                onFinished={this.onCloseContextMenu}
                className="mx_RoomTile2_contextMenu"
                compact
            >
                {content}
            </IconizedContextMenu>
        );
    }

    public render(): React.ReactElement {
        const classes = classNames({
            'nv_RoomTile2': true,
            'nv_RoomTile2_active': this.state.active,
            'nv_RoomTile2_selected': this.props.selected,
            'nv_RoomTile2_smallRow': this.props.smallRow,
            'mx_RoomTile2_hasMenuOpen': !!(this.state.contextMenuPosition),
        });

        let roomProfile: IRoomProfile = {displayName: null, avatarMxc: null};
        if (this.props.tagId === DefaultTagID.Invite) {
            roomProfile = CommunityPrototypeStore.instance.getInviteProfile(this.props.room.roomId);
        }
        let name = roomProfile.displayName || this.props.room.name;
        if (typeof name !== 'string') name = '';
        name = name.replace(":", ":\u200b"); // add a zero-width space to allow linewrapping after the colon

        let roomAvatar = null;
        if (this.props.smallRow) {
            const iconClasses = classNames({
                'nv_RoomTile2_bridgedIcon': true,
                'nv_RoomTile2_bridgedIcon_selected': this.props.selected,
            });
            roomAvatar = <BridgedIcon
                className={iconClasses}
                protocol={this.state.bridgedBy}
                size={24}
            />;
        } else {
            roomAvatar = (
                <div className="mx_DecoratedRoomAvatar">
                    <RoomAvatar
                        room={this.props.room}
                        width={42}
                        height={42}
                        oobData={({avatarUrl: roomProfile.avatarMxc})}
                    />
                    <BridgedIcon
                        className="nv_RoomTile2_bridgedIcon"
                        protocol={this.state.bridgedBy}
                    />
                </div>
            );
        }
        // aria-hidden because we summarise the unread count/highlight status in a manual aria-label below
        const badge = (
            <div className="nv_RoomTile2_badgeContainer" aria-hidden="true">
                <NotificationBadge
                    notification={this.notificationState}
                    isFavourite={this.props.isFavourite}
                />
            </div>
        );

        let messagePreview = null;
        let timestamp = null;
        if (this.showMessagePreview && this.state.messagePreview) {
            messagePreview = (
                <div className="nv_RoomTile2_messagePreview" id={messagePreviewId(this.props.room.roomId)}>
                    {this.state.messagePreview.readByAll && <img src={require("../../../../res/themes/nova-light/img/icon-received.svg")}/>}
                    {this.state.messagePreview.text}
                </div>
            );
            if (!this.props.smallRow) {
                timestamp = <RoomTileTimestamp showTwelveHour={true} ts={this.state.messagePreview.ts}/>;
            }
        }

        const nameClasses = classNames({
            "nv_RoomTile2_name": true,
            "nv_RoomTile2_nameWithPreview": !!messagePreview,
            "nv_RoomTile2_nameHasUnreadEvents": this.notificationState.isUnread,
        });

        let nameContainer = (
            <div className="nv_RoomTile2_content">
                <div className="nv_RoomTile2_nameContainer">
                    <div title={name} className={nameClasses} tabIndex={-1} dir="auto">
                        {name}
                    </div>
                    {timestamp}
                </div>
                {messagePreview}
            </div>
        );

        let ariaLabel = name;
        // The following labels are written in such a fashion to increase screen reader efficiency (speed).
        if (this.props.tagId === DefaultTagID.Invite) {
            // append nothing
        } else if (this.notificationState.hasMentions) {
            ariaLabel += " " + _t("%(count)s unread messages including mentions.", {
                count: this.notificationState.count,
            });
        } else if (this.notificationState.hasUnreadCount) {
            ariaLabel += " " + _t("%(count)s unread messages.", {
                count: this.notificationState.count,
            });
        } else if (this.notificationState.isUnread) {
            ariaLabel += " " + _t("Unread messages.");
        }

        let ariaDescribedBy: string;
        if (this.showMessagePreview) {
            ariaDescribedBy = messagePreviewId(this.props.room.roomId);
        }

        return (
            <React.Fragment>
                <RovingTabIndexWrapper inputRef={this.roomTileRef}>
                    {({onFocus, isActive, ref}) =>
                        <AccessibleButton
                            onFocus={onFocus}
                            tabIndex={isActive ? 0 : -1}
                            inputRef={ref}
                            className={classes}
                            onClick={this.props.onTileClick}
                            onContextMenu={this.onContextMenu}
                            role="treeitem"
                            aria-label={ariaLabel}
                            aria-selected={this.state.active}
                            aria-describedby={ariaDescribedBy}
                        >
                            {badge}
                            {roomAvatar}
                            {nameContainer}
                            {this.renderContextMenu()}
                        </AccessibleButton>
                    }
                </RovingTabIndexWrapper>
            </React.Fragment>
        );
    }
}

/*
Copyright 2020 Nova Technology Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import LazyRenderList from "../elements/LazyRenderList";
import * as React from "react";
import {createRef} from "react";
import {Room} from "matrix-js-sdk/src/models/room";
import {RovingTabIndexWrapper} from "../../../accessibility/RovingTabIndex";
import AccessibleButton from "../../views/elements/AccessibleButton";
import RoomTile from "./RoomTile";
import {NovaTagID, TagID} from "../../../stores/room-list/models";
import dis from "../../../dispatcher/dispatcher";
import {Key} from "../../../Keyboard";
import {objectExcluding, objectHasDiff} from "../../../utils/objects";
import RoomListSearchStore, {SEARCH_LISTS_UPDATE_EVENT} from "../../../stores/room-list/RoomListSearchStore";
import {LARGE_ROOM_TILE, SMALL_ROOM_TILE} from "./RoomList";

interface IProps {
    scrollTop: number;
    scrollerHeight: number;
    label: string;
    tagId: TagID;
    onResize: () => void;
    onHeaderClick?: () => void;
    scrollToTop: () => void;
}

interface IState {
    rooms: Room[];
}

export default class RoomSearchSublist extends React.Component<IProps, IState> {
    private headerButton = createRef<HTMLDivElement>();
    private sublistRef = createRef<HTMLDivElement>();

    constructor(props: IProps) {
        super(props);
        this.state = {
            rooms: RoomListSearchStore.instance.orderedLists[this.props.tagId] || [],
        };
        RoomListSearchStore.instance.on(SEARCH_LISTS_UPDATE_EVENT, this.onListsUpdated);
    }

    public shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>): boolean {
        if (objectHasDiff(this.props, nextProps)) {
            // Something we don't care to optimize has updated, so update.
            return true;
        }

        // Do the same check used on props for state, without the rooms we're going to no-op
        const prevStateNoRooms = objectExcluding(this.state, ['rooms']);
        const nextStateNoRooms = objectExcluding(nextState, ['rooms']);
        if (objectHasDiff(prevStateNoRooms, nextStateNoRooms)) {
            return true;
        }

        // Quickly double check we're not about to break something due to the number of rooms changing.
        if (this.state.rooms.length !== nextState.rooms.length) {
            return true;
        }

        // Finally, nothing happened so no-op the update
        return false;
    }

    public componentWillUnmount() {
        RoomListSearchStore.instance.off(SEARCH_LISTS_UPDATE_EVENT, this.onListsUpdated);
    }

    private onListsUpdated = () => {
        const currentRooms = this.state.rooms;
        const newRooms = RoomListSearchStore.instance.orderedLists[this.props.tagId] || [];

            this.setState({ rooms: newRooms}, () => {
                if (currentRooms.length !== newRooms.length) {
                    this.props.onResize();
                }
            });
    };

    private onHeaderClick = () => {
        const possibleSticky = this.headerButton.current;
        const isStickyHeader = possibleSticky.classList.contains("nv_RoomSublist2_sticky_active");
        if (isStickyHeader) {
            setImmediate(() => {
                this.props.scrollToTop();
            });
            return;
        }
    };

    private onHeaderKeyDown = (ev: React.KeyboardEvent) => {
        switch (ev.key) {
            case Key.ARROW_RIGHT: {
                if (this.sublistRef.current) {
                    // otherwise focus the first room
                    const element = this.sublistRef.current.querySelector(".nv_RoomTile2") as HTMLDivElement;
                    if (element) {
                        element.focus();
                    }
                }
                break;
            }
        }
    };

    private onKeyDown = (ev: React.KeyboardEvent) => {
        switch (ev.key) {
            // On ARROW_LEFT go to the sublist header
            case Key.ARROW_LEFT:
                ev.stopPropagation();
                this.headerButton.current.focus();
                break;
            // Consume ARROW_RIGHT so it doesn't cause focus to get sent to composer
            case Key.ARROW_RIGHT:
                ev.stopPropagation();
        }
    };

    private handleTileClick = (room: Room) => (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        dis.dispatch({
            action: 'view_room',
            show_room_tile: true, // make sure the room is visible in the list
            room_id: room.roomId,
            clear_search: ev && !ev.shiftKey,
        });
    };

    private renderHeader(): React.ReactElement {
        return (
            <RovingTabIndexWrapper inputRef={this.headerButton}>
                {({onFocus, isActive, ref}) => {
                    const tabIndex = isActive ? 0 : -1;

                    return (
                        <div className="nv_RoomSublist2_headerContainer" onKeyDown={this.onHeaderKeyDown} onFocus={onFocus} aria-label={this.props.label}>
                            <AccessibleButton
                                className="nv_RoomSublist2_sticky"
                                role="treeitem"
                                aria-level={1}
                                onClick={this.onHeaderClick}
                                onFocus={onFocus}
                                inputRef={ref}
                                tabIndex={tabIndex}
                            >
                                <div className="nv_RoomSublist2_LabelContainer">
                                    <span className="nv_RoomSublist2_Label">{this.props.label}</span>
                                </div>
                            </AccessibleButton>
                        </div>
                    );
                }}
            </RovingTabIndexWrapper>
        );
    }

    public render(): React.ReactElement {
        if (!this.state.rooms.length) {
            return null;
        }

        const smallRows = this.props.tagId === NovaTagID.Groups;
        return (
            <div
                ref={this.sublistRef}
                className="nv_RoomSublist2"
                role="group"
                aria-label={this.props.label}
                onKeyDown={this.onKeyDown}
            >
                {this.renderHeader()}
                <div className="nv_RoomSublist2_tiles">
                    <LazyRenderList
                        scrollTop={this.props.scrollTop}
                        height={this.props.scrollerHeight}
                        itemHeight={smallRows ? SMALL_ROOM_TILE : LARGE_ROOM_TILE}
                        renderItem={(room) => {
                            return <RoomTile
                                room={room}
                                key={`room-${room.roomId}`}
                                showMessagePreview={true}
                                isFavourite={false}
                                tagId={this.props.tagId}
                                smallRow={smallRows}
                                inSearch={true}
                                onTileClick={this.handleTileClick(room)}
                            />
                        }}
                        items={this.state.rooms}
                    />
                </div>
            </div>
        );
    }
}

/*
Copyright 2020 Nova Technology Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as React from "react";
import {createRef} from "react";
import {Room} from "matrix-js-sdk/src/models/room";
import classNames from 'classnames';
import {RovingTabIndexWrapper} from "../../../accessibility/RovingTabIndex";
import {_t} from "../../../languageHandler";
import AccessibleButton from "../../views/elements/AccessibleButton";
import RoomTile from "./RoomTile";
import RoomNamesRow from "./RoomNamesRow";
import SublistContextMenu from "../context_menus/SublistContextMenu";
import {ListLayout} from "../../../stores/room-list/ListLayout";
import {ChevronFace, ContextMenu, ContextMenuTooltipButton} from "../../structures/ContextMenu";
import RoomListStore, {LISTS_UPDATE_EVENT} from "../../../stores/room-list/RoomListStore";
import {DefaultTagID, TagID} from "../../../stores/room-list/models";
import dis from "../../../dispatcher/dispatcher";
import defaultDispatcher from "../../../dispatcher/dispatcher";
import NotificationBadge from "./NotificationBadge";
import {Key} from "../../../Keyboard";
import {ActionPayload} from "../../../dispatcher/payloads";
import {polyfillTouchEvent} from "../../../@types/polyfill";
import {RoomNotificationStateStore} from "../../../stores/notifications/RoomNotificationStateStore";
import RoomListLayoutStore from "../../../stores/room-list/RoomListLayoutStore";
import {arrayFastClone, arrayHasOrderChange} from "../../../utils/arrays";
import {objectExcluding, objectHasDiff} from "../../../utils/objects";
import TemporaryTile from "./TemporaryTile";
import {ListNotificationState} from "../../../stores/notifications/ListNotificationState";
import RoomListSelectionStore, {SELECTION_CHANGED} from "../../../stores/room-list/RoomListSelectionStore";
import {EchoChamber} from "../../../stores/local-echo/EchoChamber";
import {ALL_MESSAGES, MENTIONS_ONLY} from "../../../RoomNotifs";
import RoomListActions from "../../../actions/RoomListActions";
import {MatrixClientPeg} from "../../../MatrixClientPeg";
import IconizedContextMenu from "../context_menus/IconizedContextMenu";

const SHOW_N_BUTTON_HEIGHT = 28; // As defined by CSS
const RESIZE_HANDLE_HEIGHT = 4; // As defined by CSS
export const HEADER_HEIGHT = 32; // As defined by CSS

const MAX_PADDING_HEIGHT = SHOW_N_BUTTON_HEIGHT + RESIZE_HANDLE_HEIGHT;

// HACK: We really shouldn't have to do this.
polyfillTouchEvent();

interface IProps {
    startAsHidden: boolean;
    label: string;
    addRoomContextMenu?: (onFinished: () => void) => React.ReactNode;
    addRoomLabel: string;
    tagId: TagID;
    onAddRoom?: () => void;
    onResize: () => void;
    isFavourite: boolean;
    onFooterClick?: void;
    onHeaderClick?: () => void;
    scrollToTop: () => void;
    showSkeleton?: boolean;
    // TODO: Don't use this. It's for community invites, and community invites shouldn't be here.
    // You should feel bad if you use this.
    extraBadTilesThatShouldntExist?: TemporaryTile[];

    // TODO: Account for https://github.com/vector-im/element-web/issues/14179
}

type PartialDOMRect = Pick<DOMRect, "left" | "top" | "height">;

interface IState {
    contextMenuPosition: PartialDOMRect;
    isExpanded: boolean; // used for the for expand of the sublist when the room list is being filtered
    addRoomContextMenuPosition: PartialDOMRect;
    rooms: Room[];
    selected: Room[];
    filteredExtraTiles?: TemporaryTile[];
}

export default class RoomSublist extends React.Component<IProps, IState> {
    private headerButton = createRef<HTMLDivElement>();
    private sublistRef = createRef<HTMLDivElement>();
    private dispatcherRef: string;
    private layout: ListLayout;
    private notificationState: ListNotificationState;

    constructor(props: IProps) {
        super(props);

        this.notificationState = RoomNotificationStateStore.instance.getListState(this.props.tagId);
        this.layout = RoomListLayoutStore.instance.getLayoutFor(this.props.tagId);

        this.state = {
            contextMenuPosition: null,
            isExpanded: !this.layout.isCollapsed,
            addRoomContextMenuPosition: null,
            rooms: arrayFastClone(RoomListStore.instance.orderedLists[this.props.tagId] || []),
            selected: RoomListSelectionStore.instance.getSelectedFor(this.props.tagId)
        };

        RoomListStore.instance.on(LISTS_UPDATE_EVENT, this.onListsUpdated);
        RoomListSelectionStore.instance.on(SELECTION_CHANGED, this.onSelectionChanged);
        this.dispatcherRef = defaultDispatcher.register(this.onAction);
    }

    private get extraTiles(): TemporaryTile[] | null {
        if (this.props.extraBadTilesThatShouldntExist) {
            return this.props.extraBadTilesThatShouldntExist;
        }
        return null;
    }

    public shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>): boolean {
        if (objectHasDiff(this.props, nextProps)) {
            // Something we don't care to optimize has updated, so update.
            return true;
        }

        // Do the same check used on props for state, without the rooms we're going to no-op
        const prevStateNoRooms = objectExcluding(this.state, ['rooms']);
        const nextStateNoRooms = objectExcluding(nextState, ['rooms']);
        if (objectHasDiff(prevStateNoRooms, nextStateNoRooms)) {
            return true;
        }

        // If we're supposed to handle extra tiles, take the performance hit and re-render all the
        // time so we don't have to consider them as part of the visible room optimization.
        const prevExtraTiles = this.props.extraBadTilesThatShouldntExist || [];
        const nextExtraTiles = nextProps.extraBadTilesThatShouldntExist || [];
        if (prevExtraTiles.length > 0 || nextExtraTiles.length > 0) {
            return true;
        }

        // Before we go analyzing the rooms, we can see if we're collapsed. If we're collapsed, we don't need
        // to render anything. We do this after the height check though to ensure that the height gets appropriately
        // calculated for when/if we become uncollapsed.
        if (!nextState.isExpanded) {
            return false;
        }

        // Quickly double check we're not about to break something due to the number of rooms changing.
        if (this.state.rooms.length !== nextState.rooms.length) {
            return true;
        }

        // Finally, determine if the room update (as presumably that's all that's left) is within
        // our visible range. If it is, then do a render. If the update is outside our visible range
        // then we can skip the update.
        //
        // We also optimize for order changing here: if the update did happen in our visible range
        // but doesn't result in the list re-sorting itself then there's no reason for us to update
        // on our own.
        const prevSlicedRooms = this.state.rooms.slice(0, this.layout.visibleTiles);
        const nextSlicedRooms = nextState.rooms.slice(0, this.layout.visibleTiles);
        if (arrayHasOrderChange(prevSlicedRooms, nextSlicedRooms)) {
            return true;
        }

        // Finally, nothing happened so no-op the update
        return false;
    }

    public componentWillUnmount() {
        RoomListStore.instance.off(LISTS_UPDATE_EVENT, this.onListsUpdated);
        RoomListSelectionStore.instance.off(SELECTION_CHANGED, this.onSelectionChanged);
        defaultDispatcher.unregister(this.dispatcherRef);
    }

    private onListsUpdated = () => {
        const stateUpdates: IState & any = {}; // &any is to avoid a cast on the initializer

        if (this.props.extraBadTilesThatShouldntExist) {
            if (this.state.filteredExtraTiles) {
                stateUpdates.filteredExtraTiles = null;
            }
        }

        const currentRooms = this.state.rooms;
        const newRooms = arrayFastClone(RoomListStore.instance.orderedLists[this.props.tagId] || []);
        if (arrayHasOrderChange(currentRooms, newRooms)) {
            stateUpdates.rooms = newRooms;
        }

        if (Object.keys(stateUpdates).length > 0) {
            this.setState(stateUpdates, () => {
                if (currentRooms.length !== newRooms.length) {
                    this.props.onResize();
                }
            });
        }
    };

    private onSelectionChanged = () => {
        const selectedRooms = RoomListSelectionStore.instance.getSelectedFor(this.props.tagId);
        this.setState({selected: selectedRooms});
    }

    private onAction = (payload: ActionPayload) => {
        if (payload.action === "view_room" && payload.show_room_tile && this.state.rooms) {
            // XXX: we have to do this a tick later because we have incorrect intermediate props during a room change
            // where we lose the room we are changing from temporarily and then it comes back in an update right after.
            setImmediate(() => {
                const roomIndex = this.state.rooms.findIndex((r) => r.roomId === payload.room_id);

                if (!this.state.isExpanded && roomIndex > -1) {
                    this.toggleCollapsed();
                }
            });
        }
    };

    private onOpenMenuClick = (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        const target = ev.target as HTMLButtonElement;
        this.setState({contextMenuPosition: target.getBoundingClientRect()});
    };

    private onContextMenu = (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        this.setState({
            contextMenuPosition: {
                left: ev.clientX,
                top: ev.clientY,
                height: 0,
            },
        });
    };

    private onAddRoomContextMenu = (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        const target = ev.target as HTMLButtonElement;
        this.setState({addRoomContextMenuPosition: target.getBoundingClientRect()});
    };

    private onCloseMenu = () => {
        this.setState({contextMenuPosition: null});
    };

    private onLayoutChanged = () => {
        this.forceUpdate(() => {this.props.onResize()});
    }

    private onCloseAddRoomMenu = () => {
        this.setState({addRoomContextMenuPosition: null});
    };

    private onBadgeClick = (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();

        let room;
        if (this.props.tagId === DefaultTagID.Invite) {
            // switch to first room as that'll be the top of the list for the user
            room = this.state.rooms && this.state.rooms[0];
        } else {
            // find the first room with a count of the same colour as the badge count
            room = RoomListStore.instance.unfilteredLists[this.props.tagId].find((r: Room) => {
                const notifState = this.notificationState.getForRoom(r);
                return notifState.count > 0 && notifState.color === this.notificationState.color;
            });
        }

        if (room) {
            dis.dispatch({
                action: 'view_room',
                room_id: room.roomId,
                show_room_tile: false, // to make sure the room gets scrolled into view
            });
        }
    };

    private onHeaderClick = () => {
        const possibleSticky = this.headerButton.current;
        const isStickyHeader = possibleSticky.classList.contains("nv_RoomSublist2_sticky_active");

        if (isStickyHeader) {
            setImmediate(() => {
                this.props.scrollToTop();
            });
            if (!this.state.isExpanded) {
                this.toggleCollapsed();
            }
            return;
        }
        this.toggleCollapsed();
    };

    private toggleCollapsed = () => {
        this.layout.isCollapsed = this.state.isExpanded;
        this.setState({isExpanded: !this.layout.isCollapsed});
        setImmediate(() => this.props.onResize()); // needs to happen when the DOM is updated
    };

    private onHeaderKeyDown = (ev: React.KeyboardEvent) => {
        switch (ev.key) {
            case Key.ARROW_LEFT:
                ev.stopPropagation();
                if (this.state.isExpanded) {
                    // On ARROW_LEFT collapse the room sublist if it isn't already
                    this.toggleCollapsed();
                }
                break;
            case Key.ARROW_RIGHT: {
                ev.stopPropagation();
                if (!this.state.isExpanded) {
                    // On ARROW_RIGHT expand the room sublist if it isn't already
                    this.toggleCollapsed();
                } else if (this.sublistRef.current) {
                    // otherwise focus the first room
                    const element = this.sublistRef.current.querySelector(".nv_RoomTile2") as HTMLDivElement;
                    if (element) {
                        element.focus();
                    }
                }
                break;
            }
        }
    };

    private onKeyDown = (ev: React.KeyboardEvent) => {
        switch (ev.key) {
            // On ARROW_LEFT go to the sublist header
            case Key.ARROW_LEFT:
                ev.stopPropagation();
                this.headerButton.current.focus();
                break;
            // Consume ARROW_RIGHT so it doesn't cause focus to get sent to composer
            case Key.ARROW_RIGHT:
                ev.stopPropagation();
        }
    };

    private handleTileClick = (room: Room) => (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        if (this.state.selected.length) {
            dis.dispatch({
                action: ev.shiftKey ? 'RoomListActions.Selection.multiplyToggle' : 'RoomListActions.Selection.toggle',
                room,
                tagId: this.props.tagId,
            });
        } else {
            dis.dispatch({
                action: 'view_room',
                show_room_tile: true, // make sure the room is visible in the list
                room_id: room.roomId,
                clear_search: false,
            });
        }
    };

    private renderVisibleTiles(): React.ReactElement[] {
        const { rooms, isExpanded } = this.state;
        if (!isExpanded || !rooms) {
            // don't waste time on rendering
            return [];
        }

        const tiles: React.ReactElement[] = [];

        const visibleRooms = rooms.slice(0, this.layout.visibleTiles);
        for (const room of visibleRooms) {
            tiles.push(
                <RoomTile
                    room={room}
                    key={`room-${room.roomId}`}
                    showMessagePreview={this.layout.showPreviews}
                    isFavourite={this.props.isFavourite}
                    tagId={this.props.tagId}
                    smallRow={this.layout.smallRows}
                    selected={this.state.selected.includes(room)}
                    onTileClick={this.handleTileClick(room)}
                    onTagSelectedRoomsChanged={this.onTagSelectedRoomsChanged}
                />
            );
        }

        if (this.props.extraBadTilesThatShouldntExist) {
            (tiles as any[]).push(...this.props.extraBadTilesThatShouldntExist);
        }

        // We only have to do this because of the extra tiles. We do it conditionally
        // to avoid spending cycles on slicing. It's generally fine to do this though
        // as users are unlikely to have more than a handful of tiles when the extra
        // tiles are used.
        // if (tiles.length > this.layout.visibleTiles) {
        //     return tiles.slice(0, this.layout.visibleTiles);
        // }

        return tiles;
    }

    private renderMenu(): React.ReactElement {
        let contextMenu = null;
        if (this.state.contextMenuPosition) {
            contextMenu = (
                <ContextMenu
                    chevronFace={ChevronFace.None}
                    left={this.state.contextMenuPosition.left}
                    top={this.state.contextMenuPosition.top + this.state.contextMenuPosition.height}
                    onFinished={this.onCloseMenu}
                >
                    <SublistContextMenu
                        tagId={this.props.tagId}
                        layout={this.layout}
                        onCloseMenu={this.onCloseMenu}
                        hideTilesCountSection={false}
                        onLayoutChanged={this.onLayoutChanged}
                        onAddRoom={this.props.onAddRoom}
                        addRoomLabel={this.props.addRoomLabel}
                    />
                </ContextMenu>
            );
        } else if (this.state.addRoomContextMenuPosition) {
            contextMenu = (
                <IconizedContextMenu
                    chevronFace={ChevronFace.None}
                    left={this.state.addRoomContextMenuPosition.left - 7} // center align with the handle
                    top={this.state.addRoomContextMenuPosition.top + this.state.addRoomContextMenuPosition.height}
                    onFinished={this.onCloseAddRoomMenu}
                    compact
                >
                    {this.props.addRoomContextMenu(this.onCloseAddRoomMenu)}
                </IconizedContextMenu>
            );
        }

        return (
            <React.Fragment>
                <ContextMenuTooltipButton
                    className="nv_RoomSublist2_menuButton"
                    onClick={this.onOpenMenuClick}
                    title={_t("List options")}
                    isExpanded={!!this.state.contextMenuPosition}
                />
                {contextMenu}
            </React.Fragment>
        );
    }

    private renderHeader(): React.ReactElement {
        return (
            <RovingTabIndexWrapper inputRef={this.headerButton}>
                {({onFocus, isActive, ref}) => {
                    const tabIndex = isActive ? 0 : -1;

                    let ariaLabel = _t("Jump to first unread room.");
                    if (this.props.tagId === DefaultTagID.Invite) {
                        ariaLabel = _t("Jump to first invite.");
                    }

                    let badge = null;
                    if (this.props.tagId !== DefaultTagID.LowPriority) {
                        badge = <div className="nv_RoomSublist2_badgeContainer">
                            <NotificationBadge
                                notification={this.notificationState}
                                onClick={this.onBadgeClick}
                                tabIndex={tabIndex}
                                aria-label={ariaLabel}
                                isFavourite={this.props.isFavourite}
                                className="mx_RoomSublist_auxButton"
                                title={this.props.addRoomLabel}
                            />
                        </div>;
                    }

                    return (
                        <div
                            className="nv_RoomSublist2_headerContainer"
                            onKeyDown={this.onHeaderKeyDown}
                            onFocus={onFocus}
                            aria-label={this.props.label}
                        >
                            <AccessibleButton
                                className="nv_RoomSublist2_sticky"
                                role="treeitem"
                                aria-level={1}
                                onClick={this.onHeaderClick}
                                onContextMenu={this.onContextMenu}
                                onFocus={onFocus}
                                inputRef={ref}
                                tabIndex={tabIndex}
                            >
                                {badge}
                                <div className="nv_RoomSublist2_LabelContainer">
                                    <span className="nv_RoomSublist2_Label">{this.props.label}</span>
                                    {<RoomNamesRow
                                        tagId={this.props.tagId}
                                        offset={0}
                                        notification={this.notificationState}
                                        rooms={this.state.rooms}
                                        selected={this.state.selected}
                                    />}
                                </div>
                                {this.renderMenu()}
                            </AccessibleButton>
                        </div>
                    );
                }}
            </RovingTabIndexWrapper>
        );
    }

    private renderFooter(): React.ReactElement {
        const { isExpanded, rooms } = this.state;
        if (!isExpanded || !rooms || rooms.length <= this.layout.visibleTiles) {
            return null;
        }

        return (
            <div className="nv_RoomSublist2_footerContainer">
                <RoomNamesRow
                    tagId={this.props.tagId}
                    offset={this.layout.visibleTiles}
                    notification={this.notificationState}
                    rooms={this.state.rooms}
                    selected={this.state.selected}
                />
                <AccessibleButton
                    tabIndex={-1}
                    className="nv_RoomSublist2_moreButton"
                    role="treeitem"
                    aria-level={1}
                    // @ts-ignore
                    onClick={this.props.onFooterClick}
                >
                    more ›
                </AccessibleButton>
            </div>
        );
    }

    private onTagSelectedRoomsChanged = (tagId) => {
        if (tagId !== DefaultTagID.Favourite && tagId !== DefaultTagID.LowPriority) {
            return;
        }

        const inverseTag = tagId === DefaultTagID.Favourite ? DefaultTagID.LowPriority : DefaultTagID.Favourite;
        const isApplied = RoomListStore.instance.getTagsForRoom(this.state.selected[0]).includes(tagId);
        const isInverseApplied = RoomListStore.instance.getTagsForRoom(this.state.selected[0]).includes(inverseTag);

        let removeTag = null;
        let addTag = tagId;
        if (isApplied) {
            removeTag = tagId;
            addTag = null;
        } else if (isInverseApplied) {
            removeTag = inverseTag;
        }

        dis.dispatch(RoomListActions.bulkTagRooms(
            MatrixClientPeg.get(),
            this.state.selected,
            removeTag,
            addTag,
            undefined,
            0
        ));
        if ([removeTag, addTag].includes(DefaultTagID.LowPriority)) {
            for (const room of this.state.selected) {
                EchoChamber.forRoom(room).notificationVolume =
                    addTag === DefaultTagID.LowPriority ? MENTIONS_ONLY : ALL_MESSAGES;
            }
        }
    }

    public render(): React.ReactElement {
        const visibleTiles = this.renderVisibleTiles();
        const classes = classNames({
            'nv_RoomSublist2': true,
            'nv_RoomSublist2_collapsed': !this.state.isExpanded,
            'nv_RoomSublist2_empty': !this.state.rooms.length,
            'mx_RoomSublist2_hasMenuOpen': !!this.state.contextMenuPosition
        });

        return (
            <div
                ref={this.sublistRef}
                className={classes}
                role="group"
                aria-label={this.props.label}
                onKeyDown={this.onKeyDown}
            >
                {!visibleTiles.length && this.props.showSkeleton && this.state.isExpanded ?
                    <div className="mx_RoomSublist_skeletonUI" /> :
                    <div className="nv_RoomSublist2_tiles">
                        {this.renderHeader()}
                        {visibleTiles}
                    </div>
                }
                {this.renderFooter()}
            </div>
        );
    }
}

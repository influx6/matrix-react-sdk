/*
Copyright 2020 Nova Technology Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as React from "react";
import {createRef} from "react";
import classNames from "classnames";
import {Room} from "matrix-js-sdk/src/models/room";
import {RovingTabIndexWrapper} from "../../../accessibility/RovingTabIndex";
import {_t} from "../../../languageHandler";
import {Key} from "../../../Keyboard";
import {ListLayout} from "../../../stores/room-list/ListLayout";
import {
    ChevronFace,
    ContextMenu,
    ContextMenuTooltipButton
} from "../../structures/ContextMenu";
import RoomListStore, {LISTS_UPDATE_EVENT} from "../../../stores/room-list/RoomListStore";
import {DefaultTagID, TagID} from "../../../stores/room-list/models";
import RoomListLayoutStore from "../../../stores/room-list/RoomListLayoutStore";
import defaultDispatcher from "../../../dispatcher/dispatcher";
import {ActionPayload} from "../../../dispatcher/payloads";
import {arrayFastClone, arrayHasOrderChange} from "../../../utils/arrays";
import AccessibleButton from "../elements/AccessibleButton";
import LazyRenderList from "../elements/LazyRenderList";
import SublistContextMenu from "../context_menus/SublistContextMenu";
import RoomTile from "./RoomTile";
import dis from "../../../dispatcher/dispatcher";
import RoomListActions from "../../../actions/RoomListActions";
import {MatrixClientPeg} from "../../../MatrixClientPeg";
import {EchoChamber} from "../../../stores/local-echo/EchoChamber";
import RoomListSelectionStore, {SELECTION_CHANGED} from "../../../stores/room-list/RoomListSelectionStore";
import {ALL_MESSAGES, MENTIONS_ONLY} from "../../../RoomNotifs";

interface IProps {
    rooms?: Room[];
    label: string;
    onAddRoom?: () => void;
    onHeaderClick?: () => void;
    addRoomLabel?: string;
    tagId: TagID;
    scrollerHeight: number;
    onKeyDown: (ev: React.KeyboardEvent) => void;
}

type PartialDOMRect = Pick<DOMRect, "left" | "top" | "height">;

interface IState {
    contextMenuPosition: PartialDOMRect;
    rooms: Room[];
    selected: Room[];
    scrollTop: number,
}

export default class RoomListForTag extends React.Component<IProps, IState> {
    private headerButton = createRef<HTMLDivElement>();
    private scrollerRef = createRef<HTMLDivElement>();
    private dispatcherRef: string;
    private layout: ListLayout;

    constructor(props: IProps) {
        super(props);
        this.layout = RoomListLayoutStore.instance.getLayoutFor(this.props.tagId);
        this.state = {
            contextMenuPosition: null,
            rooms: arrayFastClone(RoomListStore.instance.orderedLists[this.props.tagId] || []),
            scrollTop: 0,
            selected: RoomListSelectionStore.instance.getSelectedFor(this.props.tagId)
        };
        this.dispatcherRef = defaultDispatcher.register(this.onAction);
        RoomListStore.instance.on(LISTS_UPDATE_EVENT, this.onListsUpdated);
        RoomListSelectionStore.instance.on(SELECTION_CHANGED, this.onSelectionChanged);
    }

    public componentDidMount() {
        this.headerButton.current.focus();
        if (this.scrollerRef.current) {
            this.scrollerRef.current.scrollTop = 0;
        }
    }

    public componentWillUnmount() {
        defaultDispatcher.unregister(this.dispatcherRef);
        RoomListStore.instance.off(LISTS_UPDATE_EVENT, this.onListsUpdated);
        RoomListSelectionStore.instance.off(SELECTION_CHANGED, this.onSelectionChanged);
    }

    private onSelectionChanged = () => {
        const selectedRooms = RoomListSelectionStore.instance.getSelectedFor(this.props.tagId);
        this.setState({selected: selectedRooms});
    }

    private onListsUpdated = () => {
        const newState: IState & any = {};

        const oldRooms = this.state.rooms;
        let newRooms = RoomListStore.instance.orderedLists[this.props.tagId] || [];

        if (arrayHasOrderChange(oldRooms, newRooms)) {
            newState.rooms = newRooms;
        }
        if (Object.keys(newState).length > 0) {
            this.setState(newState);
        }

        if (oldRooms.length !== newRooms.length) {
            // TODO
        }
    }

    private onAction = (payload: ActionPayload) => {
        if (payload.action === "view_room" && payload.show_room_tile && this.state.rooms) {
            // XXX: we have to do this a tick later because we have incorrect intermediate props during a room change
            // where we lose the room we are changing from temporarily and then it comes back in an update right after.
            setImmediate(() => {
                const roomIndex = this.state.rooms.findIndex((r) => r.roomId === payload.room_id);

                if (roomIndex > -1) {
                    // TODO
                }
            });
        }
    };

    private onOpenMenuClick = (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        const target = ev.target as HTMLButtonElement;
        this.setState({contextMenuPosition: target.getBoundingClientRect()});
    };

    private onContextMenu = (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        this.setState({
            contextMenuPosition: {
                left: ev.clientX,
                top: ev.clientY,
                height: 0,
            },
        });
    };

    private onLayoutChanged = () => {
        this.forceUpdate(); // because the layout doesn't trigger a re-render
    }

    private onKeyDown = (ev: React.KeyboardEvent) => {
        switch (ev.key) {
            // On ARROW_LEFT go to the sublist header
            case Key.ARROW_LEFT:
                ev.stopPropagation();
                this.headerButton.current.focus();
                break;
            case Key.ESCAPE:
                ev.stopPropagation();
                this.props.onHeaderClick();
                break;
            default:
                this.props.onKeyDown(ev);
        }
    };

    private handleTileClick = (room: Room) => (ev: React.MouseEvent) => {
        ev.preventDefault();
        ev.stopPropagation();
        if (this.state.selected.length) {
            dis.dispatch({
                action: ev.shiftKey ? 'RoomListActions.Selection.multiplyToggle' : 'RoomListActions.Selection.toggle',
                room,
                tagId: this.props.tagId,
            });
        } else {
            dis.dispatch({
                action: 'view_room',
                show_room_tile: true, // make sure the room is visible in the list
                room_id: room.roomId,
                clear_search: false,
            });
        }
    };

    private onTagSelectedRoomsChanged = (tagId) => {
        if (tagId !== DefaultTagID.Favourite && tagId !== DefaultTagID.LowPriority) {
            return;
        }

        const inverseTag = tagId === DefaultTagID.Favourite ? DefaultTagID.LowPriority : DefaultTagID.Favourite;
        const isApplied = RoomListStore.instance.getTagsForRoom(this.state.selected[0]).includes(tagId);
        const isInverseApplied = RoomListStore.instance.getTagsForRoom(this.state.selected[0]).includes(inverseTag);

        let removeTag = null;
        let addTag = tagId;
        if (isApplied) {
            removeTag = tagId;
            addTag = null;
        } else if (isInverseApplied) {
            removeTag = inverseTag;
        }

        const shouldCloseTag = this.state.selected.length === this.state.rooms.length;
        dis.dispatch(RoomListActions.bulkTagRooms(
            MatrixClientPeg.get(),
            this.state.selected,
            removeTag,
            addTag,
            undefined,
            0
        ));
        if ([removeTag, addTag].includes(DefaultTagID.LowPriority)) {
            for (const room of this.state.selected) {
                EchoChamber.forRoom(room).notificationVolume = addTag === DefaultTagID.LowPriority ? MENTIONS_ONLY : ALL_MESSAGES;
            }
        }
        if (shouldCloseTag) {
            setTimeout(this.props.onHeaderClick, 300);
        }
    }

    private makeRoomTile = (room: Room) => {
        return (
            <RoomTile
                room={room}
                key={`room-${room.roomId}`}
                showMessagePreview={this.layout.showPreviews}
                isFavourite={this.props.tagId === DefaultTagID.Favourite}
                smallRow={this.layout.smallRows}
                tagId={this.props.tagId}
                selected={this.state.selected.includes(room)}
                onTileClick={this.handleTileClick(room)}
                onTagSelectedRoomsChanged={this.onTagSelectedRoomsChanged}
            />
        );
    }

    private onScroll = () => {
        this.setState({
            scrollTop: this.scrollerRef.current.scrollTop,
        })
    }

    private renderRoomTiles(): React.ReactElement {
        return (
            <div className="nv_RoomSublist2_tiles mx_AutoHideScrollbar" ref={this.scrollerRef} onScroll={this.onScroll}>
                <LazyRenderList
                    scrollTop={this.state.scrollTop}
                    height={this.props.scrollerHeight - this.layout.headerHeight}
                    itemHeight={this.layout.tileHeight}
                    renderItem={this.makeRoomTile}
                    items={this.state.rooms}
                />
            </div>
        );
    }

    private onCloseMenu = () => {
        this.setState({contextMenuPosition: null});
    };

    private renderMenu(): React.ReactElement {
        let contextMenu = null;
        if (this.state.contextMenuPosition) {
            contextMenu = (
                <ContextMenu
                    chevronFace={ChevronFace.None}
                    left={this.state.contextMenuPosition.left}
                    top={this.state.contextMenuPosition.top + this.state.contextMenuPosition.height}
                    onFinished={this.onCloseMenu}
                >
                    <SublistContextMenu
                        tagId={this.props.tagId}
                        layout={this.layout}
                        onCloseMenu={this.onCloseMenu}
                        hideTilesCountSection={true}
                        onLayoutChanged={this.onLayoutChanged}
                        onAddRoom={this.props.onAddRoom}
                        addRoomLabel={this.props.addRoomLabel}
                    />
                </ContextMenu>
            );
        }

        return (
            <React.Fragment>
                <ContextMenuTooltipButton
                    className="nv_RoomSublist2_menuButton"
                    onClick={this.onOpenMenuClick}
                    title={_t("List options")}
                    isExpanded={!!this.state.contextMenuPosition}
                />
                {contextMenu}
            </React.Fragment>
        );
    }

    private renderHeader(): React.ReactElement {
        return (
            <RovingTabIndexWrapper inputRef={this.headerButton}>
                {({onFocus, isActive, ref}) => {
                    const tabIndex = isActive ? 0 : -1;
                    return (
                        <div
                            className="nv_RoomSublist2_headerContainer"
                            onFocus={onFocus}
                            aria-label={this.props.label}
                        >
                            <AccessibleButton
                                className="nv_RoomSublist2_sticky"
                                role="treeitem"
                                aria-level={1}
                                onClick={this.props.onHeaderClick}
                                onContextMenu={this.onContextMenu}
                                onFocus={onFocus}
                                inputRef={ref}
                                tabIndex={tabIndex}
                            >
                                <div className="nv_RoomSublist2_badgeContainer">
                                    <div className="nv_RoomSublist2_backButton"/>
                                </div>
                                <div className="nv_RoomSublist2_LabelContainer">
                                    <span className="nv_RoomSublist2_Label">{this.props.label}</span>
                                </div>
                                {this.renderMenu()}
                            </AccessibleButton>
                        </div>
                    );
                }}
            </RovingTabIndexWrapper>
        );
    }

    public render(): React.ReactElement {
        const classes = classNames({
            'nv_RoomSublist2': true,
            'nv_RoomSublist2_forTag': true,
            'mx_RoomSublist2_hasMenuOpen': !!this.state.contextMenuPosition
        });

        return (
            <div
                className={classes}
                role="group"
                aria-label={this.props.label}
                onKeyDown={this.onKeyDown}
            >
                {this.renderHeader()}
                {this.renderRoomTiles()}
            </div>
        );
    }
}

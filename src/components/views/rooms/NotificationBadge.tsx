/*
Copyright 2020 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from "react";
import classNames from "classnames";
import AccessibleButton from "../elements/AccessibleButton";
import { XOR } from "../../../@types/common";
import { NOTIFICATION_STATE_UPDATE, NotificationState } from "../../../stores/notifications/NotificationState";

interface IProps {
    notification: NotificationState;
    isFavourite?: boolean;
}

interface IClickableProps extends IProps, React.InputHTMLAttributes<Element> {
    /**
     * If specified will return an AccessibleButton instead of a div.
     */
    onClick?(ev: React.MouseEvent);
}

interface IState {
    showCounts: boolean; // whether or not to show counts. Independent of props.forceCount
}

export default class NotificationBadge extends React.PureComponent<XOR<IProps, IClickableProps>, IState> {
    constructor(props: IProps) {
        super(props);
        this.props.notification.on(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
    }

    public componentWillUnmount() {
        if (this.props.notification) {
            this.props.notification.off(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
        }
    }

    public componentDidUpdate(prevProps: Readonly<IProps>) {
        if (prevProps.notification) {
            prevProps.notification.off(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
        }

        this.props.notification.on(NOTIFICATION_STATE_UPDATE, this.onNotificationUpdate);
    }

    private onNotificationUpdate = () => {
        this.forceUpdate(); // notification state changed - update
    };

    public render(): React.ReactElement {
        /* eslint @typescript-eslint/no-unused-vars: ["error", { "ignoreRestSiblings": true }] */
        const {notification, onClick, isFavourite, ...props} = this.props;

        // Don't show a badge if we don't need to
        if (notification.isIdle) return null;

        const classes = classNames({
            'nv_NotificationBadge': true,
            'nv_NotificationBadge_unread': notification.isUnread,
            'nv_NotificationBadge_important': notification.isImportant,
            'nv_NotificationBadge_favourite': isFavourite,
        });

        return onClick ? (<AccessibleButton {...props} className={classes} onClick={onClick} />)
            : <div className={classes} />;
    }
}

/*
Copyright 2019 New Vector Ltd
Copyright 2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import {_t} from "../../../../../languageHandler";
import SdkConfig from "../../../../../SdkConfig";
import {MatrixClientPeg} from "../../../../../MatrixClientPeg";
import * as FormattingUtils from "../../../../../utils/FormattingUtils";
import AccessibleButton from "../../../elements/AccessibleButton";
import Analytics from "../../../../../Analytics";
import Modal from "../../../../../Modal";
import * as sdk from "../../../../..";
import {sleep} from "../../../../../utils/promise";
import dis from "../../../../../dispatcher/dispatcher";
import {privateShouldBeEncrypted} from "../../../../../createRoom";
import {SettingLevel} from "../../../../../settings/SettingLevel";
import SecureBackupPanel from "../../SecureBackupPanel";
import SettingsStore from "../../../../../settings/SettingsStore";
import {UIFeature} from "../../../../../settings/UIFeature";
import {isE2eAdvancedPanelPossible} from "../../E2eAdvancedPanel";
import CountlyAnalytics from "../../../../../CountlyAnalytics";

export class IgnoredUser extends React.Component {
    static propTypes = {
        userId: PropTypes.string.isRequired,
        onUnignored: PropTypes.func.isRequired,
        inProgress: PropTypes.bool.isRequired,
    };

    _onUnignoreClicked = (e) => {
        this.props.onUnignored(this.props.userId);
    };

    render() {
        const id = `mx_SecurityUserSettingsTab_ignoredUser_${this.props.userId}`;
        return (
            <div className='mx_SecurityUserSettingsTab_ignoredUser'>
                <AccessibleButton onClick={this._onUnignoreClicked} kind='primary_sm' aria-describedby={id} disabled={this.props.inProgress}>
                    { _t('Unignore') }
                </AccessibleButton>
                <span id={id}>{ this.props.userId }</span>
            </div>
        );
    }
}

export default class SecurityUserSettingsTab extends React.Component {
    static propTypes = {
        closeSettingsFn: PropTypes.func.isRequired,
    };

    constructor() {
        super();

        this.state = {
            ignoredUserIds: MatrixClientPeg.get().getIgnoredUsers(),
            waitingUnignored: [],
        };

        this._onAction = this._onAction.bind(this);
    }


    _onAction({action}) {
        if (action === "ignore_state_changed") {
            const ignoredUserIds = MatrixClientPeg.get().getIgnoredUsers();
            const newWaitingUnignored = this.state.waitingUnignored.filter(e=> ignoredUserIds.includes(e));
            this.setState({ignoredUserIds, waitingUnignored: newWaitingUnignored});
        }
    }

    componentDidMount() {
        this.dispatcherRef = dis.register(this._onAction);
    }

    componentWillUnmount() {
        dis.unregister(this.dispatcherRef);
    }

    _updateBlacklistDevicesFlag = (checked) => {
        MatrixClientPeg.get().setGlobalBlacklistUnverifiedDevices(checked);
    };

    _updateAnalytics = (checked) => {
        checked ? Analytics.enable() : Analytics.disable();
        CountlyAnalytics.instance.enable(/* anonymous = */ !checked);
    };

    _onExportE2eKeysClicked = () => {
        Modal.createTrackedDialogAsync('Export E2E Keys', '',
            import('../../../../../async-components/views/dialogs/security/ExportE2eKeysDialog'),
            {matrixClient: MatrixClientPeg.get()},
        );
    };

    _onImportE2eKeysClicked = () => {
        Modal.createTrackedDialogAsync('Import E2E Keys', '',
            import('../../../../../async-components/views/dialogs/security/ImportE2eKeysDialog'),
            {matrixClient: MatrixClientPeg.get()},
        );
    };

    _onGoToUserProfileClick = () => {
        dis.dispatch({
            action: 'view_user_info',
            userId: MatrixClientPeg.get().getUserId(),
        });
        this.props.closeSettingsFn();
    }

    _onUserUnignored = async (userId) => {
        const {ignoredUserIds, waitingUnignored} = this.state;
        const currentlyIgnoredUserIds = ignoredUserIds.filter(e => !waitingUnignored.includes(e));

        const index = currentlyIgnoredUserIds.indexOf(userId);
        if (index !== -1) {
            currentlyIgnoredUserIds.splice(index, 1);
            this.setState(({waitingUnignored}) => ({waitingUnignored: [...waitingUnignored, userId]}));
            MatrixClientPeg.get().setIgnoredUsers(currentlyIgnoredUserIds);
        }
    };

    _renderCurrentDeviceInfo() {
        const SettingsFlag = sdk.getComponent('views.elements.SettingsFlag');

        const client = MatrixClientPeg.get();
        const deviceId = client.deviceId;
        let identityKey = client.getDeviceEd25519Key();
        if (!identityKey) {
            identityKey = _t("<not supported>");
        } else {
            identityKey = FormattingUtils.formatCryptoKey(identityKey);
        }

        let importExportButtons = null;
        if (client.isCryptoEnabled()) {
            importExportButtons = (
                <div className='mx_SecurityUserSettingsTab_importExportButtons'>
                    <AccessibleButton kind='primary' onClick={this._onExportE2eKeysClicked}>
                        {_t("Export E2E room keys")}
                    </AccessibleButton>
                    <AccessibleButton kind='primary' onClick={this._onImportE2eKeysClicked}>
                        {_t("Import E2E room keys")}
                    </AccessibleButton>
                </div>
            );
        }

        let noSendUnverifiedSetting;
        if (SettingsStore.isEnabled("blacklistUnverifiedDevices")) {
            noSendUnverifiedSetting = <SettingsFlag
                name='blacklistUnverifiedDevices'
                level={SettingLevel.DEVICE}
                onChange={this._updateBlacklistDevicesFlag}
            />;
        }

        return (
            <div className='mx_SettingsTab_section'>
                <span className='mx_SettingsTab_subheading'>{_t("Cryptography")}</span>
                <ul className='mx_SettingsTab_subsectionText mx_SecurityUserSettingsTab_deviceInfo'>
                    <li>
                        <label>{_t("Session ID:")}</label>
                        <span><code>{deviceId}</code></span>
                    </li>
                    <li>
                        <label>{_t("Session key:")}</label>
                        <span><code><b>{identityKey}</b></code></span>
                    </li>
                </ul>
                {importExportButtons}
                {noSendUnverifiedSetting}
            </div>
        );
    }

    _renderIgnoredUsers() {
        const {waitingUnignored, ignoredUserIds} = this.state;

        if (!ignoredUserIds || ignoredUserIds.length === 0) return null;

        const userIds = ignoredUserIds
            .map((u) => <IgnoredUser
             userId={u}
             onUnignored={this._onUserUnignored}
             key={u}
             inProgress={waitingUnignored.includes(u)}
             />);

        return (
            <div className='mx_SettingsTab_section'>
                <span className='mx_SettingsTab_subheading'>{_t('Ignored users')}</span>
                <div className='mx_SettingsTab_subsectionText'>
                    {userIds}
                </div>
            </div>
        );
    }

    render() {
        const brand = SdkConfig.get().brand;
        const DevicesPanel = sdk.getComponent('views.settings.DevicesPanel');
        const SettingsFlag = sdk.getComponent('views.elements.SettingsFlag');
        const EventIndexPanel = sdk.getComponent('views.settings.EventIndexPanel');

        const secureBackup = (
            <div className='mx_SettingsTab_section'>
                <span className="mx_SettingsTab_subheading">{_t("Secure Backup")}</span>
                <div className='mx_SettingsTab_subsectionText'>
                    <SecureBackupPanel />
                </div>
            </div>
        );

        const eventIndex = (
            <div className="mx_SettingsTab_section">
                <span className="mx_SettingsTab_subheading">{_t("Message search")}</span>
                <EventIndexPanel />
            </div>
        );

        // XXX: There's no such panel in the current cross-signing designs, but
        // it's useful to have for testing the feature. If there's no interest
        // in having advanced details here once all flows are implemented, we
        // can remove this.
        const CrossSigningPanel = sdk.getComponent('views.settings.CrossSigningPanel');
        const crossSigning = (
            <div className='mx_SettingsTab_section'>
                <span className="mx_SettingsTab_subheading">{_t("Cross-signing")}</span>
                <div className='mx_SettingsTab_subsectionText'>
                    <CrossSigningPanel />
                </div>
            </div>
        );

        let warning;
        if (!privateShouldBeEncrypted()) {
            warning = <div className="mx_SecurityUserSettingsTab_warning">
                { _t("Your server admin has disabled end-to-end encryption by default " +
                    "in private rooms & Direct Messages.") }
            </div>;
        }

        let privacySection;
        if (Analytics.canEnable() || CountlyAnalytics.instance.canEnable()) {
            privacySection = <React.Fragment>
                <div className="mx_SettingsTab_heading">{_t("Privacy")}</div>
                <div className="mx_SettingsTab_section">
                    <span className="mx_SettingsTab_subheading">{_t("Analytics")}</span>
                    <div className="mx_SettingsTab_subsectionText">
                        {_t(
                            "%(brand)s collects anonymous analytics to allow us to improve the application.",
                            { brand },
                        )}
                        &nbsp;
                        {_t("Privacy is important to us, so we don't collect any personal or " +
                            "identifiable data for our analytics.")}
                        <AccessibleButton className="mx_SettingsTab_linkBtn" onClick={Analytics.showDetailsModal}>
                            {_t("Learn more about how we use analytics.")}
                        </AccessibleButton>
                    </div>
                    <SettingsFlag name="analyticsOptIn" level={SettingLevel.DEVICE} onChange={this._updateAnalytics} />
                </div>
            </React.Fragment>;
        }

        const E2eAdvancedPanel = sdk.getComponent('views.settings.E2eAdvancedPanel');
        let advancedSection;
        if (SettingsStore.getValue(UIFeature.AdvancedSettings)) {
            const ignoreUsersPanel = this._renderIgnoredUsers();
            const e2ePanel = isE2eAdvancedPanelPossible() ? <E2eAdvancedPanel /> : null;
            // only show the section if there's something to show
            if (ignoreUsersPanel || e2ePanel) {
                advancedSection = <>
                    <div className="mx_SettingsTab_heading">{_t("Advanced")}</div>
                    <div className="mx_SettingsTab_section">
                        {ignoreUsersPanel}
                        {e2ePanel}
                    </div>
                </>;
            }
        }

        return (
            <div className="mx_SettingsTab mx_SecurityUserSettingsTab">
                {warning}
                <div className="mx_SettingsTab_heading">{_t("Where you’re logged in")}</div>
                <div className="mx_SettingsTab_section">
                    <span>
                        {_t(
                            "Manage the names of and sign out of your sessions below or " +
                            "<a>verify them in your User Profile</a>.", {},
                            {
                                a: sub => <AccessibleButton kind="link" onClick={this._onGoToUserProfileClick}>
                                    {sub}
                                </AccessibleButton>,
                            },
                        )}
                    </span>
                    <div className='mx_SettingsTab_subsectionText'>
                        {_t("A session's public name is visible to people you communicate with")}
                        <DevicesPanel />
                    </div>
                </div>
                <div className="mx_SettingsTab_heading">{_t("Encryption")}</div>
                <div className="mx_SettingsTab_section">
                    {secureBackup}
                    {eventIndex}
                    {crossSigning}
                    {this._renderCurrentDeviceInfo()}
                </div>
                { privacySection }
                { advancedSection }
            </div>
        );
    }
}

/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as React from "react";
import classNames from 'classnames';
import RoomListStore from "../../../stores/room-list/RoomListStore";
import {ListAlgorithm, SortAlgorithm} from "../../../stores/room-list/algorithms/models";
import {DefaultTagID, TagID} from "../../../stores/room-list/models";
import {_t} from "../../../languageHandler";
import {ListLayout} from "../../../stores/room-list/ListLayout";
import {MatrixClientPeg} from "../../../MatrixClientPeg";
import {MenuItem, StyledMenuItemCheckbox, StyledMenuItemRadio} from "../../structures/ContextMenu";
import {ButtonEvent} from "../elements/AccessibleButton";
import {sleep} from "../../../utils/promise";
import dis from "../../../dispatcher/dispatcher";
import RoomViewStore from "../../../stores/RoomViewStore";

interface IProps {
    tagId: TagID;
    onCloseMenu: () => void;
    onLayoutChanged: () => void;
    layout: ListLayout;
    hideTilesCountSection: boolean;
    onAddRoom?: () => void;
    addRoomLabel?: string;
}

interface IState {
    managingInvites: boolean;
}

interface IMenuButtonProps {
    iconClassName: string;
    label: string;
    onClick(ev: ButtonEvent);
    disabled?: boolean;
}

const MenuButton: React.FC<IMenuButtonProps> = ({iconClassName, label, onClick, ...props}) => {
    return <MenuItem label={label} onClick={onClick} {...props}>
        <span className={classNames("mx_IconizedContextMenu_icon", iconClassName)} />
        <span className="mx_IconizedContextMenu_label">{label}</span>
    </MenuItem>;
};

export default class SublistContextMenu extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = {
            managingInvites: false,
        }
    }

    private onUnreadFirstChanged = async () => {
        const isUnreadFirst = RoomListStore.instance.getListOrder(this.props.tagId) === ListAlgorithm.Importance;
        const newAlgorithm = isUnreadFirst ? ListAlgorithm.Natural : ListAlgorithm.Importance;
        await RoomListStore.instance.setListOrder(this.props.tagId, newAlgorithm);
        // sublist will not be updated if the order of rooms wasn't changed after setListOrder
        // so we update the context menu here
        this.forceUpdate();
    };

    private onTagSortChanged = async (sort: SortAlgorithm) => {
        await RoomListStore.instance.setTagSorting(this.props.tagId, sort);
    };

    private onMessagePreviewChanged = () => {
        this.props.layout.showPreviews = !this.props.layout.showPreviews;
        this.props.onLayoutChanged();
    };

    private markAllRoomsAsRead = () => {
        const cli = MatrixClientPeg.get();
        cli.getRooms().forEach(r => {
            console.log(r, r.getUnreadNotificationCount());
            const events = r.getLiveTimeline().getEvents();
            if (events.length) cli.sendReadReceipt(events[events.length - 1]);
        });
    }

    private onNumVisibleTilesChanged = (val: number) => {
        this.props.layout.visibleTiles = val;
        this.props.onLayoutChanged();
    }

    private onShowSmallRowsChanged = (val: boolean) => {
        this.props.layout.smallRows = val;
        this.props.onLayoutChanged();
    }

    private onAcceptAllInvitesClicked = () => {
        this._manageInvites(true);
    }

    private onRejectAllInvitesClicked = () => {
        this._manageInvites(false);
    }

    private _manageInvites = async (accept) => {
        this.setState({ managingInvites: true });
        const cli = MatrixClientPeg.get();
        const invitedRoomIds = cli.getRooms()
            .filter((r) => r.hasMembershipState(cli.getUserId(), "invite"))
            .map((r) => r.roomId);

        const action = accept ? cli.joinRoom.bind(cli) : cli.leave.bind(cli);
        for (let i = 0; i < invitedRoomIds.length; i++) {
            const roomId = invitedRoomIds[i];

            // Accept/reject invite
            await action(roomId).then(() => {
                    if (roomId === RoomViewStore.getRoomId()) {
                        dis.dispatch({action: 'view_next_room'});
                    }
                }).catch(async (e) => {
                // Action failure
                if (e.errcode === "M_LIMIT_EXCEEDED") {
                    // Add a delay between each invite change in order to avoid rate
                    // limiting by the server.
                    await sleep(e.retry_after_ms || 2500);

                    // Redo last action
                    i--;
                } else {
                    // Print out error with joining/leaving room
                    console.warn(e);
                }
            });
        }
        this.setState({ managingInvites: false });
    }

    private preventPropagation = (ev: React.MouseEvent) => {
        ev.stopPropagation();
    }

    public render(): React.ReactElement {
        const { onCloseMenu, tagId, layout } = this.props;
        const isAlphabetical = RoomListStore.instance.getTagSorting(this.props.tagId) === SortAlgorithm.Alphabetic;
        const isUnreadFirst = RoomListStore.instance.getListOrder(this.props.tagId) === ListAlgorithm.Importance;

        // Invites don't get some nonsense options, so only add them if we have to.
        let notInviteSections = null;
        if (tagId !== DefaultTagID.Invite) {
            notInviteSections = (
                <React.Fragment>
                    <hr />
                    <div>
                        <div className='mx_RoomSublist2_contextMenu_title'>{_t("Appearance")}</div>
                        <StyledMenuItemCheckbox
                            onClose={onCloseMenu}
                            onChange={this.onUnreadFirstChanged}
                            checked={isUnreadFirst}
                        >
                            {_t("Show rooms with unread messages first")}
                        </StyledMenuItemCheckbox>
                        <StyledMenuItemCheckbox
                            onClose={onCloseMenu}
                            onChange={this.onMessagePreviewChanged}
                            checked={layout.showPreviews}
                        >
                            {_t("Show previews of messages")}
                        </StyledMenuItemCheckbox>
                    </div>
                </React.Fragment>
            );
        }
        let tilesCountSection = null;
        if (!this.props.hideTilesCountSection) {
            tilesCountSection = [5, 10, 15, 20].map((count) => {
                return (
                    <StyledMenuItemRadio
                        key={count}
                        onClose={onCloseMenu}
                        onChange={() => this.onNumVisibleTilesChanged(count)}
                        checked={layout.visibleTiles === count}
                        name={`mx_${this.props.tagId}_visibleTiles`}
                    >
                        {_t("%(count)s items", { count })}
                    </StyledMenuItemRadio>
                );
            });
        }
        let addRoomSection = null;
        if (!!this.props.onAddRoom) {
            addRoomSection = (
                <MenuButton
                    iconClassName="mx_RoomSublist_auxButton"
                    label={this.props.addRoomLabel || _t("Add room")}
                    onClick={this.props.onAddRoom}
                />
            );
        }
        let buttonsGroup;
        if (this.props.tagId === DefaultTagID.Invite) {
            buttonsGroup = (
                <>
                    <MenuButton
                        iconClassName="mx_RoomSublist_auxButton"
                        label={_t("Accept all")}
                        onClick={this.onAcceptAllInvitesClicked}
                        disabled={this.state.managingInvites}
                    />
                    <MenuButton
                        iconClassName="mx_RoomSublist_auxButton"
                        label={_t("Reject all")}
                        onClick={this.onRejectAllInvitesClicked}
                        disabled={this.state.managingInvites}
                    />
                </>
            );
        } else {
            buttonsGroup = (
                <>
                    {addRoomSection}
                    <MenuButton
                        iconClassName="mx_RoomSublist_auxButton"
                        label={_t("Mark all as read")}
                        onClick={this.markAllRoomsAsRead}
                    />
                </>
            );
        }
        return(
            <div className="mx_RoomSublist2_contextMenu" onClick={this.preventPropagation}>
                {buttonsGroup}
                {tilesCountSection}
                <div>
                    <StyledMenuItemRadio
                        onClose={onCloseMenu}
                        onChange={() => this.onShowSmallRowsChanged(false)}
                        checked={!layout.smallRows}
                        name={`nv_${this.props.tagId}_showSmallRows`}
                    >
                        {_t("Large rows")}
                    </StyledMenuItemRadio>
                    <StyledMenuItemRadio
                        onClose={onCloseMenu}
                        onChange={() => this.onShowSmallRowsChanged(true)}
                        checked={layout.smallRows}
                        name={`nv_${this.props.tagId}_showSmallRows`}
                    >
                        {_t("Small rows")}
                    </StyledMenuItemRadio>
                </div>
                <div>
                    <div className='mx_RoomSublist2_contextMenu_title'>{_t("Sort by")}</div>
                    <StyledMenuItemRadio
                        onClose={onCloseMenu}
                        onChange={() => this.onTagSortChanged(SortAlgorithm.Recent)}
                        checked={!isAlphabetical}
                        name={`mx_${this.props.tagId}_sortBy`}
                    >
                        {_t("Activity")}
                    </StyledMenuItemRadio>
                    <StyledMenuItemRadio
                        onClose={onCloseMenu}
                        onChange={() => this.onTagSortChanged(SortAlgorithm.Alphabetic)}
                        checked={isAlphabetical}
                        name={`mx_${this.props.tagId}_sortBy`}
                    >
                        {_t("A-Z")}
                    </StyledMenuItemRadio>
                </div>
                {notInviteSections}
            </div>
        );
    }
}

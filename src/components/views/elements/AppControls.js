import React, { Component } from 'react';
import isElectron from 'is-electron';

export default class AppControls extends Component {
    state = { isMaximized: false };

    componentDidMount = () => {
        if (!isElectron()) return;
        window.ipcRenderer.on('maximize', () => this.setState({ isMaximized: true }));
        window.ipcRenderer.on('unmaximize', () => this.setState({ isMaximized: false }));
    };

    renderWindows = () => {
        return (
            <div className="AppControls">
                <div className="AppControls-button AppControls-minimize" onClick={() => window.ipcRenderer.send('window-minimize')}>
                    <svg viewBox="0 0 36 32" fill="currentColor">
                        <path d="M23 15H13V15.8929H23V15Z" />
                    </svg>
                </div>
                {this.state.isMaximized ? (
                    <div className="AppControls-button AppControls-unmaximize" onClick={() => window.ipcRenderer.send('window-unmaximize')}>
                        <svg viewBox="0 0 36 32" fill="currentColor">
                            <path d="M14.665 20H23V11.665H21.335V10H13V18.335H14.665V20ZM20.5 10.835V17.5H13.835V10.835H20.5ZM22.165 12.5V19.165H15.5V18.335H21.335V12.5H22.165Z" />
                        </svg>
                    </div>
                ) : (
                    <div className="AppControls-button AppControls-maximize" onClick={() => window.ipcRenderer.send('window-maximize')}>
                        <svg viewBox="0 0 36 32" fill="currentColor">
                            <path d="M23 10H13V20H23V10ZM22.165 19.165H13.835V10.835H22.165V19.165Z" />
                        </svg>
                    </div>
                )}
                <div className="AppControls-button AppControls-close" onClick={() => window.ipcRenderer.send('window-close')}>
                    <svg viewBox="0 0 36 32" fill="currentColor">
                        <path d="M23 19.4768L18.5232 15L23 10.5232L22.4768 10L18 14.4768L13.5232 10L13 10.5232L17.4768 15L13 19.4768L13.5232 20L18 15.5232L22.4768 20L23 19.4768Z" />
                    </svg>
                </div>
            </div>
        );
    };

    renderMac = () => {
        return (
            <div className="AppControls AppControls-mac">
                <div className="AppControls-macbutton AppControls-macclose" onClick={() => window.ipcRenderer.send('window-close')}>
                    <div className="AppControls-machover AppControls-macclosebutton">
                        <span>
                            <strong>x</strong>
                        </span>
                    </div>
                </div>
                <div className="AppControls-macbutton AppControls-macminimize" onClick={() => window.ipcRenderer.send('window-minimize')}>
                    <div className="AppControls-machover AppControls-macminimizebutton">
                        <span>
                            <strong>&ndash;</strong>
                        </span>
                    </div>
                </div>
                <div
                    className="AppControls-macbutton AppControls-maczoom"
                    onClick={() => window.ipcRenderer.send(this.state.isMaximized ? 'window-unmaximize' : 'window-maximize')}
                >
                    <div className="AppControls-machover AppControls-maczoombutton">
                        <span>
                            <strong>+</strong>
                        </span>
                    </div>
                </div>
            </div>
        );
    };

    render() {
        if (!isElectron() || !window.ipcRenderer) return null;
        const isMac = window.navigator.platform.startsWith('Mac');

        if (this.props.side === 'left' && isMac) {
            return this.renderMac();
        }

        if (this.props.side === 'right' && !isMac) {
            return this.renderWindows();
        }

        return null;
    }
}
